<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_purchases_register', function (Blueprint $table){
            $table->integer('status_id')->default(1);
        });
        Schema::table('voucher_sales_register', function (Blueprint $table){
            $table->integer('status_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_purchases_register', function (Blueprint $table){
            $table->dropColumn('status_id');
        });
        Schema::table('voucher_sales_register', function (Blueprint $table){
            $table->dropColumn('status_id');
        });
    }
}
