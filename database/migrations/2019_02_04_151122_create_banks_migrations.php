<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('payment_methods', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
        });
        
        Schema::table('voucher_purchases_register', function (Blueprint $table){
            $table->integer('bank_id')->nullable();
            $table->string('check_number')->nullable();
            $table->integer('payment_id')->default(1);
            
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('payment_id')->references('id')->on('payment_methods');
        });

        Schema::table('voucher_sales_register', function (Blueprint $table){
            $table->integer('bank_id')->nullable();
            $table->string('check_number')->nullable();
            $table->integer('payment_id')->default(1);
            
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('payment_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_sales_register', function(Blueprint $table){
          // $table->dropForeign(['bank_id', 'payment_id']);
            $table->dropColumn(['bank_id', 'payment_id', 'check_number']);
        });

        Schema::table('voucher_purchases_register', function(Blueprint $table){
           // $table->dropForeign(['bank_id', 'payment_id']);
            $table->dropColumn(['bank_id', 'payment_id', 'check_number']);
        });

        Schema::drop('payment_methods');
        Schema::drop('banks');
    }
}
