<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSalesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("invoice_headers", function (Blueprint $table) {
            $table->integer('salesman_id')->default(1);
            $table->foreign('salesman_id')->references('id')->on('salesman');
        });

        Schema::create('salesman_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("salesman_id");
            $table->integer("product_id");
            $table->integer('order_detail_id');
            $table->string('commission_motive');
            $table->decimal("commission_percentage", 100, 2);
            $table->integer("amount");
            $table->boolean("canceled")->default(false);
            $table->timestamps();

            $table->foreign('salesman_id')->references('id')->on('salesman');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('order_detail_id')->references('id')->on('invoice_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("invoice_headers", function (Blueprint $table) {
            $table->dropColumn('salesman_id');
        });

        Schema::dropIfExists('salesman_commissions');

    }
}
