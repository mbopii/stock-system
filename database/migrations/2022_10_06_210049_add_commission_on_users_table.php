<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->boolean('can_get_commission')->default(false);
            $table->decimal('commission_percentage', 100, 2)->default(0);
        });

        Schema::create('user_commissions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('invoice_header_id');
            $table->decimal("commission_percentage", 100, 2);
            $table->integer("amount");
            $table->boolean("canceled")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_commissions');

        Schema::table("users", function(Blueprint $table) {
            $table->dropColumn(['can_get_commission', 'commission_percentage']);
        });
    }
}
