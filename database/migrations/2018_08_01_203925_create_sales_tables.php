<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_headers', function (Blueprint $table){
            $table->increments('id');
            $table->integer("user_id");
            $table->integer('client_id');
            $table->string('total_amount');
            $table->string('total_discount')->default(0);
            $table->date('sale_date');
            $table->integer('branch_id');
            $table->string('invoice_number')->nullable();
            $table->integer("currency_id");
            $table->string('change_fee');
            $table->string('company_id');
            $table->integer('voucher_id');
            $table->integer('sale_type_id');
            $table->integer("invoice_status")->default(1);
            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('user_id')->references('id')->on("users");
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('status_id')->references('id')->on("statuses");

        });

        Schema::create('invoice_details', function (Blueprint $table){
            $table->increments('id');
            $table->integer('invoice_header_id');
            $table->integer('product_id');
            $table->integer('pricing_list_id');
            $table->string('price');
            $table->string('special_price');
            $table->integer('currency_id');
            $table->string('change_fee');
            $table->integer('quantity');
            $table->string('sub_total');
            $table->string('special_subtotal');
            $table->string('subtotal_discount');
            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('invoice_header_id')->references('id')->on("invoice_headers");
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('pricing_list_id')->references('id')->on('pricing_header');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        Schema::create('vouchers', function (Blueprint $table){
            $table->increments('id');
            $table->string('description');

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create("statuses", function(Blueprint $table){
            $table->increments('id');
            $table->string("description");
            $table->timestamps();

            $table->engine = "InnoDB";

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_details', function (Blueprint $table){
            $table->dropForeign(['product_id', 'invoice_header_id', 'pricing_list_id']);
        });

        Schema::drop('invoice_headers', function(Blueprint $table){
            $table->dropForeign(['user_id', 'client_id', 'branch_id', 'status_id']);
        });

        Schema::drop('vouchers');
        Schema::drop("statuses");
    }
}
