<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchasesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases_details', function (Blueprint $table){
            $table->string('special_price')->default(0);
            $table->string('special_subtotal')->default(0);
            $table->string('subtotal_discount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("purchases_details", function (Blueprint $table){
            $table->dropColumn(['special_price', 'special_subtotal', 'subtotal_discount']);
        });
    }
}
