<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->string('tax_code');
            $table->string('stamping');
            $table->string('address');
            $table->string('telephone');
            $table->integer('city_id');
            $table->integer('invoice_first');
            $table->integer('invoice_second');
            $table->integer('print_invoice_number');

            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('city_id')->references('id')->on("cities");
        });

        Schema::table('invoice_headers', function(Blueprint $table){
            $table->integer('company_id')->default(1);
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table){
            $table->dropForeign(['company_id']);
        });
        Schema::drop('companies');
    }
}
