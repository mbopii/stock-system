<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_types', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
        });
        Schema::create('stock_control', function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('movement_type_id');
            $table->integer("movement_id");
            $table->string('quantity');
            $table->string('stock_before');
            $table->string('stock_after');

            $table->timestamps();

            $table->foreign('movement_type_id')->references('id')->on('movement_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_control', function (Blueprint $table){
            $table->dropForeign(['movement_type_id']);
        });

        Schema::drop('movement_types');
        
    }
}
