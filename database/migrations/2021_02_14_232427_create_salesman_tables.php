<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesmanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesman', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->string("telephone")->nullable();
            $table->longText('observations')->nullable();
            $table->integer('total_commissions')->default(0);
            $table->integer('branch_id');

            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->decimal('commission_percentage', 100, 2)->default(0);
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->decimal('commission_percentage', 100, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('commission_percentage');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('commission_percentage');
        });

        Schema::drop('salesman');
    }
}
