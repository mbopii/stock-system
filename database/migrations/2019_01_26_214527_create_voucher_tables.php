<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_headers', function(Blueprint $table){
            $table->integer('credit_days')->default(0);
            $table->integer('amount_left')->default(0);
        });

        Schema::table('purchase_header', function(Blueprint $table){
            $table->integer('credit_days')->default(0);
            $table->integer('amount_left')->default(0);
        });

        Schema::create('voucher_sales_register', function(Blueprint $table){
            $table->increments('id');
            $table->integer('invoice_header_id');
            $table->string('voucher_number');
            $table->integer('amount');
            $table->timestamps();

            $table->foreign('invoice_header_id')->references('id')->on('invoice_headers');
        });

        Schema::create('voucher_purchases_register', function(Blueprint $table){
            $table->increments('id');
            $table->integer('purchase_header_id');
            $table->string('voucher_number');
            $table->integer('amount');
            $table->timestamps();

            $table->foreign('purchase_header_id')->references('id')->on('purchase_header');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_headers', function(Blueprint $table){
            $table->dropColumn('credit_days');
            $table->dropColumn('amount_left');
        });

        Schema::table('purchase_header', function(Blueprint $table){
            $table->dropColumn('credit_days');
            $table->dropColumn('amount_left');
        });

        Schema::drop('voucher_sales_register', function(Blueprint $table){
           $table->dropForeign(['invoice_header_id']);
        });

        Schema::drop('voucher_purchases_register', function(Blueprint $table){
            $table->dropForeign(['purchase_header_id']);
        });
    }

}
