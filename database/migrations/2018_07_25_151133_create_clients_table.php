<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('cities', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->integer('department_id');
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('departments');

            $table->engine = 'InnoDB';
        });

        Schema::create('clients', function(Blueprint $table){
             $table->increments('id');
             $table->string('client_code');
             $table->string('description');
             $table->string("idnum");
             $table->string("tax_name")->default("Sin Nombre");
             $table->string('tax_code')->default('4444444-4');
             $table->string('address')->nullable();
             $table->string('telephone')->nullable();
             $table->integer('city_id')->nullable();
             $table->timestamps();

             $table->engine = 'InnoDB';

             $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
        Schema::drop('cities', function(Blueprint $table){
            $table->dropForeign(['department_id']);
        });
        Schema::drop('departments', function(Blueprint $table){
            $table->dropForeign(['city_id']);
        });
    }
}
