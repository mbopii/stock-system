<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksFollowup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->nullable();
            $table->integer("client_id")->nullable();
            $table->integer("provider_id")->nullable();
            $table->integer('current_available');
            $table->integer('new_available');
            $table->string("reason");
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks_movements');
    }
}
