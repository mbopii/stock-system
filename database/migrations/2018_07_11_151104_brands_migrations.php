<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BrandsMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('metric_unit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('abbreviation');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('tax_name');
            $table->string('tax_code');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax_number')->nullable();

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('address')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax_number')->nullable();

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('bar_code');
            $table->integer('metric_unit_id');
            $table->integer('brand_id');
            $table->integer('category_id');

            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('metric_unit_id')->references('id')->on('metric_unit');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::create('pricing_header', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->timestamps();

            $table->engine = "InnoDB";
        });

        Schema::create('pricing_details', function(Blueprint $table){
            $table->increments('id');
            $table->integer('pricing_header_id');
            $table->integer('product_id');
            $table->string('purchase_price')->nullable();
            $table->string('selling_price');
            $table->string('profit')->default(0);
            $table->string('profit_percentage')->default(0);

            $table->timestamps();
            $table->engine = 'InnoDB';

            $table->foreign('pricing_header_id')->references('id')->on('pricing_header');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('products_pricing', function(Blueprint $table){
            $table->integer('product_id');
            $table->integer('pricing_header_id');

            $table->timestamps();

            $table->primary(['product_id', 'pricing_header_id']);

            $table->engine = 'InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pricing_details', function(Blueprint $table){
            $table->dropForeign(['pricing_header_id', 'product_id']);
        });

        Schema::drop('pricing_header');
        Schema::drop('products', function(Blueprint $table){
            $table->dropForeign(['category_id', 'brand_id', 'metric_unit_id']);
        });

        Schema::drop('products_pricing');
        Schema::drop('brands');
        Schema::drop('categories');
        Schema::drop('metric_unit');
        Schema::drop('providers');
        Schema::drop('branches');
    }
}
