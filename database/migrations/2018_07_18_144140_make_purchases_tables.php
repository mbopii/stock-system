<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePurchasesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string("symbol");
            $table->string('purchase_price')->default(0);
            $table->string('selling_price')->default(0);
            $table->string('icon')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDb';
        });

        Schema::create('payments_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('abbreviation');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('fee_status', function(Blueprint $table){
            $table->increments('id');
            $table->string('description');

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('purchase_header', function (Blueprint $table) {
            $table->increments('id');
            $table->date('purchase_date');
            $table->string('total_amount')->default(0);
            $table->string('invoice_number');
            $table->string('stamping'); // Timbrado
            $table->integer('provider_id');
            $table->integer('payment_form_id');
            $table->integer('total_fee')->default(0);
            $table->integer('remaining_fee')->default(0);
            $table->integer('currency_id');
            $table->string('currency_change');
            $table->integer('branch_id');
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('providers');
            $table->foreign('payment_form_id')->references('id')->on('payments_forms');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('user_id')->references('id')->on('users');

            $table->engine = 'InnoDB';
        });

        Schema::create('purchases_details', function (Blueprint $table){
            $table->increments('id');
            $table->integer('purchase_header_id');
            $table->integer('product_id');
            $table->string('quantity');
            $table->integer('currency_id');
            $table->string('currency_change');
            $table->string('single_amount');
            $table->string('total_amount');
            $table->timestamps();

            $table->foreign('purchase_header_id')->references('id')->on('purchase_header');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('currency_id')->references('id')->on("currencies");

            $table->engine = 'InnoDB';
        });

        Schema::create("providers_fees", function(Blueprint $table){
            $table->increments('id');
            $table->integer('purchase_header_id');
            $table->string("total_amount");
            $table->integer("total_fees");
            $table->integer('current_fee');
            $table->string('fee_amount');
            $table->string('remaining_debt');
            $table->integer("fee_status_id");
            $table->integer('provider_id');

            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('purchase_header_id')->references('id')->on('purchase_header');
            $table->foreign('fee_status_id')->references('id')->on('fee_status');
            $table->foreign('provider_id')->references('id')->on('providers');

        });

        Schema::create('payments_header', function(Blueprint $table){
            $table->increments('id');
            $table->string('total_amount');
            $table->integer('purchase_header_id');
            $table->integer('currency_id');
            $table->string('currency_change');
            $table->integer('total_fee')->default(0);
            $table->integer('remaining_fee')->default(0);
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('purchase_header_id')->references('id')->on('purchase_header');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('user_id')->references('id')->on('users');

            $table->engine = 'InnoDB';
        });

        Schema::create('payments_details', function(Blueprint $table){
            $table->increments('id');
            $table->integer('payment_header_id');
            $table->integer('payment_form_id');
            $table->integer('currency_id')->default(1);
            $table->string('currency_change')->default(1);
            $table->string('amount');
            $table->integer('fee_number')->default(0);
            $table->string('receipt_number');
            $table->integer('total_fees');
            $table->timestamps();

            $table->foreign('payment_header_id')->references('id')->on('payments_header');
            $table->foreign('payment_form_id')->references('id')->on('payments_forms');

        });

        Schema::create('stocks', function(Blueprint $table){
            $table->increments('id');
            $table->integer('branch_id');
            $table->integer('product_id');
            $table->integer('available');
            $table->integer('max_quantity');
            $table->integer('min_quantity');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('product_id')->references('id')->on('products');


            $table->engine = 'InnoDB';
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks', function(Blueprint $table){
            $table->dropForeign(['branch_id', 'product_id']);
        });

        Schema::drop('payments_details', function(Blueprint $table){
            $table->dropForeign(['payment_header_id', 'payment_form_id']);
        });

        Schema::drop('payments_header', function(Blueprint $table){
            $table->dropForeign(['purchase_header_id', 'currency_id', 'user_id']);
        });

        Schema::drop('purchases_details', function(Blueprint $table){
            $table->dropForeign(['purchase_header_id', 'product_id']);
        });

        Schema::drop('purchase_header', function(Blueprint $table){
            $table->dropForeign(['provider_id', 'payment_form_id', 'currency_id', 'branch_id', 'user_id']);
        });

        Schema::drop('payments_forms');
        Schema::drop('currencies');

    }
}
