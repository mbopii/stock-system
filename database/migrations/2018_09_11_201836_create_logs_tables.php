<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_stocks', function(Blueprint $table){
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("stock_id");
            $table->integer('current_available');
            $table->integer('new_available');
            $table->string("reason");
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on("users");
            $table->foreign('stock_id')->references('id')->on("stocks");

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("log_stocks");
    }
}
