<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_details', function (Blueprint $table){
            $table->integer('stock_before')->default(0);
            $table->integer('stock_after')->default(0);
        });

        Schema::table('purchases_details', function (Blueprint $table){
            $table->integer('stock_before')->default(0);
            $table->integer('stock_after')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_details', function (Blueprint $table){
            $table->dropColumn('stock_before');
            $table->dropColumn('stock_after');
        });

        Schema::table('purchases_details', function (Blueprint $table){
            $table->dropColumn('stock_before');
            $table->dropColumn('stock_after');
        });
    }
}
