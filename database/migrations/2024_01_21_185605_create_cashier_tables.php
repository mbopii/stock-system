<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashierTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashiers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_open')->default(false);
            $table->timestamps();
        });

        Schema::create('cashier_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cashier_id')->references('id')->on('cashiers');
            $table->integer('currency_id')->references('id')->on('currencies');
            $table->integer('open_by')->references('id')->on('users');
            $table->integer('closed_by')->nullable()->references('id')->on('users');
            $table->string('opening_amount')->default(0);
            $table->string('closing_amount')->nullable()->default(0);
            $table->date('opened_at')->nullable();
            $table->time('opened_time')->nullable();
            $table->date('closed_at')->nullable();
            $table->time('closed_time')->nullable();
            $table->timestamps();;
        });

        Schema::create('cashier_withdraws', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cashier_movement_id')->references('id')->on('cashier_movements');
            $table->string('amount_withdraw');
            $table->string('amount_before_withdraw');
            $table->string('amount_after_withdraw');
            $table->longText('reason')->nullable();
            $table->integer("withdraw_by")->references('id')->on('users');
            $table->timestamps();
        });

        Schema::table('invoice_headers', function (Blueprint $table) {
            $table->integer("cashier_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_headers', function(Blueprint $table) {
            $table->dropColumn('cashier_id');
        });
        Schema::drop('cashier_withdraws');
        Schema::drop('cashier_movements');
        Schema::drop('cashiers');
    }
}
