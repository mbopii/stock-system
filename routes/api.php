<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('products/{query}', function ($query) {

    // Products select
    $results = \DB::table('products')
        ->join('brands', 'brands.id', '=', 'products.brand_id')
        ->join('categories', 'categories.id', '=', 'products.category_id')
        ->where('products.title', 'ILIKE', "%{$query}%")
        ->orWhere('products.description', 'ILIKE', "%{$query}%")
        ->orWhere('brands.description', 'ILIKE', "%{$query}%")
        ->orWhere('categories.description', 'ILIKE', "%{$query}%")
        ->select('products.*', \DB::raw('brands.description as brand_description'))
        ->get();
    return response()->json(['error' => false, 'data' => ['query' => $query, 'results' => $results]]);
});