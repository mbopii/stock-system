<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', ['as' => 'login.page.show', 'uses' => 'AuthController@loginPage']);
Route::post('login', ['as' => 'login.page.do', 'uses' => 'AuthController@loginAttempt']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
/*
|--------------------------------------------------------------------------
| Dash Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::get('/', 'HomeController@index');
/*
/*
|--------------------------------------------------------------------------
| Permission Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::resource('permissions', 'PermissionsController');
/*
|--------------------------------------------------------------------------
| Roles Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::resource('roles', 'RolesController');
/*
|--------------------------------------------------------------------------
| Users Routes | Sentinel Implementation
|--------------------------------------------------------------------------
*/
Route::resource('users', 'UsersController');
/*
|--------------------------------------------------------------------------
| Cities Routes
|--------------------------------------------------------------------------
*/
Route::resource('departments/{department_id}/cities', 'CitiesController');
/*
|--------------------------------------------------------------------------
| Departments Routes
|--------------------------------------------------------------------------
*/
Route::resource('departments', 'DepartmentsController');

/*
|--------------------------------------------------------------------------
| Clients Routes
|--------------------------------------------------------------------------
*/
Route::get('clients/{id}/toggle/', 'ClientsController@toggleClients')->name("clients.toggle");
Route::get('clients/get_client_code', 'ClientsController@getLastClientCode');
Route::post('clients/add_sales', 'ClientsController@addClientBySale');
Route::get('clients/search/{idnum}', 'ClientsController@searchClient');
Route::resource('clients', 'ClientsController');
/*
|--------------------------------------------------------------------------
| Brands Routes
|--------------------------------------------------------------------------
*/
Route::resource('brands', 'BrandsController');
/*
|--------------------------------------------------------------------------
| Metrics Units Routes
|--------------------------------------------------------------------------
*/
Route::resource('metrics', 'MetricsController');
/*
|--------------------------------------------------------------------------
| Categories Routes
|--------------------------------------------------------------------------
*/
Route::resource('categories', 'CategoriesController');
/*
|--------------------------------------------------------------------------
| Providers Routes
|--------------------------------------------------------------------------
*/
Route::get('providers/search/{id}', 'ProvidersController@searchProvider');
Route::resource('providers', 'ProvidersController');
/*
|--------------------------------------------------------------------------
| Products Routes
|--------------------------------------------------------------------------
*/
Route::get("products/find_price/{id}/{list_id}", 'ProductsController@findSellProductsDetails');
Route::get('products/find_product/{id}', 'ProductsController@findProductDetails');
Route::resource('products', 'ProductsController');
/*
|--------------------------------------------------------------------------
| Pricing List Routes
|--------------------------------------------------------------------------
*/
Route::post('all_products', ['as' => 'all.products', 'uses' => 'PricingListController@salesProducts']);
Route::patch('pricing/update', 'PricingListController@updateList');
Route::resource('pricing', 'PricingListController');
/*
|--------------------------------------------------------------------------
| Branches Routes
|--------------------------------------------------------------------------
*/
Route::resource('branches', 'BranchesController');
/*
|--------------------------------------------------------------------------
| Payments Form Routes
|--------------------------------------------------------------------------
*/
Route::resource('payments_forms', 'PaymentsFormController');
/*
|--------------------------------------------------------------------------
| Currencies Routes
|--------------------------------------------------------------------------
*/
Route::resource('currencies', 'CurrenciesController');
/*
|--------------------------------------------------------------------------
| Purchases Routes
|--------------------------------------------------------------------------
*/
Route::post('purchases/payments/submit', ['as' => 'purchases.voucher.submit', 'uses' => 'PurchasesController@paymentVouchersConfirmationSubmit']);
Route::post('purchases/payments/confirmation', ['as' => 'purchases.payments.confirmation', 'uses' => 'PurchasesController@paymentVouchersConfirmation']);
Route::get('purchases/payments', ['as' => 'purchases.payments', 'uses' => 'PurchasesController@paymentVouchers']);
Route::get('purchases/{id}/voucher/cancel', ['as' => 'purchases.voucher.cancel', 'uses' => 'PurchasesController@cancelVoucher']);
Route::post('purchases/{id}/voucher', ['as' => 'purchases.voucher', 'uses' => 'PurchasesController@registerVoucherPayment']);
Route::get('purchases/{id}/voucher', ['as' => 'purchases.voucher.show', 'uses' => 'PurchasesController@showRegisterVoucher']);
Route::resource('purchases', 'PurchasesController');
/*
|--------------------------------------------------------------------------
| Stock Routes
|--------------------------------------------------------------------------
*/
Route::post("stocks/update", 'StocksController@stockAdjustment');
Route::resource('stocks', 'StocksController', ['except' => 'show']);
/*
|--------------------------------------------------------------------------
| Selling Routes
|--------------------------------------------------------------------------
*/
Route::get('sales/create2', 'SalesController@newSalesMethod')->name('sales.create.two');
Route::post('sales/payments/submit', ['as' => 'sales.voucher.submit', 'uses' => 'SalesController@paymentVouchersConfirmationSubmit']);
Route::post('sales/payments/confirmation', ['as' => 'sales.payments.confirmation', 'uses' => 'SalesController@paymentVouchersConfirmation']);
Route::get('sales/payments', ['as' => 'sales.payments', 'uses' => 'SalesController@paymentVouchers']);
Route::get('sales/{id}/voucher/cancel', ['as' => 'sales.voucher.cancel', 'uses' => 'SalesController@cancelVoucher']);
Route::get('sales/{id}/refund', ['as' => 'sales.refund', 'uses' => 'SalesController@showSalesRefund']);
Route::post('sales/{id}/refund', ['as' => 'sales.refund.do', 'uses' => 'SalesController@doSalesRefund']);
Route::post('sales/{id}/voucher', ['as' => 'sales.voucher', 'uses' => 'SalesController@registerVoucherPayment']);
Route::get('sales/{id}/voucher', ['as' => 'sales.voucher.show', 'uses' => 'SalesController@showRegisterVoucher']);
Route::get('sales/print/{id}', ['as' => 'sales.print', 'uses' => 'SalesController@printInvoice']);
Route::resource('sales', 'SalesController');
/*
|--------------------------------------------------------------------------
| Companies Routes
|--------------------------------------------------------------------------
*/
Route::get('companies/get_number/{id}', 'CompaniesController@getNumber');
Route::resource('companies', 'CompaniesController');

/*
|--------------------------------------------------------------------------
| Reports Routes
|--------------------------------------------------------------------------
*/
Route::get('reports/purchases/', ['as' => 'reports.purchases', 'uses' => 'ReportsController@purchases']);
Route::get('reports/refunds/', ['as' => 'reports.refunds', 'uses' => 'ReportsController@refunds']);
Route::get('reports/pricing/', ['as' => 'reports.pricing', 'uses' => 'ReportsController@pricing']);
Route::get('reports/products/', ['as' => 'reports.products.all', 'uses' => 'ReportsController@allProducts']);
Route::get('reports/providers/credit', ['as' => 'reports.providers.credit', 'uses' => 'ReportsController@providersWithCredit']);
Route::get('reports/clients/credit', ['as' => 'reports.clients.credit', 'uses' => 'ReportsController@clientsWithCredit']);
Route::get('reports/products/provider', ['as' => 'reports.products.provider', 'uses' => 'ReportsController@productsByProvider']);
Route::get('reports', ['as' => 'reports.index', 'uses' => 'ReportsController@index']);
Route::get('reports/sales', ['as' => 'reports.sales', 'uses' => 'ReportsController@sales']);
Route::get('reports/day', ['as' => 'reports.sales.day', 'uses' => 'ReportsController@daySales']);
Route::get('reports/products/history', ['as' => 'reports.products.history', 'uses' => 'ReportsController@productHistory']);
Route::get('reports/sales/day', ['as' => 'reports.sales.day', 'uses' => 'ReportsController@daySales']);
Route::get('reports/salesman/commissions', ['as' => 'reports.salesman.commissions', 'uses' => 'ReportsController@getSalesmanCommissions']);
Route::get('reports/users/commissions', ['as' => 'reports.users.commissions', 'uses' => 'ReportsController@getUsersCommissions']);
Route::get('reports/cashiers/movements', ['as' => 'reports.cashiers.movements', 'uses' => 'ReportsController@cashierStatus']);

/*
|--------------------------------------------------------------------------
| Banks Routes
|--------------------------------------------------------------------------
*/
Route::resource('banks', 'BanksController');

/*
|--------------------------------------------------------------------------
| Salesman Routes
|--------------------------------------------------------------------------
*/
Route::resource('salesman', 'SalesmanController');

/*
|--------------------------------------------------------------------------
| Cashier Routes
|--------------------------------------------------------------------------
*/
Route::post('cashiers/{id}/extraction', ['as' => 'cashiers.extraction.do', 'uses' => 'CashiersController@cashierExtractionDo']);
Route::get('cashiers/{id}/extraction', ['as' => 'cashiers.extraction.show', 'uses' => 'CashiersController@cashierExtraction']);
Route::post("cashiers/{id}/toggle", ['as' => 'cashiers.toggle.do', 'uses' => 'CashiersController@doToggleCashier']);
Route::get("cashiers/{id}/toggle", ['as' => 'cashiers.toggle', 'uses' => 'CashiersController@toggleCashier']);
Route::resource('cashiers', 'CashiersController');
