<?php

namespace App\Console\Commands;

use App\Models\Products;
use Illuminate\Console\Command;

class UpdateStockTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the stock table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $products = Products::all();
//        foreach ($products as $p) {
//            $firstQuery = \DB::table('invoice_details')
//                ->join('invoice_headers', 'invoice_headers.id', '=', 'invoice_details.invoice_header_id')
//                ->join('products', 'products.id', '=', 'invoice_details.product_id')
//                ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
//                ->join('currencies', 'currencies.id', '=', 'invoice_headers.currency_id')
//                ->join('stocks', 'stocks.product_id', '=', 'products.id')
//                ->selectRaw("products.title as product, invoice_headers.sale_date as movement_sale, invoice_headers.invoice_number as invoice,
//                invoice_details.quantity as sale_quantity, clients.description as client, currencies.symbol as currency, 'Venta' as movement,
//                invoice_details.price as price, invoice_details.change_fee as fee, stocks.available as available")
//                ->where('products.id', '=', $p->id);
//
//            $stockLogQuery = \DB::table('log_stocks')
//                ->join('stocks', 'stocks.id', '=', 'log_stocks.stock_id')
//                ->join('users', 'users.id', '=', 'log_stocks.user_id')
//                ->join('products', 'products.id', '=', 'stocks.product_id')
//                ->selectRaw("products.title as product, log_stocks.created_at as movement_sale, log_stocks.id::varchar as invoice,
//                log_stocks.new_available::varchar as sale_quantity, users.description as client, '0' as currency, 'Ajuste' as movement, '0' as price, '0' as fee,
//                stocks.available as available")
//                ->where('products.id', '=', $p->id);
//            $queryRaw = \DB::table('purchases_details')
//                ->join('purchase_header', 'purchase_header.id', '=', 'purchases_details.purchase_header_id')
//                ->join('products', 'products.id', '=', 'purchases_details.product_id')
//                ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
//                ->join('currencies', 'currencies.id', '=', 'purchase_header.currency_id')
//                ->join('stocks', 'stocks.product_id', '=', 'products.id')
//                ->selectRaw("products.title as product, purchase_header.purchase_date as movement_sale, purchase_header.invoice_number as invoice,
//                purchases_details.quantity as sale_quantity, providers.description as provider, currencies.symbol as currency, 'Compra' as movement,
//                purchases_details.single_amount as price, purchases_details.currency_change as fee, stocks.available as available")
//                ->where('products.id', '=', $p->id)
//                ->union($firstQuery)
//                ->union($stockLogQuery)
//                ->orderBy('movement_sale', 'asc')
//                ->get();
//
//            foreach ($queryRaw as $movement) {
//                $array = [];
//                $stock = 0;
//                switch ($movement->movement) {
//                    case 'Venta':
//                        $stock = $movement->
//                        $array
//                        break;
//
//                    case 'Compra':
//                        echo 'Compra';
//                        break;
//
//                    case 'Ajuste':
//                        echo 'Ajuste';
//                        break;
//                }
//
//                \DB::table('stocks_movements')
//                    ->insert($array);
//            }
//            dd($queryRaw);
//
//        }

    }
}
