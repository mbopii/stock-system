<?php

namespace App\Console\Commands;

use App\Models\PricingDetails;
use App\Models\PurchasesDetails;
use Illuminate\Console\Command;

class UpdatePricingList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pricing:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the pricing details table';

    /**0
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all price list
        $pricingList = PricingDetails::all();
        foreach ($pricingList as $details) {
            // Get the purchase price for the specific product
            $purchasePrice = PurchasesDetails::where('product_id', $details->product_id)->select(['single_amount'])->orderBy('id', 'desc')->first();
            if (!$purchasePrice || is_null($purchasePrice)) {
                $details->profit = 0;
                $details->profit_percentage = 100;
            } else {
                $singleAmount = str_replace(',', '.', $purchasePrice->single_amount);
                $details->purchase_price = $singleAmount;
                $details->profit = $details->selling_price - $singleAmount;
                if ($details->selling_price != 0) {
                    $details->profit_percentage = round((($details->selling_price - $singleAmount) / $details->selling_price) * 100, 2, PHP_ROUND_HALF_DOWN);
                }
            }
            $details->update();
        }
    }
}
