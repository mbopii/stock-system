<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentsForm extends Model
{
    protected $table = 'payments_forms';

    protected $fillable = ['description', 'abbreviation'];

}