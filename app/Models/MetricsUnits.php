<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricsUnits extends Model
{

    protected $table = 'metric_unit';

    protected $fillable = ['description', 'abbreviation'];

}