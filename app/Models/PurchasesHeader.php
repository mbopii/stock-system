<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchasesHeader extends Model
{

    protected $table = 'purchase_header';

    protected $fillable = ['purchase_date', 'total_amount', 'invoice_number', 'stamping', 'provider_id',
        'payment_form_id', 'currency_id', 'branch_id', 'user_id', 'currency_change', 'invoice_status', 'company_id',
        'credit_days', 'amount_left'];


    public function purchaseDetails()
    {
        return $this->hasMany('App\Models\PurchasesDetails', 'purchase_header_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Providers', 'provider_id');
    }

    public function invoiceStatus()
    {
        return $this->belongsTo('App\Models\Statuses', 'invoice_status');
    }

    public function companies()
    {
        return $this->belongsTo('App\Models\Companies', 'company_id');
    }

    public function paymentForm()
    {
        return $this->belongsTo('App\Models\PaymentsForm', 'payment_form_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currencies', 'currency_id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\VoucherPurchases', 'purchase_header_id');
    }
}