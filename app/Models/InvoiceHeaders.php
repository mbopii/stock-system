<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceHeaders extends Model
{

    protected $table = 'invoice_headers';

    protected $fillable = ['user_id', 'client_id', 'total_amount', 'sale_date', 'branch_id', 'invoice_number',
        'currency_id', 'change_fee', 'company_id', 'sale_type_id', 'invoice_status', 'total_discount','payment_form_id',
        'credit_days', 'amount_left', 'cashier_id'];


    public function invoiceDetails()
    {
        return $this->hasMany('App\Models\InvoiceDetails', 'invoice_header_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Clients', 'client_id');
    }

    public function cashier()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function invoiceStatus()
    {
        return $this->belongsTo('App\Models\Statuses', 'invoice_status');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Companies', 'company_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currencies', 'currency_id');
    }

    public function paymentForm()
    {
        return $this->belongsTo('App\Models\PaymentsForm', 'payment_form_id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Vouchers', 'invoice_header_id');
    }
}
