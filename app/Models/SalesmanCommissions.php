<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesmanCommissions extends Model
{

    protected $table = 'salesman_commissions';

    protected $fillable = [
        'salesman_id', 'product_id', 'order_detail_id', 'commission_motive', 'commission_percentage', 'canceled', 'amount'
    ];

    public function detail()
    {
        return $this->belongsTo(InvoiceDetails::class, 'order_detail_id');
    }

    public function salesman()
    {
        return $this->belongsTo(Salesman::class, 'salesman_id');
    }

}
