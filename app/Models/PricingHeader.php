<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingHeader extends Model
{

    protected $table = 'pricing_header';

    protected $fillable = ['title', 'description'];

    public function details()
    {
        return $this->hasMany('App\Models\PricingDetails', 'pricing_header_id');
    }

}