<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Refunds extends Model
{

    protected $table = 'refunds';

    protected $fillable = ['invoice_id', 'invoice_details_id', 'product_id', 'quantity'];


    public function invoiceHeader()
    {
        return $this->hasOne('App\Models\InvoiceHeaders', 'id', 'invoice_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Products', 'id', 'product_id');
    }
}
