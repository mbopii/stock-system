<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashierWithdraws extends Model
{

    protected $table = 'cashier_withdraws';

    protected $fillable = ['cashier_movement_id', 'amount_withdraw', 'amount_before_withdraw', 'amount_after_withdraw',
        'withdraw_by', 'reason'];

    public function cashierMovement()
    {
        return $this->belongsTo(CashierMovements::class, 'cashier_movement_id');
    }

}
