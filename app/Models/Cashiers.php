<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashiers extends Model
{

    protected $table = 'cashiers';

    protected $fillable = ['name', 'is_open'];

    public function movements()
    {
        return $this->hasMany(CashierMovements::class, 'cashier_id');
    }

    public function lastMovement()
    {
        return $this->movements()->orderBy("id", 'desc')->first();
    }

}
