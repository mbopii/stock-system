<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{

    protected $table = 'clients';

    protected $fillable = ['client_code', 'description', 'idnum', 'tax_name', 'tax_code', 'address',
        'telephone', 'city_id', 'active', 'credit_limit', 'credit_available'
    ];

    public function myCity()
    {
        return $this->belongsTo('App\Models\Cities', 'city_id');
    }

}
