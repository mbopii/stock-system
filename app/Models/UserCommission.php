<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCommission extends Model
{

    protected $table = 'user_commissions';

    protected $fillable = [
        'user_id', 'invoice_header_id', 'commission_percentage', 'amount', 'canceled'
    ];

    public function order()
    {
        return $this->belongsTo(InvoiceHeaders::class, 'invoice_header_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
