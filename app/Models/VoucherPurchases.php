<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherPurchases extends Model
{

    protected $table = 'voucher_purchases_register';

    protected $fillable = ['purchase_header_id', 'amount', 'voucher_number', 'bank_id', 'payment_id', 'check_number', 'status_id'];

    public function paymentMethods()
    {
        return $this->belongsTo('App\Models\PaymentMethods', 'payment_id');
    }

    public function banks()
    {
        return $this->belongsTo('App\Models\Banks', 'bank_id');
    }
}