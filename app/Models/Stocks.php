<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stocks extends Model
{

    protected $table = 'stocks';

    protected $fillable = ['branch_id', 'product_id', 'available', 'max_quantity', 'min_quantity'];

    public function products()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branches', 'branch_id');
    }
}
