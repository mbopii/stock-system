<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMovements extends Model
{

    protected $table = 'stock_control';

    protected $fillable = ['product_id', 'movement_type_id', 'quantity', 'movement_id', 'stock_before', 'stock_after'];

    public function movementType()
    {
        return $this->belongsTo('App\Models\StockMovementsTypes', 'movement_type_id');
    }

    public function purchaseInfo()
    {
        return $this->belongsTo('App\Models\PurchasesHeader', 'movement_id');
    }

    public function saleInfo()
    {
        return $this->belongsTo('App\Models\InvoiceHeaders', 'movement_id');
    }

    public function adjustmentInfo()
    {
        return $this->belongsTo('App\Models\LogStocks', 'movement_id');
    }

    public function productInfo()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }

}
