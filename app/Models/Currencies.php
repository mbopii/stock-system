<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{

    protected $table = 'currencies';

    protected $fillable = ['description', 'symbol', 'purchase_price', 'selling_price', 'icon'];

}
