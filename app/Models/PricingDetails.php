<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingDetails extends Model
{

    protected $table = 'pricing_details';

    protected $fillable = ['pricing_header_id', 'product_id', 'purchase_price', 'selling_price', 'profit',
        'profit_percentage'];

    public function header()
    {
        return $this->belongsTo('App\Models\PricingHeader', 'pricing_header_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }

}