<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vouchers extends Model
{

    protected $table = 'voucher_sales_register';

    protected $fillable = ['invoice_header_id', 'amount', 'voucher_number', 'bank_id', 'check_number', 'payment_id', 'status_id', 'check_date'];


    public function invoiceHeader()
    {
        return $this->hasMany('App\Models\InvoiceHeader', 'invoice_header_id');
    }

    public function paymentMethods()
    {
        return $this->belongsTo('App\Models\PaymentMethods', 'payment_id');
    }

    public function banks()
    {
        return $this->belongsTo('App\Models\Banks', 'bank_id');
    }
}
