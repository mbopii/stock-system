<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{

    protected $table = 'salesman';

    protected $fillable = ['name', 'address', 'telephone', 'observations', 'total_commissions', 'branch_id', 'percentage'];

}
