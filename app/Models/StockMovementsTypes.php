<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMovementsTypes extends Model
{

    protected $table = 'movement_types';

    protected $fillable = ['description'];
}
