<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashierMovements extends Model
{

    protected $table = 'cashier_movements';

    protected $fillable = ['cashier_id', 'open_by', 'closed_by', 'opening_amount',
        'closing_amount', 'opened_at', 'closed_at', 'currency_id', 'closed_time', 'opened_time'];

    public function cashier()
    {
        return $this->belongsTo(Cashiers::class, 'cashier_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currencies::class, 'currency_id');
    }

}
