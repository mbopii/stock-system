<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    protected $table = 'products';

    protected $fillable = ['description', 'title', 'bar_code', 'metric_unit_id', 'brand_id',
        'category_id', 'active', 'commission_percentage', 'salesman_id'];

    public function purchase()
    {
        return $this->hasMany('App\Models\PurchasesDetails', 'product_id');
    }

    public function pricing()
    {
        return $this->hasMany('App\Models\PricingDetails', 'product_id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Categories', 'category_id');
    }

    public function stock()
    {
        return $this->hasMany('App\Models\Stocks', 'product_id');
    }

    public function brands()
    {
        return $this->belongsTo('App\Models\Brands', 'brand_id');
    }

}
