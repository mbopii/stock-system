<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogStocks extends Model
{

    protected $table = 'log_stocks';

    protected $fillable = ['user_id', 'stock_id', 'current_available', 'new_available', 'reason'];


    public function stockRow()
    {
        return $this->hasMany('App\Models\Stocks', 'stock_id');
    }

    public function userResponsable()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}