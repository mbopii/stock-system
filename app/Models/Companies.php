<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = 'companies';
    protected $fillable = [
        'description', 'address', 'stamping', 'tax_code', 'telephone', 'city_id', 'invoice_first', 'invoice_second', 'print_invoice_number'
    ];

}