<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{

    protected $table = 'invoice_details';

    protected $fillable = ['invoice_header_id', 'product_id', 'price', 'quantity', 'sub_total', 'pricing_list_id',
        'currency_id', 'change_fee', 'special_price', 'special_subtotal', 'subtotal_discount'];


    public function invoiceHeader ()
    {
        return $this->belongsTo('App\Models\InvoiceHeaders', 'invoice_header_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }

    public function priceList()
    {
        return $this->belongsTo('App\Models\PricingHeader', 'pricing_list_id');
    }

    public function currencies()
    {
        return $this->belongsTo('App\Models\Currencies', 'currency_id');
    }
    
    public function refunds()
    {
        return $this->hasMany('App\Models\Refunds', 'invoice_details_id');
    }
}