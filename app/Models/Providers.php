<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{

    protected $table = 'providers';

    protected $fillable = ['description', 'tax_name', 'tax_code', 'email', 'address',
        'telephone', 'fax_number'];

}