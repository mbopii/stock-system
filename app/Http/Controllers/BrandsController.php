<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('brands')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['description'])) $where .= "description ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $brands = Brands::orderBy('id', 'desc')->paginate(20);
            } else {
                $brands = Brands::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        }else{
            $brands = Brands::orderBy('id', 'desc')->paginate(20);
            $description ='';
            $q = '';
        }

        return view('brands.index', compact('brands', 'description', 'q'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('brands.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("BrandsController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $arrayToSave = [
            'description' => $description
        ];

        if (!Brands::create($arrayToSave)){
            \Log::warning("BrandsController | Error while trying to save new brand");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('brands.index')->with('success', 'Marca creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$brand = Brands::find($id)){
            \Log::warning("BrandsController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

        return view('brands.edit', compact('brand'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$brand = Brands::find($id)){
            \Log::warning("BrandsController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("BrandsController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $brand->description = $description;

        if (!$brand->save()){
            \Log::warning("BrandsController | Error on update brand id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('brands.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('brands.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($brand = Brands::find($id)) {
                try {
                    if (Brands::destroy($id)) {
                        $message = 'Marca eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("BrandsController | Error deleting Brand: " . $e->getMessage());
                    $message = 'Error al intentar eliminar la marca';
                    $error = true;
                }
            } else {
                \Log::warning("BrandsController | Brand {$id} not found");
                $message = 'Marca no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

}