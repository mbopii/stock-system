<?php

namespace App\Http\Controllers;


use App\Http\Request\ProvidersRequest;
use App\Models\Providers;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('providers')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['description'])) $where .= "description ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $providers = Providers::orderBy('id', 'desc')->paginate(20);
            } else {
                $providers = Providers::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        } else {
            $providers = Providers::orderBy('id', 'desc')->paginate(20);
            $description = '';
            $q = '';
        }

        return view('providers.index', compact('providers', 'q', 'description'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('providers.create');
    }

    public function store(ProvidersRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (array_key_exists('email', $input) && !is_null($input['email'])) {
            $validator = \Validator::make($input, ['email' => 'email']);
            if ($validator->fails()) {
                \Log::warning('ProvidersController | Invalid email field');
                return redirect()->back()->with('error', 'La dirección de email no es una dirección válida')
                    ->withInput();
            }
        }

        if ($provider = Providers::create($input)) {
            \Log::info("ProvidersController | New provider created");
            return redirect()->route('providers.index')->with('success', 'Proveedor creado exitosamente');
        } else {
            \Log::warning("ProvidersController | There's a problem with the new provider");
            return redirect()->back()->with('error', 'Ocurrio un error al crear el registro');
        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$provider = Providers::find($id)) {
            \Log::warning("ProvidersController | Provider {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view('providers.edit', compact('provider'));
    }

    public function update(ProvidersRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('providers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!$provider = Providers::find($id)) {
            \Log::warning("ProvidersController | Provider {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrada');
        }

        if (array_key_exists('email', $input) && !is_null($input['email'])) {
            $validator = \Validator::make($input, ['email' => 'email']);
            if ($validator->fails()) {
                \Log::warning('ProvidersController | Invalid email field');
                return redirect()->back()->with('error', 'La dirección de email no es una dirección válida')
                    ->withInput();
            }
        }

        if (!$provider->update($input)) {
            \Log::warning("ProvidersController | Error on update provider id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('providers.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function searchProvider($id)
    {
        if (is_null($id)) {
            \Log::warning("ClientsControllers | Missing id to search");
            return json_encode(['error' => true]);
        }

        $provider = Providers::find($id);
        if (!is_null($provider)) {
            return json_encode(['provider' => $provider, 'error' => false]);
        } else {
            return json_encode(['error' => true, 'code' => 10]);
        }
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('providers.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($provider = Providers::find($id)) {
                try {
                    if (Providers::destroy($id)) {
                        $message = 'Registro eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("ProvidersController | Error deleting Provider : " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("ProviderController | Provider {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
