<?php

namespace App\Http\Controllers;

use App\Http\Request\CompaniesRequest;
use App\Models\Cities;
use App\Models\Companies;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('companies')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $companies = Companies::orderBy('id', 'desc')->paginate(20);

        return view('companies.index', compact('companies'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('companies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $citiesJson = json_encode(Cities::all(['description', 'id']));
        return view('companies.create', compact('citiesJson'));
    }

    public function store(CompaniesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('companies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if ($newCompany = Companies::create($input)){
            \Log::debug("CompaniesController | New company created");
            return redirect()->route('companies.index')->with('success', 'Registro creado exitosamente');
        }

        \Log::warning("CompaniesController | Error while creating a new company");
        return redirect()->back()->with('error', 'Ocurrio un error al crear el registro');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('companies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($company = Companies::find($id)){
            \Log::debug("CompaniesController | Edit company {$company->description}");
            $citiesJson = json_encode(Cities::all(['description', 'id']));
            return view("companies.edit", compact('company', 'citiesJson'));
        }

        \Log::warning("CompaniesController | Missing company {$id}");
        return redirect()->back()->with('error', 'Registro no encontrado');
    }

    public function update(CompaniesRequest $request, $id)
    {

        if (!\Sentinel::getUser()->hasAccess('companies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($company = Companies::find($id)){
            $input = $request->all();
            \Log::debug("CompaniesController | Update company {$company->description}");

            $company->update($input);

            return redirect()->route('companies.index')->with('success', 'Registro modificado exitosamente');
        }

        \Log::warning("CompaniesController | Missing company {$id}");
        return redirect()->back()->with('error', 'Registro no encontrado');
    }

    public function destroy($id)
    {

    }

    public function getNumber($id)
    {
        if (is_null($id)){
            \Log::warning("CompaniesController | Missing id for company");
            return json_encode(['error' => true, 'message' => 'Debe elegir una empresa o comprobante']);
        }

        if ($company = Companies::find($id)){
            // Armamos los numeros de factura para enviar;
            $invoiceNumber = "00" . $company->invoice_first . "-" . "00" . $company->invoice_second . "-" . $company->print_invoice_number;
            return json_encode(['error' => false, 'number' => $invoiceNumber]);
        }else{
            \Log::warning("CompaniesController | Company {$id} not found");
            return json_encode(['error' => true, 'message' => 'Empresa no encontrada']);
        }
    }

}
