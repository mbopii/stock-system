<?php

namespace App\Http\Controllers;


use App\Models\Clients;
use App\Models\InvoiceDetails;
use App\Models\InvoiceHeaders;
use App\Models\Products;
use App\Models\Providers;
use App\Models\PurchasesHeader;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['findProductDetails']]);
    }

    public function index()
    {
        $productsCount = Products::count('*');
        $providersCount = Providers::count('*');
        $salesCount = InvoiceHeaders::count('*');
        $purchasesCount = PurchasesHeader::count('*');
        $clientsCount = Clients::count('*');

        $purchaseByProvider = PurchasesHeader::selectRaw('sum(total_amount::int) as p_sub_total, provider_id')
            ->groupBy('provider_id')
            ->orderBy('p_sub_total', 'desc')
            ->limit(10)
            ->get();

        if ($purchaseByProvider->isEmpty()){
            $purchaseByProvider = 'Sin Compras';
        }

        $productsMostSale = InvoiceDetails::selectRaw('sum(cast(quantity as double precision)) as p_quantity, product_id')
            ->groupBy('product_id')
            ->orderBy('p_quantity', 'desc')
            ->limit(10)
            ->get();

        if ($productsMostSale->isEmpty()){
            $productsMostSale = 'Sin ventas significativas';
        }

        return view('welcome', compact('productsCount', 'providersCount', 'salesCount',
            'purchaseByProvider', 'purchasesCount', 'clientsCount', 'productsMostSale'));
    }
}