<?php

namespace App\Http\Controllers;

use App\Http\Request\UserRequest;
use App\Models\Permissions;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Log;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',
            [
                'except' => [
                    'activate'
                ]
            ]);
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('users')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input)) {
            $where = '';
            if (!is_null($input['description']) && $input['description'] != '') $where .= "description ilike '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == ''){
                $users = User::with('roles')->orderBy('id', 'desc')->paginate(20);
            }else{
                $users = User::with('roles')->whereRaw($where)->orderBy('id', 'desc')->paginate(20);
            }
        }else{
            $users = User::with('roles')->orderBy('id', 'desc')->paginate(20);
        }

        return view('users.index', compact('users'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $roleArray = Role::all()->pluck('description', 'id')->toArray();
        $roleArray = [0 => 'Seleccione .. '] + $roleArray;

        $data = ['rolesList' => $roleArray];

        return view('users.create', $data);
    }

    public function store(UserRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();

        if (is_null($input['password']) || is_null($input['password_confirmation'])){
            \Log::warning('UsersController | Passwords dont match');
            return redirect()->back()->with('error', 'Debe introducir una contraseña');
        }else{
            if ($input['password'] != $input['password_confirmation']){
                \Log::warning('UsersController | Passwords dont match');
                return redirect()->back()->with('error', 'Las contraseñas no coinciden');
            }
        }

        array_key_exists('birthday', $input) ? $birthday = $input['birthday'] : $birthday = null;
        array_key_exists('address', $input) ? $address = $input['address'] : $address = null;
        array_key_exists('telephone', $input) ? $telephone = $input['telephone'] : $telephone = null;

        $input['can_get_commission'] == 1 ? $userCanCommission = true : $userCanCommission = false;


        $credentials = [
            'username' => $input['username'],
            'email' => $input['email'],
            'description' => $input['description'],
            'password' => $input['password'],
            'telephone' => $telephone,
            'address' => $address,
            'birthday' => $birthday,
            'can_get_commission' => $userCanCommission,
            'commission_percentage' => $input['commission_percentage']
        ];


        \Log::debug("UserController | Credentials Array: " . json_encode($credentials));
        try{
            if ($user = \Sentinel::registerAndActivate($credentials)) {
                $expectedPermissions = array_pull($input, 'permissions');
                $expectedPermissions = empty($expectedPermissions) ? [] : $expectedPermissions;

                foreach ($expectedPermissions as $p => $v) {
                    if ($v['inherited'] === '0') {
                        $user->addPermission($p, isset($v['state']) ? filter_var($v['state'],
                            FILTER_VALIDATE_BOOLEAN) : false);
                    }
                }

                if (!$user->save()) {
                    \Log::error('Cant update Users Permissions.', $input);
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error', 'Problemas al actualizar registro.');
                }
            }

            $expectedRoles = $input['role_id'];
            $expectedRoles = explode(',', $expectedRoles);

            if (!empty($expectedRoles)) {
                $user->roles()->attach($expectedRoles);
                Log::info('Roles agregados a Usuario: ' . $user->username, $expectedRoles);
            }

            return redirect()->route('users.index')
                ->with('success', 'Usuario creado exitosamente.');

        }catch (QueryException $e) {

            if ($e->getCode() == 23505) {
                \Log::error('Unique permission violation.',
                    ['action' => 'permissions.store', 'input' => $input, 'message' => $e->getMessage()]);

                return redirect()->back()
                    ->with('error', 'El Nombre de Usuario ya existe.');
            }

            \Log::warning($e->getMessage());
            \Log::warning("UsersControlller | Error while creating new user");
            return redirect()->back()->with('error', 'Error al intentar crear nuevo usuario');
        }

    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.show')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'no posee permisos para realizar esta accion.');
        }

        if ($user = User::find($id)) {
            Log::info("User {$id} find");
            return view('administration.users.show', compact('user'));
        }

        Log::warning("User not found");
        return redirect()->back()->with('error', 'Usuario no encontrado');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($user = User::with('roles')->find($id)) {

            $processedPermissions = $user->getProcessedPermissions()->all();

            $permissions = Permissions::orderBy('permission')->get();

            foreach ($permissions as $permission) {
                if (array_key_exists($permission->permission, $processedPermissions)) {
                    $permission->has = $processedPermissions[$permission->permission]['state'];
                    $permission->inherited = $processedPermissions[$permission->permission]['inherited'];
                } else {
                    $permission->has = null;
                    $permission->inherited = null;
                }
            }

            $roleArray = Role::all()->pluck('description', 'id')->toArray();
            $roleArray = [0 => 'Seleccione .. '] + $roleArray;

            $data = [
                'user' => $user,
                'rolesList' => $roleArray,
            ];
            return view('users.edit', $data);
        }

        return redirect()->back()->with('error', 'Usuario no encontrado.');

    }

    public function update(UserRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('users.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }


        // Get User with Roles
        if ($user = User::with('roles')->find($id)) {
            // Get the form input
            $input = $request->all();

            if ($input['role_id'] == 0){
                \Log::warning('UsersController | User witout assigned role');
                return redirect()->back()->with('error', 'Debe asignar un rol al usuario');
            }


            // Get the form input expected permissions
            // Permissions Rules:
            //  if state is null and inherited is 0 -> revoked permission user specific
            //  if state is true and inherited is 0 -> grant permission user specific
            //  if state is true and inherited is 1 -> already granted permission role inherited
            //  if state is null and inherited is 1 -> already revoked permission role inherited
            //  if state is null and inherited is null -> permission set in neither role nor user
            $expectedPermissions = array_pull($input, 'permissions');
            $expectedPermissions = empty($expectedPermissions) ? [] : $expectedPermissions;

            foreach ($expectedPermissions as $p => $v) {
                if (!isset($v['inherited']) OR $v['inherited'] === '0') {
                    $user->updatePermission($p, isset($v['state']) ? filter_var($v['state'], FILTER_VALIDATE_BOOLEAN) : false, true);
                }
            }

            if (!$user->save()) {
                \Log::error('Cant update Users Permissions.', $input);
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', 'Problemas al actualizar registro.');
            }
            \Log::debug('Permissions Saved!. ', $input);

            array_key_exists('birthday', $input) ? $birthday = $input['birthday'] : $birthday = null;
            array_key_exists('address', $input) ? $address = $input['address'] : $address = null;
            array_key_exists('telephone', $input) ? $telephone = $input['telephone'] : $telephone = null;

            $input['can_get_commission'] == 1 ? $userCanCommission = true : $userCanCommission = false;
            $credentials = [
                'username' => $input['username'],
                'email' => $input['email'],
                'description' => $input['description'],
                'telephone' => $telephone,
                'address' => $address,
                'birthday' => $birthday,
                'can_get_commission' => $userCanCommission,
                'commission_percentage' => $input['commission_percentage']

            ];


            if (!is_null($input['password'])){
                if ($input['password'] != $input['password_confirmation']){
                    \Log::warning('UsersController | Passwords dont match');
                    return redirect()->back()->with('error', 'Las contraseñas no coinciden');
                }
                $credentials['password'] = $input['password'];
            }



            // Get array of the User's current Roles IDs
            $currentRoles = [];

            if (!$user->roles->isEmpty())
                $currentRoles = explode(',', $user->roles->implode('id', ','));

            // Get arry of the User's expected Roles IDs
            $expectedRoles = explode(',', $request->get('role_id'));

            // Prepare array of Roles to detach from User
            if (!empty($currentRoles))
                $toDetachRoles = array_diff($currentRoles, $expectedRoles);

            if (!empty($toDetachRoles)) {
                $user->roles()->detach($toDetachRoles);
                Log::info('Roles eliminados de Usuario: ' . $user->username, $toDetachRoles);
            }

            // Prepare array of Roles to attach to User
            $toAttachRoles = array_diff($expectedRoles, $currentRoles);

            if (!empty($toAttachRoles)) {
                $user->roles()->attach($toAttachRoles);
                Log::info('Roles agregados a Usuario: ' . $user->username, $toAttachRoles);
            }


            // Update User with credentials
            //$user->update($credentials);
            try{
                \Log::debug(json_encode($credentials));
                $user = \Sentinel::update(\Sentinel::findById($id), $credentials);
                \Log::info("User {$user->username} updated successfully");
                return redirect()
                    ->route('users.index')
                    ->with('success', 'Usuario actualizado.');
            }catch (QueryException $e) {

                if ($e->getCode() == 23505) {
                    \Log::error('Unique username violation.',
                        ['action' => 'permissions.store', 'input' => $input, 'message' => $e->getMessage()]);

                    return redirect()->back()
                        ->with('error', 'El Nombre de Usuario ya existe.');
                }

                \Log::warning($e->getMessage());
                \Log::warning("UsersControlller | Error while creating new user");
                return redirect()->back()->with('error', 'Error al intentar crear nuevo usuario');
            }

        }

        return redirect()
            ->route('users.index')
            ->with('error', 'Error al actualizar el Usuario.');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('users.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        } else {
            if ($user = User::find($id)) {
                if (User::destroy($id) !== false) {
                    \Log::info('User destroy.', $user->toArray());
                    $message = 'Usuario eliminado correctamente';
                    $error = false;
                } else {
                    \Log::warning("Error while trying to destroy user: {$id}");
                    $message = 'Error al intentar eliminar el usuario';
                    $error = true;
                }
            } else {
                \Log::warning("User {$id} not found");
                $message = 'Usuario no encontrado';
                $error = true;
            }
        }
        $response = [
            'error' => $error,
            'message' => $message
        ];
        \Log::debug('Sending response. ' . json_encode($response));
        return response()->json($response);
    }

    public function activate($id, $code)
    {
        $user = \Sentinel::findUserById($id);
        \Log::info("UserController | New Activation Request");
        if (!\Activation::complete($user, $code)) {
            \Log::warning("User not activated {$user->username}");
            return redirect()->route('admin.login.page')->withErrors('Codigo de activacion inválido o expirado');
        }

        return redirect()->route('admin.login.page')->with('success', 'Cuenta de Usuario activada');
    }
}
