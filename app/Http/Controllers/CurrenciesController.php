<?php

namespace App\Http\Controllers;

use App\Http\Request\CurrenciesRequest;
use App\Models\Currencies;

class CurrenciesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('currencies')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $currencies = Currencies::paginate(20);

        return view('currencies.index', compact('currencies'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('currencies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('currencies.create');
    }

    public function store(CurrenciesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('currencies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if ($currency = Currencies::create($input)){
            \Log::debug("CurrenciesController | New Currency created");

            if (array_key_exists('icon', $input) && !is_null($input['icon'])){
                // TODO Guardamos Imagenes
            }

            return redirect()->route('currencies.index')->with('Registro ingresado exitosamente');
        }else{
            \Log::warning("CurrenciesController | Error on insert new currency");
            return redirect()->back()->with('error', 'Error al intentar ingresar el registro');
        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('currencies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($currency = Currencies::find($id)){
            return view('currencies.edit', compact('currency'));
        }else{
            \Log::warning("CurrenciesController | Currency {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function update(CurrenciesRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('currencies.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($currency = Currencies::find($id)){
            $input = $request->all();
            if ($currency->update($input)){
                if (array_key_exists('icon', $input) && !is_null($input['icon'])){
                    // TODO Guardamos Imagenes
                }
                return redirect()->route('currencies.index');
            }else{
                \Log::warning("CurrenciesController | Error on update currency {$id}");
                return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
            }
        }else{
            \Log::warning("CurrenciesController | Currency {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('currencies.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($currency = Currencies::find($id)) {
                try {
                    if (Currencies::destroy($id)) {
                        $message = 'Registro eliminado correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("CurrenciesController | Error deleting Currency: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("CurrenciesController | Currency {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
