<?php

namespace App\Http\Controllers;


use App\Http\Request\PricingHeaderRequest;
use App\Models\Branches;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\PricingDetails;
use App\Models\PricingHeader;
use App\Models\Products;
use App\Models\PurchasesDetails;
use Barryvdh\DomPDF\Facade as DPDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PricingListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('pricing')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['price_id'])) $where .= "pricing_header_id = '{$input['price_id']}' AND ";
            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $pricingHeaders = PricingDetails::orderBy('id', 'desc')->get();
            } else {
                $pricingHeaders = PricingDetails::orderBy('id', 'desc')->whereRaw($where)->get();
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('pricing.pdf', $pricingHeaders, 'ListaPrecio ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('pricing.excel', $pricingHeaders, 'ListaPrecio ' . date('Y-m-d'));
                }
            }
        }

        $pricingHeaders = [0 => 'Seleccione ..'];
        $pricingHeaders += PricingHeader::orderBy('id', 'asc')->pluck('description', 'id')->toArray();



        return view('pricing.index', compact('pricingHeaders'));
    }

    public function updateList(Request $request)
    {
        $message = '';
        $error = '';
        $data = [];

        if (!\Sentinel::getUser()->hasAccess('pricing.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $error = true;
            $message = 'No posee los permisos para realizar esta acción';
        } else {
            $input = $request->all();

            $products = PricingDetails::where('product_id', $input['product_id'])->get();
            if (!$products->isEmpty()) {
                foreach ($products as $list) {
                    $listName = "list_" . $list->pricing_header_id;
                    $details = PricingDetails::where('product_id', $list->product_id)->where('pricing_header_id', $list->pricing_header_id)->first();
                    if (!is_null($details)) {
                        \Log::debug("PricingListController | We get the header to update {$list->pricing_header_id} - Amount: {$input[$listName]}");
                        $details->selling_price = $input[$listName];
                        $details->update();
                    }
                }
                $error = false;
                $message = 'Registro modificado exitosamente';
                $data = [
                    'stock' => $products[0]->product->stock[0]->available,
                    'title' => $products[0]->product->title,
                ];
            } else {
                \Log::warning("PricingController | Missing products on price list");
                $error = true;
                $message = 'Producto no encontrado en la lista de precios';
            }
        }
        $response = [
            'error' => $error,
            'message' => $message,
            'data' => $data
        ];
        \Log::debug('Sending response. ' . json_encode($response));
        return response()->json($response);
    }


    public function salesProducts(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'product',
            2 => 'stock',
            3 => 'list_1',
            4 => 'list_2',
            5 => 'list_3',
            6 => 'purchase_price',
            8 => 'action'
        );

        $totalProducts = Products::count();
        $totalFiltered = $totalProducts;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $products = \DB::table('products')
                ->join('stocks', 'stocks.product_id', '=', 'products.id')
                ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                ->where("stocks.branch_id", "1")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $products = \DB::table('products')
                ->join('stocks', 'stocks.product_id', '=', 'products.id')
                ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                ->where("stocks.branch_id", "1")
                ->where('products.title', 'ILIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = \DB::table('products')
                ->join('stocks', 'stocks.product_id', '=', 'products.id')
                ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                ->where("stocks.branch_id", "1")
                ->where('products.title', 'LIKE', "%{$search}%")
                ->count('*');

        }

        \Log::info(json_encode($products));

        $data = array();
        if (!empty($products)) {
            foreach ($products as $product) {
                $nestedData['id'] = $product->id;
                $nestedData['product'] = $product->product;
                $nestedData['stock'] = $product->stock;
                $nestedData['list_1'] = $product->list_1;
                $nestedData['list_2'] = $product->list_2;
                $nestedData['list_3'] = $product->list_3;
                $nestedData['purchase_price'] = $product->purchase_price;
                $nestedData['quantity'] = '';
//                $nestedData['action'] = '<button class="btn btn-success btn-sm btn-success add_product" type="button"><i class="fa fa-cart-plus"></i> Agregar</button>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalProducts),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }

    private function downloadXLS($view, $data, $fileName)
    {
        return Excel::create($fileName, function ($excel) use ($data, $view) {
            $excel->sheet('Reporte', function ($excel) use ($data, $view) {
                $excel->loadView($view, compact('data'));
            });
        })->export('xls');
    }

    private function downloadPDF($view, $data, $fileName)
    {
        return DPDF::loadView($view, compact('data'))->download($fileName);
    }

}
