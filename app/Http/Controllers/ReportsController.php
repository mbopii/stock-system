<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\CashierMovements;
use App\Models\Cashiers;
use App\Models\Clients;
use App\Models\Companies;
use App\Models\Currencies;
use App\Models\InvoiceDetails;
use App\Models\InvoiceHeaders;
use App\Models\Products;
use App\Models\Categories;
use App\Models\Brands;
use App\Models\Providers;
use App\Models\PurchasesHeader;
use App\Models\Salesman;
use App\Models\SalesmanCommissions;
use App\Models\StockMovements;
use App\Models\User;
use App\Models\UserCommission;
use Barryvdh\DomPDF\Facade as DPDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Refunds;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('reports')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('reports.index');
    }

    public function sales(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('reports.sales')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        ini_set('memory_limit', '256M');
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "sale_date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "sale_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "sale_date= '{$input['date_end']}' AND ";
            }

            if (!is_null($input['company_id'])) {
                $where .= "company_id= '{$input['company_id']}' AND ";
            }

            if (!is_null($input['currency_id'])) {
                $where .= "currency_id= '{$input['currency_id']}' AND ";
            }

            if (!is_null($input['invoice_status']) && $input['invoice_status'] != 0) {
                $where .= "invoice_status= '{$input['invoice_status']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");
            if ($where == '') {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para su busqueda');
            } else {
                $salesHeader = InvoiceHeaders::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->paginate(15);
                $download = InvoiceHeaders::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->get();
                $sum = InvoiceHeaders::whereRaw($where)->sum(\DB::raw('total_amount::NUMERIC'));
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.sales', $download, 'ReporteVentas ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.sales', $download, 'ReporteVentas ' . date('Y-m-d'));
                }
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $company_id = isset($input['company_id']) ? $input['company_id'] : '';
            $invoice_status = isset($input['invoice_status']) ? $input['invoice_status'] : '';
            $currency_id = isset($input['currency_id']) ? $input['currency_id'] : '';

        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $company_id = '';
            $invoice_status = '';
            $currency_id = '';
            $salesHeader = InvoiceHeaders::orderBy('id', 'desc')
                ->where('invoice_status', '=', '1')
                ->paginate(30);
            $sum = InvoiceHeaders::where('invoice_status', '=', 1)->sum(\DB::raw('total_amount::NUMERIC'));
        }

        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $currenciesJson = json_encode(Currencies::all(['description', 'id']));

        return view('reports.sales', compact('salesHeader', 'date_end', 'date_start', 'q',
            'companiesJson', 'company_id', 'currenciesJson', 'invoice_status', 'sum', 'currency_id'));

    }

    public function daySales(Request $request)
    {
        $input = $request->all();
        $searchDate = 'CURRENT_DATE';
        $reportDate = date('Y-m-d');
        if (array_key_exists('date_start', $input)) {
            $searchDate = "'" . $input['date_start'] . "'";
            $reportDate = $input['date_start'];
        }
        $totalSale = InvoiceHeaders::whereRaw("created_at::date = {$searchDate} AND invoice_status = 1")->count();
        $productsOut = InvoiceDetails::join('invoice_headers', 'invoice_headers.id', '=', 'invoice_details.invoice_header_id')
            ->whereRaw("invoice_details.created_at::date = {$searchDate} AND invoice_status = 1")
            ->selectRaw("DISTINCT('product_id')")->count();
        $totalPurchases = PurchasesHeader::whereRaw("created_at::date = {$searchDate}")->count();
        $totalCollected = InvoiceHeaders::whereRaw("created_at::date = {$searchDate} AND invoice_status = 1")->selectRaw('sum(total_amount::int)')->first();
        $newClients = Clients::whereRaw("created_at::date = {$searchDate}")->count();
        $purchasesCount = PurchasesHeader::whereRaw("created_at::date = {$searchDate}")->count('*');
        $purchasesCollected = PurchasesHeader::whereRaw("created_at::date = {$searchDate}")->sum('total_amount');

        $companies = Companies::all()->except(5);
        $companiesArray = [];
        $i = 0;
        foreach ($companies as $company) {
            $companiesArray[$i]['name'] = $company->description;
            $companiesArray[$i]['amount'] = InvoiceHeaders::whereRaw("created_at::date = {$searchDate}")
                ->where('company_id', $company->id)
                ->selectRaw('sum(total_amount::int)')->first()->sum;
            $i++;
        }

        if (isset($input['download'])) {
            $data = [
                'date' => $searchDate,
                'companiesArray' => $companiesArray,
                'totalSale' => $totalSale,
                'totalPurchases' => $totalPurchases,
                'totalCollected' => $totalCollected,
                'newClients' => $newClients,
                'purchasesCollected' => $purchasesCollected,
                'productsOut' => $productsOut,
                'purchasesCount' => $purchasesCount,
                'reportDate' => $reportDate,
            ];
            if ($input['download'] == 'pdf') {
                return $this->downloadPDF('reports.pdf.day_sales', $data, 'VentasDelDia ' . date('Y-m-d') . '.pdf');
            }

            if ($input['download'] == 'xls') {
                $this->downloadXLS('reports.excel.day_sales', $data, 'VentasDelDia ' . date('Y-m-d'));
            }
        }

        return view('reports.day_sales', compact('totalSale', 'productsOut', 'totalPurchases',
            'totalCollected', 'newClients', 'purchasesCollected', 'companiesArray', 'purchasesCount', 'reportDate'));
    }

    public function purchases(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "purchase_date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "purchase_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "purchase_date= '{$input['date_end']}' AND ";
            if (!is_null($input['provider_id']) && $input['provider_id'] != 0) $where .= "provider_id = {$input['provider_id']} AND ";
            if (!is_null($input['invoice_number']) && $input['invoice_number'] != 0) $where .= "invoice_number = {$input['invoice_number']} AND ";
            if (!is_null($input['branch_id']) && $input['branch_id'] != 0) $where .= "sale_status_id = {$input['branch_id']} AND ";
            if (!is_null($input['payment_form_id']) && $input['payment_form_id'] != 0) $where .= "payment_form_id = {$input['payment_form_id']} AND ";
            if (!is_null($input['invoice_status']) && $input['invoice_status'] != 0) $where .= "invoice_status = {$input['invoice_status']} AND ";
            if (!is_null($input['company_id'])) $where .= "company_id= '{$input['company_id']}' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para su busqueda');
            } else {
                $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->paginate(15);
                $download = PurchasesHeader::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->get();
                $sum = PurchasesHeader::whereRaw($where)->sum(\DB::raw('total_amount::NUMERIC'));
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.purchases', $download, 'ReporteCompras ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.purchases', $download, 'ReporteCompras ' . date('Y-m-d'));
                }
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $provider_id = isset($input['provider_id']) ? $input['provider_id'] : '';
            $invoice_number = isset($input['invoice_number']) ? $input['invoice_number'] : '';
            $branch_id = isset($input['branch_id']) ? $input['branch_id'] : '';
            $payment_form_id = isset($input['payment_form_id']) ? $input['payment_form_id'] : '';
            $invoice_status = isset($input['invoice_status']) ? $input['invoice_status'] : '';
            $company_id = isset($input['company_id']) ? $input['company_id'] : '';

            $q = $input['q'];

        } else {
            $date_start = '';
            $date_end = '';
            $provider_id = '';
            $invoice_number = '';
            $branch_id = '';
            $q = '';
            $payment_form_id = '';
            $invoice_status = '';
            $company_id = '';
            $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')->paginate(15);
            $sum = PurchasesHeader::sum(\DB::raw('total_amount::NUMERIC'));
        }
        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $providersJson = json_encode(Providers::all('description', 'id'));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all('description', 'id'));
        } else {
            $branchesJson = '';
        }


        return view('reports.purchases', compact('purchasesHeader', 'providersJson', 'branchesJson',
            'date_start', 'date_end', 'provider_id', 'invoice_number', 'branch_id', 'q', 'payment_form_id',
            'invoice_status', 'sum', 'companiesJson', 'company_id'));

    }

    public function productHistory(Request $request)
    {
        ini_set('max_execution_time', 50000);
        $input = $request->all();
        $query = [];
        if (array_key_exists('q', $input) && $input['q'] != '') {
            if (is_null($input['product_id'])) {
                return redirect()->back()->with('error', 'Debe elegir un producto para continuar');
            }
            $where = '';

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date between '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (!is_null($input['product_id'])) {
                $where .= "product_id= {$input['product_id']} AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if (isset($input['download'])) {
                $query = StockMovements::whereRaw($where)->orderBy('id', 'desc')->get();
            } else {
                $query = StockMovements::whereRaw($where)->orderBy('id', 'desc')->paginate(20);
            }
            $i = 0;
            $arrayResponse = [];
            // Armamos el cuadro
            foreach ($query as $q) {
                switch ($q->movement_type_id) {
                    case 1 : // Compra
                        $info = $q->purchaseInfo;
                        $details = $q->purchaseInfo->purchaseDetails;
                        break;
                    case 2: // Cancelacion de Compra
                        $info = $q->purchaseInfo;
                        $details = $q->purchaseInfo->purchaseDetails;
                        break;
                    case 3: // Venta
                        $info = $q->saleInfo;
                        $details = $q->saleInfo->invoiceDetails;
                        break;
                    case 4: // Devolucion
                        $info = $q->saleInfo;
                        $details = $q->saleInfo->invoiceDetails;
                        break;
                    case 5: // Ajuste de Stock
                        $info = $q->adjustmentInfo;
                        $details = 0;
                        break;
                    default:
                        $info = null;
                        $details = 0;
                }
                if (($details instanceof \Illuminate\Database\Eloquent\Collection) && $details->count() >= 1) {
                    foreach ($details as $detail) {
                        if ($detail->product_id == $q->product_id) {
                            $arrayResponse[$i] = [
                                'transaction_date' => $q->created_at,
                                'product' => $q->productInfo->title,
                                'invoice_number' => $info->invoice_number,
                                'quantity' => $q->quantity,
                                'client' => isset($info->provider_id) ? $info->provider->description : $info->client->description,
                                'movement_type' => $q->movementType->description,
                                'price' => isset($detail->price) ? $detail->price : $detail->single_amount,
                                'currency' => Currencies::find($detail->currency_id)->description,
                                'fee' => isset($detail->currency_change) ? $detail->currency_change : $detail->change_fee,
                                'before' => $q->stock_before,
                                'after' => $q->stock_after,
                                'reason' => '---'
                            ];
                            $i++;
                        }
                    }
                } else {
                    $arrayResponse[$i] = [
                        'transaction_date' => $q->created_at,
                        'product' => $q->productInfo->title,
                        'invoice_number' => '---',
                        'quantity' => $q->quantity,
                        'client' => '---',
                        'movement_type' => $q->movementType->description,
                        'price' => 0,
                        'currency' => '---',
                        'fee' => 0,
                        'before' => $q->stock_before,
                        'after' => $q->stock_after,
                        'reason' => !is_null($q->adjustmentInfo) ? $q->adjustmentInfo->reason : '---'
                    ];
                    $i++;
                }
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.product_history', $arrayResponse, 'ReporteHistoralProducto ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.product_history', $arrayResponse, 'ReporteHistoralProducto ' . date('Y-m-d'));
                }
            }

            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $product_id = isset($input['product_id']) ? $input['product_id'] : '';
            $final['data'] = $arrayResponse;
            $final['query'] = $query;
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $product_id = '';
            $final = null;
        }
        $productsJson = json_encode(Products::all('title', 'id'));
        return view('reports.product_history', compact('productsJson', 'final', 'date_end', 'date_start', 'q', 'product_id'));
    }

    public function productsByProvider(Request $request)
    {
        $input = $request->all();

        $arrayResponse = [];
        $i = 0;
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "purchase_date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "purchase_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "purchase_date= '{$input['date_end']}' AND ";
            }

            if (!is_null($input['product_id'])) {
                $where .= "purchases_details.product_id = {$input['product_id']} AND ";
            }

            if (!is_null($input['provider_id'])) {
                $where .= "purchase_header.provider_id = {$input['provider_id']} AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $responseArray = \DB::table('purchases_details')
                    ->join('purchase_header', 'purchase_header.id', '=', 'purchases_details.purchase_header_id')
                    ->join('products', 'products.id', '=', 'purchases_details.product_id')
                    ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                    ->join('stocks', 'stocks.product_id', '=', 'products.id')
                    ->join('currencies', 'currencies.id', '=', 'purchases_details.currency_id')
                    ->select('products.title', 'providers.description', 'purchases_details.quantity', 'stocks.available',
                        'purchases_details.single_amount', 'purchase_header.purchase_date', 'currencies.description as currency')
                    ->whereRaw($where)
                    ->orderBy('purchases_details.id', 'desc')
                    ->paginate(20);

                $download = \DB::table('purchases_details')
                    ->join('purchase_header', 'purchase_header.id', '=', 'purchases_details.purchase_header_id')
                    ->join('products', 'products.id', '=', 'purchases_details.product_id')
                    ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                    ->join('stocks', 'stocks.product_id', '=', 'products.id')
                    ->join('currencies', 'currencies.id', '=', 'purchases_details.currency_id')
                    ->select('products.title', 'providers.description', 'purchases_details.quantity', 'stocks.available',
                        'purchases_details.single_amount', 'purchase_header.purchase_date', 'currencies.description as currency')
                    ->whereRaw($where)
                    ->orderBy('purchases_details.id', 'desc')
                    ->get();

            } else {
                return redirect()->back()->with('error', 'Debe seleccionar al menos un filtro de busqueda');
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.products_by_provider', $download, 'ProductosPorProveedor ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.products_by_provider', $download, 'ProductosPorProveedor ' . date('Y-m-d'));
                }
            }

            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $provider_id = isset($input['provider_id']) ? $input['provider_id'] : '';
            $product_id = isset($input['product_id']) ? $input['product_id'] : '';

        } else {
            $responseArray = \DB::table('purchases_details')
                ->join('purchase_header', 'purchase_header.id', '=', 'purchases_details.purchase_header_id')
                ->join('products', 'products.id', '=', 'purchases_details.product_id')
                ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                ->join('stocks', 'stocks.product_id', '=', 'products.id')
                ->join('currencies', 'currencies.id', '=', 'purchases_details.currency_id')
                ->select('products.title', 'providers.description', 'purchases_details.quantity', 'stocks.available',
                    'purchases_details.single_amount', 'purchase_header.purchase_date', 'currencies.description as currency')
                ->orderBy('purchases_details.id', 'desc')
                ->paginate(20);
            $product_id = "";
            $provider_id = "";
            $date_start = "";
            $date_end = "";
            $q = "";
        }

        $productsJson = json_encode(Products::all('title', 'id'));
        $providersJson = json_encode(Providers::all('description', 'id'));

        return view('reports.products_by_provider', compact('productsJson', 'providersJson', 'responseArray', 'product_id', 'provider_id',
            'date_start', 'date_end', 'q'));
    }

    public function clientsWithCredit(Request $request)
    {
        $input = $request->all();

        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "invoice_headers.sale_date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "invoice_headers.sale_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "invoice_headers.sale_date= '{$input['date_end']}' AND ";
            }

            if (!is_null($input['sale_type_id'])) {
                $where .= "invoice_headers.sale_type_id = {$input['sale_type_id']} AND ";
            }

            if (!is_null($input['client_id'])) {
                $where .= "clients.id = {$input['client_id']} AND ";
            }

            //$where .= "invoice_headers.sale_type_id = 2 AND ";
            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");
            if ($where) {
                $invoices = \DB::table('invoice_headers')
                    ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
                    ->join('statuses', 'invoice_headers.invoice_status', '=', 'statuses.id')
                    ->selectRaw("invoice_headers.id as id, clients.description as client, invoice_headers.invoice_number,
                invoice_headers.sale_date, invoice_headers.total_amount, invoice_headers.credit_days, invoice_headers.amount_left, invoice_headers.total_discount, statuses.description")
                    ->whereRaw($where)
                    ->where('invoice_status', '=', '1')
                    ->orderBy('invoice_headers.id', 'desc')
                    ->paginate(20);

                $download = \DB::table('invoice_headers')
                    ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
                    ->join('statuses', 'invoice_headers.invoice_status', '=', 'statuses.id')
                    ->selectRaw("invoice_headers.id as id, clients.description as client, invoice_headers.invoice_number,
                invoice_headers.sale_date, invoice_headers.total_amount, invoice_headers.credit_days, invoice_headers.amount_left, invoice_headers.total_discount, statuses.description")
                    ->whereRaw($where)
                    ->where('invoice_status', '=', '1')
                    ->orderBy('invoice_headers.id', 'desc')
                    ->get();

                $sum = \DB::table('invoice_headers')
                    ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
                    ->selectRaw("invoice_headers.id as id, clients.description as client, invoice_headers.invoice_number,
                invoice_headers.sale_date, invoice_headers.total_amount, invoice_headers.credit_days, invoice_headers.amount_left, invoice_headers.total_discount")
                    ->whereRaw($where)
                    ->where('invoice_status', '=', '1')
                    ->sum(\DB::raw('invoice_headers.total_amount::numeric'));
            } else {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para realizar la busqueda');
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.clients_with_credit', $download, 'ClientesConCredito ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.clients_with_credit', $download, 'ClientesConCredito ' . date('Y-m-d'));
                }
            }

            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $sale_type_id = isset($input['sale_type_id']) ? $input['sale_type_id'] : '';
            $client_id = isset($input['client_id']) ? $input['client_id'] : '';
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $client_id = '';
            $sale_type_id = '';
            $invoices = \DB::table('invoice_headers')
                ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
                ->join('statuses', 'invoice_headers.invoice_status', '=', 'statuses.id')
                ->selectRaw("invoice_headers.id as id, clients.description as client, invoice_headers.invoice_number,
                invoice_headers.sale_date, invoice_headers.total_amount, invoice_headers.credit_days, invoice_headers.amount_left, invoice_headers.total_discount, statuses.description")
                //->where('invoice_headers.sale_type_id', '=', '2')
                ->where("invoice_headers.invoice_status", '=', '1')
                ->orderBy('invoice_headers.id', 'desc')
                ->paginate(20);

            $sum = \DB::table('invoice_headers')
                ->join('clients', 'clients.id', '=', 'invoice_headers.client_id')
                ->selectRaw("invoice_headers.id as id, clients.description as client, invoice_headers.invoice_number,
                invoice_headers.sale_date, invoice_headers.total_amount, invoice_headers.credit_days, invoice_headers.amount_left, invoice_headers.total_discount")
                //->where('invoice_headers.sale_type_id', '=', '2')
                ->where("invoice_headers.invoice_status", '=', '1')
                ->sum(\DB::raw('invoice_headers.total_amount::numeric'));
        }

        $clientsJson = json_encode(Clients::select(['description', 'id'])->get());
        return view('reports.clients_with_credit', compact('invoices', 'date_start', 'date_end', 'q', 'client_id', 'sale_type_id', 'clientsJson',
            'sum'));
    }

    public function providersWithCredit(Request $request)
    {
        $input = $request->all();

        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "purchase_header.purchase_date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "purchase_header.purchase_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "purchase_header.purchase_date= '{$input['date_end']}' AND ";
            }

            if (!is_null($input['provider_description'])) {
                $where .= "providers.description ILIKE '%{$input['provider_description']}%' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $purchases = \DB::table('purchase_header')
                    ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                    ->selectRaw("purchase_header.id as id, providers.description as client, purchase_header.invoice_number,
                    purchase_header.purchase_date, purchase_header.total_amount, purchase_header.credit_days, purchase_header.amount_left")
                    ->whereRaw($where)
                    ->orderBy('purchase_header.id', 'desc')
                    ->paginate(10);

                $download = \DB::table('purchase_header')
                    ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                    ->selectRaw("purchase_header.id as id, providers.description as client, purchase_header.invoice_number,
                purchase_header.purchase_date, purchase_header.total_amount, purchase_header.credit_days, purchase_header.amount_left")
                    ->whereRaw($where)
                    ->orderBy('purchase_header.id', 'desc')
                    ->get();
            } else {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para realizar la busqueda');
            }

            if (isset($input['download'])) {
                if ($input['download'] == 'pdf') {
                    return $this->downloadPDF('reports.pdf.providers_with_credit', $download, 'ComprasACredito ' . date('Y-m-d') . '.pdf');
                }

                if ($input['download'] == 'xls') {
                    $this->downloadXLS('reports.excel.providers_with_credit', $download, 'ComprasACredito ' . date('Y-m-d'));
                }
            }

            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $provider_description = isset($input['provider_description']) ? $input['provider_description'] : '';
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $provider_description = '';
            $purchases = \DB::table('purchase_header')
                ->join('providers', 'providers.id', '=', 'purchase_header.provider_id')
                ->selectRaw("purchase_header.id as id, providers.description as client, purchase_header.invoice_number,
            purchase_header.purchase_date, purchase_header.total_amount, purchase_header.credit_days, purchase_header.amount_left")
                ->orderBy('purchase_header.id', 'desc')
                ->paginate(10);

        }
        return view('reports.providers_with_credit', compact('purchases', 'date_start', 'date_end', 'q', 'provider_description'));
    }

    public function allProducts(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['product_id'])) {
                $where .= "id= '{$input['product_id']}' AND ";
            }

            if (!is_null($input['brand_id'])) {
                $where .= "brand_id = '{$input['brand_id']}' AND ";
            }

            if (!is_null($input['category_id'])) {
                $where .= "category_id = '{$input['category_id']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $products = Products::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = Products::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();

                if (isset($input['download'])) {
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.products', $download, 'ReporteProductos ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.products', $download, 'ReporteProductos ' . date('Y-m-d'));
                    }
                }
                $product_id = isset($input['product_id']) ? $input['product_id'] : '';
                $brand_id = isset($input['brand_id']) ? $input['brand_id'] : '';
                $q = isset($input['q']) ? $input['q'] : '';;
            } else {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para realizar la busqueda');
            }
        } else {
            $product_id = '';
            $brand_id = '';
            $q = '';
            $products = Products::paginate(10);
        }

        $productsJson = json_encode(Products::all(['title', 'id']));
        $brandsJson = json_encode(Brands::all(['description', 'id']));
        $categoriesJson = json_encode(Categories::all(['description', 'id']));

        return view('reports.products', compact('product_id', 'brand_id', 'q', 'products', 'brandsJson', 'productsJson', 'categoriesJson'));
    }

    public function pricing(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['brand_id'])) {
                $where .= "products.brand_id= '{$input['brand_id']}' AND ";
            }

            if (!is_null($input['category_id'])) {
                $where .= "products.category_id = '{$input['category_id']}' AND ";
            }

            if (!is_null($input['description'])) {
                $where .= "products.title ILIKE '%{$input['description']}%' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $pricing = $products = \DB::table('products')
                    ->join('stocks', 'stocks.product_id', '=', 'products.id')
                    ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                    ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                    ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                    ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                    ->where("stocks.branch_id", "1")
                    ->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = $products = \DB::table('products')
                    ->join('stocks', 'stocks.product_id', '=', 'products.id')
                    ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                    ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                    ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                    ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                    ->where("stocks.branch_id", "1")
                    ->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();

                if (isset($input['download'])) {
                    foreach ($download as $p) {
                        $nestedData['id'] = $p->id;
                        $nestedData['product'] = $p->product;
                        $nestedData['stock'] = $p->stock;
                        $nestedData['list_1'] = $p->list_1;
                        $nestedData['list_2'] = $p->list_2;
                        $nestedData['list_3'] = $p->list_3;
                        $nestedData['purchase_price'] = $p->purchase_price;
                        $data[] = $nestedData;
                        $nestedData = [];
                    }
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.pricing', $data, 'ListaPrecios ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.pricing', $data, 'ListaPrecios ' . date('Y-m-d'));
                    }
                }
                $category_id = isset($input['category_id']) ? $input['category_id'] : '';
                $description = isset($input['description']) ? $input['description'] : '';
                $brand_id = isset($input['brand_id']) ? $input['brand_id'] : '';
                $q = isset($input['q']) ? $input['q'] : '';;
            } else {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para realizar la busqueda');
            }
        } else {
            $category_id = '';
            $brand_id = '';
            $description = '';
            $q = '';
            $pricing = $products = \DB::table('products')
                ->join('stocks', 'stocks.product_id', '=', 'products.id')
                ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
                ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
                ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
                ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1, 
        b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price')
                ->where("stocks.branch_id", "1")
                ->orderBy('id', 'desc')
                ->paginate(10);
        }

        $data = [];
        foreach ($pricing as $p) {
            $nestedData['id'] = $p->id;
            $nestedData['product'] = $p->product;
            $nestedData['stock'] = $p->stock;
            $nestedData['list_1'] = $p->list_1;
            $nestedData['list_2'] = $p->list_2;
            $nestedData['list_3'] = $p->list_3;
            $nestedData['purchase_price'] = $p->purchase_price;
            $data['data'][] = $nestedData;
        }
        $data['paginate'] = $pricing;
        $categoriesJson = json_encode(Categories::all(['description', 'id']));
        $brandsJson = json_encode(Brands::all(['description', 'id']));

        return view('reports.pricing', compact('category_id', 'brand_id', 'description', 'q', 'data', 'brandsJson', 'categoriesJson'));
    }

    public function refunds(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['product_id'])) {
                $where .= "product_id = '{$input['product_id']}' AND ";
            }

            if (!is_null($input['invoice_number'])) {
                $invoice = InvoiceHeaders::whereRaw("invoice_number ILIKE '%{$input['invoice_number']}%'")->first();
                if (!is_null($invoice)) {
                    $where .= "invoice_id = {$invoice->id} AND ";
                }
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $refunds = Refunds::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = Refunds::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();

                if (isset($input['download'])) {
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.refunds', $download, 'ReporteDevolucion ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.refunds', $download, 'ReporteDevolucion ' . date('Y-m-d'));
                    }
                }
                $product_id = isset($input['product_id']) ? $input['product_id'] : '';
                $invoice_number = isset($input['invoice_number']) ? $input['invoice_number'] : '';
                $q = isset($input['q']) ? $input['q'] : '';;
            } else {
                return redirect()->back()->with('error', 'Debe añadir algun filtro para realizar la busqueda');
            }
        } else {
            $product_id = '';
            $invoice_number = '';
            $q = '';
            $refunds = Refunds::paginate(10);
        }

        $productsJson = json_encode(Products::all(['title', 'id']));

        return view('reports.refunds', compact('product_id', 'invoice_number', 'q', 'refunds', 'productsJson'));
    }

    public function getSalesmanCommissions(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['salesman_id'])) {
                $where .= "salesman_id = '{$input['salesman_id']}' AND ";
            }

            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "created_at::date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date= '{$input['date_end']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where) {
                $commissions = SalesmanCommissions::where('canceled', false)->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = SalesmanCommissions::where('canceled', false)->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();

                if (isset($input['download'])) {
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.salesman_commissions', $download, 'ReporteComisiones ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.salesman_commissions', $download, 'ReporteComisiones ' . date('Y-m-d'));
                    }
                }
                $date_start = isset($input['date_start']) ? $input['date_start'] : '';
                $date_end = isset($input['date_end']) ? $input['date_end'] : '';
                $q = $input['q'];
                $salesman_id = isset($input['salesman_id']) ? $input['salesman_id'] : '';
            }
        } else {
            $q = '';
            $commissions = SalesmanCommissions::where('canceled', false)->orderBy('id', 'desc')->paginate(20);
            $date_start = '';
            $date_end = '';
            $salesman_id = '';
        }
        $salesmanJson = json_encode(Salesman::all('name', 'id'));

        return view('reports.commissions', compact('commissions', 'q', 'salesmanJson',
            'salesman_id', 'date_end', 'date_start'));
    }

    public function getUsersCommissions(Request $request)
    {
        $input = $request->all();
        $sum = 0;
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['user_id'])) {
                $where .= "user_id = '{$input['user_id']}' AND ";
            }

            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "created_at::date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date= '{$input['date_end']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where != '') {
                $commissions = UserCommission::where('canceled', false)->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = UserCommission::where('canceled', false)->whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();

                $sum = UserCommission::where('canceled', false)->whereRaw($where)->sum('amount');

                if (isset($input['download'])) {
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.user_commissions', $download, 'ReporteComisionesUsuario ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.user_commissions', $download, 'ReporteComisionesUsuario ' . date('Y-m-d'));
                    }
                }

            } else {
                return redirect()->back()->with('error', 'Debe elegir un valor para el filtrado');
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $user_id = isset($input['user_id']) ? $input['user_id'] : '';
        } else {
            $q = '';
            $commissions = UserCommission::where('canceled', false)->orderBy('id', 'desc')->paginate(20);
            $date_start = '';
            $date_end = '';
            $user_id = '';
            $sum = UserCommission::where('canceled', false)->sum('amount');
        }
        $userJson = json_encode(User::all('description', 'id'));

        return view('reports.user_commissions', compact('commissions', 'q', 'userJson',
            'user_id', 'date_end', 'date_start', 'sum'));
    }

    public function cashierStatus(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['cashier_id'])) {
                $where .= "cashier_id = '{$input['cashier_id']}' AND ";
            }

            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "created_at::date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "created_at::date= '{$input['date_end']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where != '') {
                $movements = CashierMovements::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                $download = CashierMovements::whereRaw($where)
                    ->orderBy('id', 'desc')
                    ->get();
                if (isset($input['download'])) {
                    if ($input['download'] == 'pdf') {
                        return $this->downloadPDF('reports.pdf.cashiers', $download, 'ReporteMovimientoCaja ' . date('Y-m-d') . '.pdf');
                    }

                    if ($input['download'] == 'xls') {
                        $this->downloadXLS('reports.excel.cashiers', $download, 'ReporteMovimientoCaja ' . date('Y-m-d'));
                    }
                }
                $date_start = isset($input['date_start']) ? $input['date_start'] : '';
                $date_end = isset($input['date_end']) ? $input['date_end'] : '';
                $q = $input['q'];
                $cashier_id = isset($input['cashier_id']) ? $input['cashier_id'] : '';
            } else {
                return redirect()->back()->with('error', 'Debe elegir un valor para el filtrado');
            }
        } else {
            $q = '';
            $date_start = '';
            $date_end = '';
            $cashier_id = '';
            $movements = CashierMovements::orderBy('id', 'desc')->paginate(20);
        }
        $cashierJson = Cashiers::all(['name', 'id']);
        return view('reports.cashiers', compact('cashierJson', 'movements', 'q', 'date_start', 'date_end', 'cashier_id'));
    }

    public function array_sort($array, $on, $order = SORT_ASC)
    {

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    private
    function downloadXLS($view, $data, $fileName)
    {
        return Excel::create($fileName, function ($excel) use ($data, $view) {
            $excel->sheet('Reporte', function ($excel) use ($data, $view) {
                $excel->loadView($view, compact('data'));
            });
        })->export('xls');
    }

    private
    function downloadPDF($view, $data, $fileName)
    {
        return DPDF::loadView($view, compact('data'))->download($fileName);
    }
}
