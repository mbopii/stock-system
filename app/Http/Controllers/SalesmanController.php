<?php

namespace App\Http\Controllers;

use App\Http\Request\SalesmanRequest;
use App\Models\Salesman;
use App\Models\MetricsUnits;
use Illuminate\Http\Request;

class SalesmanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('salesman')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['name'])) $where .= "name ILIKE '%{$input['name']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $salesman = Salesman::orderBy('id', 'desc')->paginate(20);

            } else {
                $salesman = Salesman::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $name = isset($input['name']) ? $input['name'] : '';
            $q = $input['q'];
        }else{
            $salesman = Salesman::orderBy('id', 'desc')->paginate(20);
            $name ='';
            $q = '';
        }

        return view('salesman.index', compact('salesman','name', 'q'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('salesman.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('salesman.create');
    }

    public function store(SalesmanRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('salesman.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!Salesman::create($request->all())){
            \Log::warning("SalesmanController | Error while trying to save new category");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('salesman.index')->with('success', 'Registro creado exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('salesman.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$salesman = Salesman::find($id)){
            \Log::warning("SalesmanController | Category {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view('salesman.edit', compact('salesman'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('salesman.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Salesman::find($id)){
            \Log::warning("SalesmanController | Category {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        if (!$category->update($request->all())){
            \Log::warning("SalesmanController | Error on update category id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('salesman.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('salesman.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($category = Salesman::find($id)) {
                try {
                    if (Salesman::destroy($id)) {
                        $message = 'Registro eliminado correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("SalesmanController | Error deleting Category: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("SalesmanController | Category {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
