<?php

namespace App\Http\Controllers;

use App\Http\Request\BranchesRequest;
use App\Models\Branches;

class BranchesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('branches')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $branches = Branches::paginate(20);

        return view('branches.index', compact('branches'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('branches.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('branches.create', compact('create'));
    }

    public function store(BranchesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('branches.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if ($newBranch = Branches::create($input)) {
            \Log::info("BranchesController | Branch {$newBranch->description} created successfully");
            return redirect()->route('branches.index')->with('success', 'Registro creado exitosamente');
        } else {
            \Log::warning('BranchesController | Error while trying to save a new branch');
            return redirect()->back()->with('error', 'Ocurrio un error al intentar agregar un nuevo registro');
        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('branches.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($branch = Branches::find($id)){
            \Log::info("BranchesController | Branch {$branch->description} ready to edit");
            return view('branches.edit', compact('branch'));
        }else{
            \Log::warning("BranchesController | Branch {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function update(BranchesRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('branches.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($branch = Branches::find($id)){
            \Log::info("BranchesController | Branch {$branch->description} ready to update");
            $input = $request->all();
            if ($branch->update($input)){
                \Log::info("BranchesController | Branch {$id} updated");
                return redirect()->route('branches.index')->with('success', 'Registro actualizado Correctamente');
            }else{
                \Log::warning("BranchesController | Cant update branch -> {$branch->title}");
                return redirect()->back()->with('error', 'Registro no encontrado');
            }
        }else{
            \Log::warning("BranchesController | Branch {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('branches.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($branch = Branches::find($id)) {
                try {
                    if (Branches::destroy($id)) {
                        $message = 'Registro eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("BranchesController | Error deleting Branch : " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("BranchesController | Branch {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }

        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
