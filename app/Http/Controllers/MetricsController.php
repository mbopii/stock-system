<?php

namespace App\Http\Controllers;

use App\Http\Request\MetricsUnitsRequest;
use App\Models\MetricsUnits;

class MetricsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('metrics')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $metricsUnits = MetricsUnits::paginate(20);

        return view('metrics.index', compact('metricsUnits'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('metrics.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('metrics.create');
    }

    public function store(MetricsUnitsRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('brands.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!MetricsUnits::create($input)){
            \Log::warning("MetricsController | Error while trying to save new metricUnit");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('metrics.index')->with('success', 'Unidad creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('metrics.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($metricUnit = MetricsUnits::find($id)){
            \Log::info("MetricsController | Edit unit: {$id}");
            return view('metrics.edit', compact('metricUnit'));
        }else{
            \Log::warning("MetricsController | Unit: {$id} not found");
            return redirect()->back()->with('error', 'Unidad no encontrada');
        }
    }

    public function update(MetricsUnitsRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('metrics.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!$brand = MetricsUnits::find($id)){
            \Log::warning("MetricsController | Unit {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }


        if (!$brand->update($input)){
            \Log::warning("MetricsController | Error on update Unit id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('metrics.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('metrics.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($metric = MetricsUnits::find($id)) {
                try {
                    if (MetricsUnits::destroy($id)) {
                        $message = 'Unidad eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("MetricsController | Error deleting Unit: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("MetricsController | Metric {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
