<?php

namespace App\Http\Controllers;

use App\Http\Request\PaymentsFormRequest;
use App\Models\PaymentsForm;

class PaymentsFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('payments_forms')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $paymentsForm = PaymentsForm::paginate(20);

        return view('payments_form.index', compact('paymentsForm'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('payments_forms.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('payments_form.create');
    }

    public function store(PaymentsFormRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('payments_forms.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!PaymentsForm::create($input)){
            \Log::warning("PaymentsFormsController | Error while trying to save new metricUnit");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('payments_forms.index')->with('success', 'Forma de Pago creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('payments_forms.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($payment_form = PaymentsForm::find($id)){
            \Log::info("PaymentsFormsController | Edit payment: {$id}");
            return view('payments_form.edit', compact('payment_form'));
        }else{
            \Log::warning("PaymentsFormsController | payment: {$id} not found");
            return redirect()->back()->with('error', 'Forma de pago no encontrada');
        }
    }

    public function update(PaymentsFormRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('payments_forms.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($payment_form = PaymentsForm::find($id)){
            $input = $request->all();
            if (!$payment_form->update($input)){
                \Log::warning("PaymentsFormsController | Error on update Unit id {$id}");
                return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
            }

            return redirect()->route('payments_forms.index')->with('success', 'Registro actualizado exitosamente');
        }else{
            \Log::warning("PaymentsFormsController | payment: {$id} not found");
            return redirect()->back()->with('error', 'Forma de pago no encontrada');
        }
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('payments_forms.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($paymentsForm = PaymentsForm::find($id)) {
                try {
                    if (PaymentsForm::destroy($id)) {
                        $message = 'Registro eliminado correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("PaymentsFormsController | Error deleting Unit: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("PaymentsFormsController | Metric {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
