<?php

namespace App\Http\Controllers;


use App\Http\Request\PurchasesRequest;
use App\Models\Branches;
use App\Models\Cashiers;
use App\Models\Companies;
use App\Models\Currencies;
use App\Models\PaymentsForm;
use App\Models\PricingDetails;
use App\Models\PricingHeader;
use App\Models\Products;
use App\Models\Providers;
use App\Models\PurchasesDetails;
use App\Models\PurchasesHeader;
use App\Models\StockMovements;
use App\Models\Stocks;
use App\Models\Banks;
use App\Models\PaymentMethods;
use App\Models\VoucherPurchases;
use Illuminate\Http\Request;

class PurchasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "purchase_date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "purchase_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "purchase_date= '{$input['date_end']}' AND ";
            if (!is_null($input['provider_id']) && $input['provider_id'] != 0) $where .= "provider_id = {$input['provider_id']} AND ";
            if (!is_null($input['invoice_number']) && $input['invoice_number'] != 0) $where .= "invoice_number = {$input['invoice_number']} AND ";
            if (!is_null($input['branch_id']) && $input['branch_id'] != 0) $where .= "sale_status_id = {$input['branch_id']} AND ";
            if (!is_null($input['payment_form_id']) && $input['payment_form_id'] != 0) $where .= "payment_form_id = {$input['payment_form_id']} AND ";
            if (!is_null($input['invoice_status']) && $input['invoice_status'] != 0) $where .= "invoice_status = {$input['invoice_status']} AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')
                    ->paginate(50);
            } else {
                $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->paginate(50);
            }

            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $provider_id = isset($input['provider_id']) ? $input['provider_id'] : '';
            $invoice_number = isset($input['invoice_number']) ? $input['invoice_number'] : '';
            $branch_id = isset($input['branch_id']) ? $input['branch_id'] : '';
            $payment_form_id = isset($input['payment_form_id']) ? $input['payment_form_id'] : '';
            $invoice_status = isset($input['invoice_status']) ? $input['invoice_status'] : '';
            $q = $input['q'];

        } else {
            $date_start = '';
            $date_end = '';
            $provider_id = '';
            $invoice_number = '';
            $branch_id = '';
            $q = '';
            $payment_form_id = '';
            $invoice_status = '';
            $purchasesHeader = PurchasesHeader::orderBy('id', 'desc')->paginate(20);
        }

        $providersJson = json_encode(Providers::all('description', 'id'));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all('description', 'id'));
        } else {
            $branchesJson = '';
        }


        return view('purchases.index', compact('purchasesHeader', 'providersJson', 'branchesJson',
            'date_start', 'date_end', 'provider_id', 'invoice_number', 'branch_id', 'q', 'payment_form_id', 'invoice_status'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (\Session::exists('details')) {
            \Session::pull('details');
        }

        $providers = Providers::all(['description', 'id']);
        $currencies = Currencies::all(['description', 'id']);
        $paymentsForm = PaymentsForm::all(['description', 'id']);
        $products = Products::all(['title', 'id']);
        $companiesJson = json_encode(Companies::all(['description', 'id']));

        if ($providers->isEmpty()) {
            \Log::warning("PurchasesController | Missing providers");
            return redirect()->route('providers.create')->with('error', 'Debe agregar al menos un proveedor');
        }

        if ($currencies->isEmpty()) {
            \Log::warning("PurchasesController | Missing Currencies");
            return redirect()->route('currencies.create')->with('error', 'Debe agregar al menos una moneda');
        }

        if ($paymentsForm->isEmpty()) {
            \Log::warning("PurchasesController | Missing Payments Forms");
            return redirect()->route('payments_forms.create')->with('error', 'Debe agregar al menos una forma de pago');
        }

        if ($products->isEmpty()) {
            \Log::warning("PurchasesController | Missing Products");
            return redirect()->route('produccts.create')->with('error', 'Debe agregar al menos un producto');
        }
        $providersJson = json_encode($providers);
        $paymentsFormJson = json_encode($paymentsForm);
        $currencyJson = json_encode($currencies);
        $productsJson = json_encode($products);
        $cashierJson = json_encode(Cashiers::all(['name', 'id']));

        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all(['description', 'id']));
        } else {
            $branchesJson = '{}';
        }
        return view('purchases.create', compact('providersJson', 'paymentsFormJson', 'currencyJson',
            'branchesJson', 'productsJson', 'companiesJson', 'cashierJson'));
    }

    public function store(PurchasesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        $currency = Currencies::find($input['currency_id']);

        if (is_null($currency)) {
            \Log::warning("PurchasesController | Error while getting currencies");
            return redirect()->back()->with('error', 'Moneda no encotrada');
        }

        $headerArray = [
            'purchase_date' => $input['purchase_date'],
            'total_amount' => 0,
            'invoice_number' => $input['invoice_number'],
            'stamping' => $input['stamping'],
            'provider_id' => $input['provider_id'],
            'currency_id' => $input['currency_id'],
            'payment_form_id' => $input['payment_form_id'],
            'branch_id' => $input['branch_id'],
            'user_id' => \Sentinel::getUser()->id,
            'currency_change' => $currency->selling_price,
            'company_id' => $input['company_id'],
            'credit_days' => isset($input['credit_days']) ? $input['credit_days'] : 0,
            'amount_left' => 0
        ];

        // Create Headers
        if ($header = PurchasesHeader::create($headerArray)) {
            // We need the details
            $totalAmount = 0;
            for ($i = 0; $i <= count($input['purchase_product_id']) - 1; $i++) {
                $totalAmount += $input['purchase_quantity'][$i] * $input['purchase_price'][$i];
                $detailArray = [
                    'purchase_header_id' => $header->id,
                    'product_id' => $input['purchase_product_id'][$i],
                    'quantity' => $input['purchase_quantity'][$i],
                    'single_amount' => $input['purchase_price'][$i],
                    'total_amount' => $input['purchase_quantity'][$i] * $input['purchase_price'][$i],
                    'currency_change' => $currency->selling_price,
                    'currency_id' => $currency->id
                ];

                if ($input['purchase_discount'][$i] != 0 ) {
                    $newPrice = $input['purchase_price'][$i] - ($input['purchase_price'][$i] * $input['purchase_discount'][$i] / 100);
                    $detailArray['special_price'] = $newPrice;
                    $detailArray['special_subtotal'] = $newPrice * $input['purchase_quantity'][$i];
                    $detailArray['subtotal_discount'] = ($input['purchase_quantity'][$i] * $input['purchase_price'][$i]) - ($newPrice * $input['purchase_quantity'][$i]) ;
                } else {
                    $detailArray['special_price'] = 0;
                    $detailArray['special_subtotal'] = 0;
                    $detailArray['subtotal_discount'] = 0;
                }

                $newPurchaseDetail = PurchasesDetails::create($detailArray);

                $productStock = Stocks::where('product_id', $input['purchase_product_id'][$i])
                    ->where('branch_id', $header->branch_id)->first();

                if (!is_null($productStock)) {
                    $currentStockAvailable = $productStock->available;
                    $productStock->available = $input['purchase_quantity'][$i] + $productStock->available;
                    $productStock->save();
                } else {
                    $currentStockAvailable = 0;
                }

                $newPurchaseDetail->stock_before = $currentStockAvailable;
                $newPurchaseDetail->stock_after = $productStock->available;
                $newPurchaseDetail->save();

                // Actualizamos la nueva tabla de movimientos para este producto
                $stockMovement = [
                    'product_id' => $input['purchase_product_id'][$i],
                    'movement_type_id' => 1, // Compra,
                    'movement_id' => $header->id,
                    'quantity' => $input['purchase_quantity'][$i],
                    'stock_before' => $currentStockAvailable,
                    'stock_after' => $currentStockAvailable + $input['purchase_quantity'][$i]
                ];

                StockMovements::create($stockMovement);

                // Modificar el precio de costo en las listas de precios!
                $priceList = PricingDetails::where('product_id', '=', $input['purchase_product_id'][$i])->get();
                foreach ($priceList as $list) {
                    if ($input['purchase_discount'][$i] != 0 ) {
                        $singleAmount = str_replace(',', '.', $input['purchase_price'][$i] - ($input['purchase_price'][$i] * $input['purchase_discount'][$i] / 100));
                        // Multiplicamos por la cotización del día
                        $list->purchase_price = round($singleAmount * $header->currency->selling_price, 0, PHP_ROUND_HALF_UP);
                        $list->profit = $list->selling_price - $singleAmount;
                    } else {
                        $singleAmount = str_replace(',', '.', $input['purchase_price'][$i]);
                        // Multiplicamos por la cotización del día
                        $list->purchase_price = round($singleAmount * $header->currency->selling_price, 0, PHP_ROUND_HALF_UP);
                        $list->profit = $list->selling_price - $singleAmount;
                    }

                    if ($list->selling_price != 0) {
                        $list->profit_percentage = round((($list->selling_price - $singleAmount) / $list->selling_price) * 100, 2, PHP_ROUND_HALF_DOWN);
                    }
                    $list->update();
                }
            }

            $header->total_amount = $totalAmount;
            if ($input['payment_form_id'] == 2) {
                $header->amount_left = $totalAmount;
            }
            if ($input['payment_form_id'] == 1) {
                // Agregamos a la plata que sale a la caja
                $cashier = Cashiers::where('id', $input['cashier_id'])->first();
                $cashier_movement = $cashier->lastMovement()->where('currency_id', 1)->where('opened_at', date('Y-m-d'))->first();
                $cashier_movement->closing_amount = $cashier_movement->closing_amount - $totalAmount;
                $cashier_movement->save();
            }
            $header->save();
            return redirect()->route('purchases.index')->with('success', 'Compra creada exitosamente');
        } else {
            \Log::warning("PurchasesController | Error on creating a new purchase header");
            return redirect()->back()->with('error', 'Error al intentar crear el registro');
        }
    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$header = PurchasesHeader::find($id)) {
            \Log::warning("PurchasesHeader | Invoice id {$id} not found");
            return redirect()->back()->with("error", 'Factura no encontrada');
        }


        return view("purchases.show", compact('header'));
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($purchase = PurchasesHeader::find($id)) {
            $providers = Providers::all(['description', 'id']);
            $currencies = Currencies::all(['description', 'id']);
            $paymentsForm = PaymentsForm::all(['description', 'id']);
            $products = Products::all(['title', 'id']);

            if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
                $branchesJson = json_encode(Branches::all(['description', 'id']));
            } else {
                $branchesJson = '';
            }

            $providersJson = json_encode($providers);
            $paymentsFormJson = json_encode($paymentsForm);
            $currencyJson = json_encode($currencies);
            $productsJson = json_encode($products);

            $detailArray = [];
            $i = 0;

            foreach ($purchase->purchaseDetails as $detail) {
                $detailArray[$i]['product_id'] = $detail->product_id;
                $detailArray[$i]['quantity'] = $detail->quantity;
                $detailArray[$i]['price'] = $detail->single_amount;
                $i++;
            }

            \Session::pull('details');
            \Session::put('details', $detailArray);
            return view('purchases.edit', compact('purchase', 'productsJson', 'providersJson', 'currencyJson',
                'paymentsFormJson', 'branchesJson'));
        } else {
            \Log::warning("PurchasesController | Header not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function update(PurchasesRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('purchases.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($purchase = PurchasesHeader::find($id)) {
            $input = $request->all();
            $inputSession = \Session::get('details');
            \Session::pull('details');
            $input['user_id'] = \Sentinel::getUser()->id;
            $purchase->update($input);


            foreach ($inputSession as $detail) {
                $details = PurchasesDetails::where('purchase_header_id', $purchase->id)
                    ->where('product_id', $detail['product_id'])
                    ->first();

                if (!is_null($details)) {
                    $details->quantity = $detail['quantity'];
                    $details->single_amount = $detail['price'];
                    $details->total_amount = $detail['price'] * $detail['quantity'];
                    $details->save();
                } else {
                    // New one
                    $detailsToSave = [
                        'purchase_header_id' => $purchase->id,
                        'product_id' => $detail['product_id'],
                        'quantity' => $detail['quantity'],
                        'single_amount' => $detail['price'],
                        'total_amount' => $detail['price'] * $detail['quantity']
                    ];
                    PurchasesDetails::create($detailsToSave);
                }
            }

            return redirect()->route('purchases.index')->with('success', 'Compra editada correctamente');

        } else {
            \Log::warning("PurchasesController | Header not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
    }

    public function destroy($id)
    {
        // NOt really destroy the invoice, we really cancel the invoice and return the goods to our stock
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('sales.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($purchase = PurchasesHeader::find($id)) {
                if ($purchase->invoice_status == 2) {
                    \Log::warning("PurchasesController | Invoice already canceled");
                    $error = true;
                    $message = 'Esta factura ya ha sido anulada';
                } else {
                    try {

                        $purchase->invoice_status = 2;
                        $purchase->save();

                        // Return stock to deposit - Only one branch ... for now;
                        foreach ($purchase->purchaseDetails as $detail) {
                            $stock = Stocks::where('product_id', $detail->product_id)
                                ->where("branch_id", 1)
                                ->first();
                            $currentStockAvailable = $stock->available;
                            $stock->available = $stock->available - $detail->quantity;
                            $stock->save();

                            // Actualizamos la nueva tabla de movimientos para este producto
                            $stockMovement = [
                                'product_id' => $detail->product_id,
                                'movement_type_id' => 2, // Cancelacion de Compra,
                                'movement_id' => $purchase->id,
                                'quantity' => $detail->quantity,
                                'stock_before' => $currentStockAvailable,
                                'stock_after' => $currentStockAvailable - $detail->quantity
                            ];

                            StockMovements::create($stockMovement);

                        }
                        // Confirmation Messages
                        $error = false;
                        $message = 'Compra anulada exitosamente';

                    } catch (\Exception $e) {
                        \Log::warning("SalesController | Error cancel Order : " . $e->getMessage());
                        $message = 'Error al intentar eliminar el registro';
                        $error = true;
                    }
                }


            } else {
                \Log::warning("SalesController | Sale {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }

        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);

    }

    public function showRegisterVoucher($id)
    {
        if ($purchase = PurchasesHeader::find($id)) {
            $banksJson = json_encode(Banks::all(['description', 'id']));
            $paymentMethodsJson = json_encode(PaymentMethods::all(['description', 'id']));
            return view('purchases.voucher', compact('purchase', 'banksJson', 'paymentMethodsJson'));
        } else {
            \Log::warning("PurchasesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function registerVoucherPayment($id, Request $request)
    {
        if ($purchase = PurchasesHeader::find($id)) {
            $input = $request->all();

            $newVoucherArray = [
                'purchase_header_id' => $id,
                'voucher_number' => $input['voucher_number'],
                'amount' => $input['amount'],
                'payment_id' => $input['payment_id']
            ];

            if (isset($input['check_number'])) {
                $newVoucherArray['check_number'] = $input['check_number'];
                $newVoucherArray['bank_id'] = $input['bank_id'];
            }

            VoucherPurchases::create($newVoucherArray);

            // Editamos la fila de la factura para registrar los pagos
            $purchase->amount_left = $purchase->amount_left - $input['amount'];
            $purchase->save();

            return redirect()->route('purchases.index')->with('success', 'Recibo cargado exitosamente');
        } else {
            \Log::warning("PurchasesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function cancelVoucher($id)
    {
        if ($voucher = VoucherPurchases::find($id)) {
            \Log::info("PurchasesController | Deleting voucher id: {$id}");
            $voucher->status_id = 2;
            $voucher->save();
            // Devolvemos el dinero a la cuenta
            $purchase = PurchasesHeader::find($voucher->purchase_header_id);
            $purchase->amount_left = $purchase->amount_left + $voucher->amount;
            $purchase->save();

            return redirect()->back()->with('success', 'Recibo anulado exitosamente');
        }

        return redirect()->back()->with('error', 'Registro no encontrado');
    }

    public function paymentVouchers()
    {
        $creditPurchases = PurchasesHeader::where('payment_form_id', '=', 2)
            ->where('invoice_status', '=', 1)
            ->orderBy('id', 'desc')->get();
        if (!$creditPurchases->isEmpty()) {
            $i = 0;
            $arrayResponse = [];
            foreach ($creditPurchases as $purchase) {
                $arrayResponse[$i] = [
                    'id' => $purchase->id,
                    'provider' => $purchase->provider->description,
                    'company' => $purchase->companies->description,
                    'purchase_date' => $purchase->purchase_date,
                    'invoice_number' => $purchase->invoice_number,
                    'amounts' => $purchase->total_amount . " / " . $purchase->amount_left,
                    'created_at' => $purchase->created_at->format('Y-m-d'),
                    'options' => ''
                ];
                $i++;
            }
            uasort($arrayResponse, function ($a, $b) {
                return ($a['id'] < $b['id']) ? 1 : -1;
            });
            $creditPurchasesJson = json_encode($arrayResponse);
            return view('purchases.payment_vouchers', compact('creditPurchasesJson'));
        }

        return redirect()->back()->with('error', 'No existen facturas a credito disponibles');
    }

    public function paymentVouchersConfirmation(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('payment', $input)){
            $arrayResponse = [];
            $i = 0;
            $amountLeft = 0;
            foreach ($input['payment'] as $p) {
                $purchase = PurchasesHeader::find($p);

                $arrayResponse[$i] = [
                    'id' => $purchase->id,
                    'provider' => $purchase->provider->description,
                    'company' => $purchase->companies->description,
                    'purchase_date' => $purchase->purchase_date,
                    'invoice_number' => $purchase->invoice_number,
                    'amounts' => $purchase->amount_left,
                    'created_at' => $purchase->created_at->format('Y-m-d'),
                    'options' => ''
                ];
                $amountLeft = $amountLeft + $purchase->amount_left;
                $i++;
            }
            $banksJson = json_encode(Banks::all(['description', 'id']));
            $paymentMethodsJson = json_encode(PaymentMethods::all(['description', 'id']));
            return view('purchases.payment_voucher_confirmation', compact('arrayResponse', 'banksJson', 'paymentMethodsJson', 'amountLeft'));
        }

        return redirect()->back()->with('error', 'Debe seleccionar facturas a pagar');
    }

    public function paymentVouchersConfirmationSubmit (Request $request)
    {
        $input = $request->all();
        if ($input['amount'] == $input['total_amount']) {
            foreach ($input['purchase_id'] as $purchase_id) {
                $purchase = PurchasesHeader::find($purchase_id);
                $newVoucherArray = [
                    'purchase_header_id' => $purchase_id,
                    'voucher_number' => $input['voucher_number'],
                    'amount' => $purchase->amount_left,
                    'payment_id' => $input['payment_id']
                ];

                if (isset($input['check_number'])) {
                    $newVoucherArray['check_number'] = $input['check_number'];
                    $newVoucherArray['bank_id'] = $input['bank_id'];
                }

                VoucherPurchases::create($newVoucherArray);

                // Editamos la fila de la factura para registrar los pagos
                $purchase->amount_left = 0;
                $purchase->save();
            }

            return redirect()->route('purchases.index')->with('success', 'Pagos ingresados correctamente');
        } else {
            \Log::warning("PurchasesController | Amounts dont match");
            return redirect()->back()->with('error', 'Las cantidades no coinciden');
        }
    }


}
