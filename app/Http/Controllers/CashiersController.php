<?php

namespace App\Http\Controllers;

use App\Models\CashierMovements;
use App\Models\Cashiers;
use App\Models\CashierWithdraws;
use App\Models\Currencies;
use Illuminate\Http\Request;

class CashiersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('cashiers')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['name'])) $where .= "name ILIKE '%{$input['name']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $cashiers = Cashiers::orderBy('id', 'desc')->paginate(20);
            } else {
                $cashiers = Cashiers::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $name = isset($input['name']) ? $input['name'] : '';
            $q = $input['q'];
        } else {
            $cashiers = Cashiers::orderBy('id', 'desc')->paginate(20);
            $name = '';
            $q = '';
        }

        return view('cashiers.index', compact('cashiers', 'name', 'q'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('cashiers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('cashiers.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('cashiers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $name = $request->get('name');
        if (empty($name)) {
            \Log::warning("CashiersController | Missing name");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $arrayToSave = [
            'name' => $name
        ];

        if (!Cashiers::create($arrayToSave)) {
            \Log::warning("CashiersController | Error while trying to save new brand");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('cashiers.index')->with('success', 'Marca creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('cashiers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$cashier = Cashiers::find($id)) {
            \Log::warning("CashiersController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

        return view('cashiers.edit', compact('cashier'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('cashiers.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$brand = Cashiers::find($id)) {
            \Log::warning("CashiersController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

        $name = $request->get('name');
        if (empty($name)) {
            \Log::warning("CashiersController | Missing name");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $brand->name = $name;

        if (!$brand->save()) {
            \Log::warning("CashiersController | Error on update brand id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('cashiers.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('cashiers.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($brand = Cashiers::find($id)) {
                try {
                    if (Cashiers::destroy($id)) {
                        $message = 'Marca eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("CashiersController | Error deleting Brand: " . $e->getMessage());
                    $message = 'Error al intentar eliminar la marca';
                    $error = true;
                }
            } else {
                \Log::warning("CashiersController | Brand {$id} not found");
                $message = 'Marca no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

    public function toggleCashier($id)
    {
        $cashier = Cashiers::find($id);

        return view('cashiers.toggle', compact('cashier'));
    }

    public function doToggleCashier($id, Request $request)
    {
        $cashier = Cashiers::find($id);

        $input = $request->except('_token');

        $currencies = Currencies::all();
        $open = false;
        foreach ($currencies as $currency) {
            $arrayToSave['cashier_id'] = $cashier->id;
            // Traemos el último movimiento y controlamos las fechas
            $cashier_movement = CashierMovements::where('currency_id', $currency->id)->latest();
            if (is_null($cashier_movement) || !$cashier->is_open || $cashier_movement->opened_at != date('Y-m-d')){ // No podemos tener el mismo cajero con 2 fechas diferentes abiertos
                // We open the cashier
                $arrayToSave['open_by'] = \Sentinel::getUser()->id;
                $arrayToSave['currency_id'] = $currency->id;
                $arrayToSave['opening_amount'] = $input['amount_' . $currency->symbol] == '' ? 0 : $input['amount_' . $currency->symbol];
                $arrayToSave['opened_at'] = date('Y-m-d');
                $arrayToSave['opened_time'] = date('H:i');
                CashierMovements::create($arrayToSave);
                $open = true;
            } else {
                // We close the cashier
                $cashier_movement->update(
                    [
                        'closed_by' => \Sentinel::getUser()->id,
                        'closing_amount' => $input['amount_' . $currency->symbol] == '' ? 0 : $input['amount_' . $currency->symbol],
                        'closed_at' => date('Y-m-d'),
                        'closed_time' => date('H:i'),
                    ]
                );
                $open = false;
            }
        }

        $cashier->is_open = $open;
        $cashier->save();

        return redirect('cashiers')->with('success', 'Registro creado exitosamente');
    }

    public function cashierExtraction($id)
    {
        $cashier = Cashiers::find($id);

        return view('cashiers.show_extraction', compact('cashier'));
    }

    public function cashierExtractionDo($id, Request $request)
    {
        $cashier = Cashiers::find($id);

        if (!$cashier->is_open)
            return redirect()->back()->with('error', 'Debe tener una caja abierta para continuar');

        $currentMovement = $cashier->lastMovement()->where('currency_id', $request->get('currency_id'))->where('opened_at', date('Y-m-d'))->first();
        $arrayToSave['cashier_movement_id'] = $currentMovement->id;
        $arrayToSave['amount_withdraw'] = $request->get('amount');
        $arrayToSave['amount_before_withdraw'] = $currentMovement->closing_amount;
        $arrayToSave['amount_after_withdraw'] = $currentMovement->closing_amount - $request->get('amount');
        $arrayToSave['withdraw_by'] = \Sentinel::getUser()->id;
        $arrayToSave['reason'] = $request->get('reason');

        CashierWithdraws::create($arrayToSave);

        $currentMovement->closing_amount = $currentMovement->closing_amount - $request->get('amount');
        $currentMovement->save();

        return redirect('cashiers')->with('success', 'Registro generado exitosamente');
    }

}
