<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\LogStocks;
use App\Models\StockMovements;
use App\Models\Stocks;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('stocks')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['title'])) $where .= "title ILIKE '%{$input['title']}%' AND ";
            if (!is_null($input['category_id']) && $input['category_id'] != 0) $where .= "category_id = {$input['category_id']} AND ";
            if (!is_null($input['brand_id']) && $input['brand_id'] != 0) $where .= "brand_id = {$input['brand_id']} AND ";
            if (!is_null($input['branch_id']) && $input['branch_id'] != 0) $where .= "branch_id = 1 AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $stocks = \DB::table('stocks')
                    ->join('products', 'stocks.product_id', '=', 'products.id')
                    ->join("branches", 'stocks.branch_id', '=', 'branches.id')
                    ->join("categories", 'categories.id', '=', 'products.category_id')
                    ->select('stocks.id', 'products.title', 'branches.description', 'stocks.available', 'stocks.max_quantity',
                        'stocks.min_quantity')
                    ->orderBy('stocks.id', 'desc')
                    ->paginate(20);
            } else {
                $stocks = \DB::table('stocks')
                    ->join('products', 'stocks.product_id', '=', 'products.id')
                    ->join("branches", 'stocks.branch_id', '=', 'branches.id')
                    ->join("categories", 'categories.id', '=', 'products.category_id')
                    ->select('stocks.id', 'products.title', 'branches.description', 'stocks.available', 'stocks.max_quantity',
                        'stocks.min_quantity')
                    ->orderBy('stocks.id', 'desc')
                    ->whereRaw($where)
                    ->paginate(20);
            }

            $title = isset($input['title']) ? $input['title'] : '';
            $category_id = isset($input['category_id']) ? $input['category_id'] : '';
            $branch_id = isset($input['branch_id']) ? $input['branch_id'] : '';
            $brand_id = isset($input['brand_id']) ? $input['brand_id'] : '';
            $q = $input['q'];
        }else{
            $stocks = \DB::table('stocks')
                ->join('products', 'stocks.product_id', '=', 'products.id')
                ->join("branches", 'stocks.branch_id', '=', 'branches.id')
                ->join("categories", 'categories.id', '=', 'products.category_id')
                ->select('stocks.id', 'products.title', 'branches.description', 'stocks.available', 'stocks.max_quantity',
                    'stocks.min_quantity')
                ->orderBy('stocks.id', 'desc')
                ->paginate(20);

            $title ='';
            $q = '';
            $category_id = '';
            $branch_id = '';
            $brand_id = '';
        }

        $categoriesJson = json_encode(Categories::all('description', 'id'));
        $brandsJson = json_encode(Brands::all('description', 'id'));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')){
            $branchesJson = json_encode(Branches::all('description', 'id'));
        }else{
            $branchesJson = '';
        }


        return view('stocks.index', compact('stocks', 'categoriesJson', 'brandsJson', 'branchesJson',
            'title', 'q', 'category_id', 'brand_id', 'branch_id'));
    }

    public function stockAdjustment(Request $request)
    {
        $input = $request->all();
        if ($stock = Stocks::find($input['stock_id'])){
            // Actualizar la tabla log
            $logArray = [
                'user_id' =>  \Sentinel::getUser()->id,
                'stock_id' => $stock->id,
                'current_available' => $stock->available,
                'new_available' => $input['new_stock'],
                'reason' => $input['observation']
            ];
            $currentStockAvailable = $stock->available;

            $log = LogStocks::create($logArray);

            // Actualizar el stock
            $stock->available = $input['new_stock'];
            $stock->save();

            // Actualizamos la nueva tabla de movimientos para este producto
            $stockMovement = [
                'product_id' => $stock->product_id,
                'movement_type_id' => 5, // Ajuste de Stock
                'movement_id' => $log->id,
                'quantity' =>  $input['new_stock'],
                'stock_before' => $currentStockAvailable,
                'stock_after' => $input['new_stock']
            ];

            StockMovements::create($stockMovement);

            return json_encode(['error' => false]);
        }else{
            \Log::warning("StocksController | Stock for update not found");
            return json_encode(['error' => true]);

        }


    }
}