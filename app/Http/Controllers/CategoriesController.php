<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\MetricsUnits;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('categories')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['description'])) $where .= "description ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $categories = Categories::orderBy('id', 'desc')->paginate(20);

            } else {
                $categories = Categories::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        }else{
            $categories = Categories::orderBy('id', 'desc')->paginate(20);
            $description ='';
            $q = '';
        }

        return view('categories.index', compact('categories','description', 'q'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('categories.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("CategoriesController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $arrayToSave = [
            'description' => $description,
            'commission_percentage' => $request->get('commission_percentage')
        ];

        if (!Categories::create($arrayToSave)){
            \Log::warning("CategoriesController | Error while trying to save new category");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('categories.index')->with('success', 'Registro creado exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Categories::find($id)){
            \Log::warning("CategoriesController | Category {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Categories::find($id)){
            \Log::warning("CategoriesController | Category {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("CategoriesController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $category->description = $description;
        $category->commission_percentage = $request->get('commission_percentage');
        if (!$category->save()){
            \Log::warning("CategoriesController | Error on update category id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('categories.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('categories.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($category = Categories::find($id)) {
                try {
                    if (Categories::destroy($id)) {
                        $message = 'Registro eliminado correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("CategoriesController | Error deleting Category: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("CategoriesController | Category {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }
}
