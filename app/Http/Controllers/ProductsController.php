<?php

namespace App\Http\Controllers;

use App\Http\Request\ProductsRequest;
use App\Models\Branches;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\Companies;
use App\Models\MetricsUnits;
use App\Models\PricingDetails;
use App\Models\PricingHeader;
use App\Models\Products;
use App\Models\PurchasesDetails;
use App\Models\Stocks;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['findProductDetails']]);
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('products')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['title'])) $where .= "title ILIKE '%{$input['title']}%' AND ";
            if (!is_null($input['category_id']) && $input['category_id'] != 0) $where .= "category_id = {$input['category_id']} AND ";
            if (!is_null($input['brand_id']) && $input['brand_id'] != 0) $where .= "brand_id = {$input['brand_id']} AND ";
            if (!is_null($input['active']) && $input['active'] != 0) {
                if ($input['active'] == 1) $where .= "active = true AND "; else $where .= "active = false AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $products = Products::orderBy('id', 'desc')->paginate(20);
            } else {
                $products = Products::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $title = isset($input['title']) ? $input['title'] : '';
            $q = $input['q'];
            $category_id = isset($input['category_id']) ? $input['category_id'] : '';
            $brand_id = isset($input['brand_id']) ? $input['brand_id'] : '';
            $active = isset($input['active']) ? $input['active'] : '';
        } else {
            $products = Products::orderBy('id', 'desc')->paginate(20);
            $title = '';
            $q = '';
            $category_id = '';
            $brand_id = '';
            $active = '';
        }
        $categoriesJson = json_encode(Categories::all('description', 'id'));
        $brandsJson = json_encode(Brands::all('description', 'id'));

        return view('products.index', compact('products', 'title', 'q', 'categoriesJson', 'brandsJson',
            'category_id', 'brand_id', 'active'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $categoriesJson = json_encode(Categories::all(['description', 'id']));
        $metricsUnitJson = json_encode(MetricsUnits::all(['description', 'id']));
        $brandsJson = json_encode(Brands::all(['description', 'id']));
        $branch_id = null;

        return view('products.create', compact('categoriesJson', 'metricsUnitJson', 'brandsJson'));
    }

    public function store(ProductsRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if ($newProduct = Products::create($input)) {
            \Log::info("ProductsController | Product {$newProduct->title} created successfully");

            $stockArray = [
                'product_id' => $newProduct->id,
                'available' => 0,
                'max_quantity' => $input['max_quantity'],
                'min_quantity' => $input['min_quantity'],
                'branch_id' => 1
            ];

//            $branches = Branches::all();
//            foreach ($branches as $branch){
//                $stockArray['branch_id'] = $branch->id;
            Stocks::create($stockArray); // TODO this is for 1 branch only - After we make the transfer
            // TODO section we can enable this shit
//            }

            $priceListing = PricingHeader::all();
            $productArray = [
                'product_id' => $newProduct->id,
                'selling_price' => 0,
                'purchase_price' => 0,
                'profit' => 0,
                'profit_percentage' => 0
            ];
            foreach ($priceListing as $priceListHeader) {
                $productArray['pricing_header_id'] = $priceListHeader->id;
                PricingDetails::create($productArray);
                \Log::debug("ProductsController | Product added to price list -> {$priceListHeader->title}");
            }

            return redirect()->route('products.index')->with('success', 'Producto creado exitosamente');
        } else {
            \Log::warning('ProductsController | Error while trying to save a new product');
            return redirect()->back()->with('error', 'Ocurrio un error al intentar agregar un nuevo registro');
        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($product = Products::find($id)) {
            \Log::debug("ProductsController | Edit Product -> {$product->title}");
            $stock = Stocks::where('product_id', $product->id)->first();
            $product->min_quantity = $stock->min_quantity;
            $product->max_quantity = $stock->max_quantity;

            $categoriesJson = json_encode(Categories::all(['description', 'id']));
            $metricsUnitJson = json_encode(MetricsUnits::all(['description', 'id']));
            $brandsJson = json_encode(Brands::all('description', 'id'));

            return view('products.edit', compact('product', 'categoriesJson', 'metricsUnitJson', 'brandsJson'));
        } else {
            \Log::warning("ProductsController | Product {$id} not found");
            return redirect()->back()->with('error', 'Producto no encontrado');
        }
    }

    public function update(ProductsRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('products.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($product = Products::find($id)) {
            \Log::debug("ProductsController | Updating Product -> {$product->title}");
            $input = $request->all();
            $input['active'] == 1 ? $input['active'] = true : $input['active'] = false;
            if ($product->update($input)) {
                \Log::info("ProductsController | Product {$id} updated");
                $stock = Stocks::where('product_id', $product->id)->get();
                foreach ($stock as $s) {
                    $s->max_quantity = $input['max_quantity'];
                    $s->min_quantity = $input['min_quantity'];
                    $s->save();
                }

                return redirect()->route('products.index')->with('success', 'Registro actualizado Correctamente');
            } else {
                \Log::warning("ProductsController | Cant update product -> {$product->title}");
                return redirect()->back()->with('error', 'Producto no encontrado');
            }
        } else {
            \Log::warning("ProductsController | Product {$id} not found");
            return redirect()->back()->with('error', 'Producto no encontrado');
        }
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('products.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($provider = Products::find($id)) {
                try {
                    $stock = Stocks::where('product_id', $id)->get();
                    if ($stock->isNotEmpty()) {
                        $message = 'NO puede eliminar productos con stock cargado en el sistema';
                        $error = true;
                    } else {
                        if (Products::destroy($id)) {
                            $message = 'Registro eliminada correctamente';
                            $error = false;
                        }
                    }

                } catch (\Exception $e) {
                    \Log::warning("ProductsController | Error deleting Provider : " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("ProductsController | Provider {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }

        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

    public function findProductDetails($id)
    {
        if (!isset($id)) {
            \Log::warning("PurchasesController | Missing product id");
            return json_encode(['error' => 'true', 'Message' => 'Debe elegir un producto']);
        }

        $product = Products::find($id);

        $lastPrice = PurchasesDetails::where('product_id', $id)
            ->select('single_amount')
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();

        is_null($lastPrice) ? $price = 0 : $price = $lastPrice->single_amount;

        \Log::debug($lastPrice);

        return json_encode(['purchase_price' => $price, 'name' => $product->title]);
    }

    public function findSellProductsDetails($id, $list_id)
    {
        if (!isset($id) || !isset($list_id)) {
            \Log::warning("PurchasesController | Missing product id or list_id");
            return json_encode(['error' => 'true', 'message' => 'Debe elegir un producto']);
        }

        if ($product = Products::find($id)) {
            $priceList = PricingDetails::where("product_id", $product->id)
                ->where('pricing_header_id', $list_id)
                ->first();

            if (!$priceList) {
                \Log::warning("PurchasesController | Missing Pricing details");
                return json_encode(['error' => 'true', 'message' => 'Precio no encontrado']);
            }

            $arrayReturn = [
                'price' => $priceList->selling_price,
                'product_name' => $product->title,
                'error' => false,
                'message' => 'Precio encontrado'
            ];

            return json_encode($arrayReturn);

        } else {
            \Log::warning("PurchasesController | Product {$id} not found");
            return json_encode(['error' => 'true', 'message' => 'Producto no encontrado']);
        }
    }

}
