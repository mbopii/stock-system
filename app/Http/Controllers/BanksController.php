<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use Illuminate\Http\Request;

class BanksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('banks')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['description'])) $where .= "description ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $banks = Banks::orderBy('id', 'desc')->paginate(20);
            } else {
                $banks = Banks::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        }else{
            $banks = Banks::orderBy('id', 'desc')->paginate(20);
            $description ='';
            $q = '';
        }
        return view('banks.index', compact('banks', 'description', 'q'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('banks.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('banks.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('banks.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("BanksController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $arrayToSave = [
            'description' => $description
        ];

        if (!Banks::create($arrayToSave)){
            \Log::warning("BanksController | Error while trying to save new brand");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }

        return redirect()->route('banks.index')->with('success', 'Banco creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('banks.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$bank = Banks::find($id)){
            \Log::warning("BanksController | Bank {$id} not found");
            return redirect()->back()->with('error', 'Banco no encontrado');
        }

        return view('banks.edit', compact('bank'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('banks.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$bank = Banks::find($id)){
            \Log::warning("BanksController | Bank {$id} not found");
            return redirect()->back()->with('error', 'Banco no encontrado');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("BanksCOntroller | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $bank->description = $description;

        if (!$bank->save()){
            \Log::warning("BanksController | Error on update bank id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('banks.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('banks.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($brand = Banks::find($id)) {
                try {
                    if (Banks::destroy($id)) {
                        $message = 'Marca eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("BanksController | Error deleting Bank: " . $e->getMessage());
                    $message = 'Error al intentar eliminar el banco';
                    $error = true;
                }
            } else {
                \Log::warning("BanksController | Bank {$id} not found");
                $message = 'Banco no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

}