<?php

namespace App\Http\Controllers;

use App\Http\Request\ClientsRequest;
use App\Models\Cities;
use App\Models\Clients;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Client;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['searchClient']]);
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('clients')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['description'])) $where .= "description ILIKE '%{$input['description']}%' AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $clients = Clients::orderBy('id', 'desc')->paginate(20);
            } else {
                $clients = Clients::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }

            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        }else{
            $clients = Clients::orderBy('id', 'desc')->paginate(20);
            $description ='';
            $q = '';
        }

        return view('clients.index', compact('clients', 'q', 'description'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('clients.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $lastClientCode = Clients::select("client_code")->orderBy('id','desc')->first();
        if ($lastClientCode->client_code){
            $clientCode = $lastClientCode->client_code + 1;
        }else{
            $clientCode = 1;
        }
        $citiesJson = json_encode(Cities::all(['description', 'id']));
        return view('clients.create', compact('citiesJson', 'clientCode'));
    }

    public function store(ClientsRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('clients.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (Clients::where('idnum', $input['idnum'])->first()){
            \Log::warning("ClientsController | Client already in the system");
            return redirect()->back()->with('error', 'Este numero de cédula ya fue registrado');
        }

        if (Clients::where('tax_code', $input['tax_code'])->first()){
            \Log::warning("ClientsController | Client already in the system");
            return redirect()->back()->with('error', 'Este ruc ya fue registrado');
        }

        if ($input['credit_limit'] != 0) {
            $input['credit_available'] = $input['credit_limit'];
        }

        if ($client = Clients::create($input)){
            \Log::info("ClientsController | New client created");
            return redirect()->route('clients.index')->with('success', 'Cliente creado exitosamente');
        }else{
            \Log::warning("ClientsController | There's a problem with the new client");
            return redirect()->back()->with('error', 'Ocurrio un error al crear el registro');
        }
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('clients.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$client = Clients::find($id)){
            \Log::warning("ClientsController | Client {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }
        $citiesJson = json_encode(Cities::all(['description', 'id']));
        return view('clients.edit', compact('client', 'citiesJson'));
    }

    public function update(ClientsRequest $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('clients.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (!$client = Clients::find($id)) {
            \Log::warning("ClientsController | Client {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrada');
        }

        if ($input['credit_limit'] != 0) {
            if ($input['credit_limit'] > $client->credit_limit) {
                $diff = $input['credit_limit'] - $client->credit_limit;
                $input['credit_available'] = $client->credit_available + $diff;
            } else {
                $diff = $client->credit_limit - $input['credit_limit'];
                $input['credit_available'] = $client->credit_available - $diff;
            }
        }


        if (!$client->update($input)) {
            \Log::warning("ClientsController | Error on update client id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }

        return redirect()->route('clients.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('clients.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($client = Clients::find($id)) {
                try {
                    if (Clients::destroy($id)) {
                        $message = 'Registro eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("ClientsController | Error deleting Client : " . $e->getMessage());
                    $message = 'Error al intentar eliminar el registro';
                    $error = true;
                }
            } else {
                \Log::warning("ClientsController | Client {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

    public function searchClient($id)
    {
        if (is_null($id)){
            \Log::warning("ClientsControllers | Missing id to search");
            return json_encode(['error' => true]);
        }

        $client = Clients::find($id);
        if (!is_null($client)){
            return json_encode(['client' => $client, 'error' => false]);
        }else{
            return json_encode(['error' => true, 'code' => 10]);
        }
    }

    public function addClientBySale(Request $request)
    {
        \Log::debug("ClientsController | New Client from sales view");
        \Log::info(json_encode($request->all()));
        if (Clients::where('tax_code', $request->get('tax_code'))->first()){
            \Log::warning("ClientsController | Client already registered");
            return json_encode(['error' => true, 'message' => 'El cliente ya existe']);
        }
        if ($client = Clients::create($request->all())){
            return json_encode(['error' => false, 'message' => 'success', 'data' => ['id' => $client->id]]);
        }else{
            return json_encode(['error' => true, 'message' => 'ocurrio un error al crear el registro']);
        }
    }

    public function getLastClientCode()
    {
        $lastClientCode = Clients::select("client_code")->orderBy('id','desc')->first();

        if (is_null($lastClientCode)){
            $clientSend = 1;
        }else{
            $clientSend = $lastClientCode->client_code + 1;
        }
        \Log::info("ClientsController | Send last client_code => {$clientSend}");
        return response()->json(['error' => false, 'client_code' => $clientSend]);
    }

    public function toggleClients($id)
    {
        if (is_null($id)){
            \Log::warning("ClientsControllers | Missing id to search");
            return redirect()->back()->with('error', 'Debe elegir un cliente');
        }

        $client = Clients::find($id);
        if (!is_null($client)){
            $client->active = $client->active == true ? false : true;
            $client->save();
            return redirect()->back()->with('success', 'Cliente actualizado exitosamente');
        }else{
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }
}
