<?php

namespace App\Http\Controllers;

use App\Http\Request\SalesRequest;
use App\Models\Banks;
use App\Models\Branches;
use App\Models\Cashiers;
use App\Models\Cities;
use App\Models\Clients;
use App\Models\Companies;
use App\Models\Currencies;
use App\Models\InvoiceDetails;
use App\Models\InvoiceHeaders;
use App\Models\PaymentMethods;
use App\Models\PricingDetails;
use App\Models\PricingHeader;
use App\Models\Products;
use App\Models\Refunds;
use App\Models\Salesman;
use App\Models\SalesmanCommissions;
use App\Models\StockMovements;
use App\Models\Stocks;
use App\Models\UserCommission;
use App\Models\Vouchers;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',
            [
                'except' => [
                    'salesProducts',
                ],
            ]
        );
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) {
                $where .= "sale_date = '{$input['date_start']}' AND ";
            }

            if (!is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "sale_date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            }

            if (is_null($input['date_start']) && !is_null($input['date_end'])) {
                $where .= "sale_date= '{$input['date_end']}' AND ";
            }

            if (!is_null($input['sale_type_id']) && $input['sale_type_id'] != 0) {
                $where .= "sale_type_id = {$input['sale_type_id']} AND ";
            }
            if (!is_null($input['company_id']) && $input['company_id'] != 0) {
                $where .= "company_id = '{$input['company_id']}' AND ";
            }

            if (!is_null($input['invoice_status']) && $input['invoice_status'] != 0) {
                $where .= "invoice_status = '{$input['invoice_status']}' AND ";
            }

            if (!is_null($input['currency_id']) && $input['currency_id'] != 0) {
                $where .= "currency_id = '{$input['currency_id']}' AND ";
            }

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                return redirect()->back()->with('error', 'Debe añadir al menos un filtro para realizar la busqueda');
            } else {
                $salesHeader = InvoiceHeaders::orderBy('id', 'desc')
                    ->whereRaw($where)
                    ->paginate(50);
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $q = $input['q'];
            $sale_type_id = isset($input['sale_type_id']) ? $input['sale_type_id'] : '';
            $company_id = isset($input['company_id']) ? $input['company_id'] : '';
            $invoice_status = isset($input['invoice_status']) ? $input['invoice_status'] : '';
            $currency_id = isset($input['currency_id']) ? $input['currency_id'] : '';
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $sale_type_id = '';
            $company_id = '';
            $invoice_status = '';
            $currency_id = '';
            $salesHeader = InvoiceHeaders::orderBy('id', 'desc')
                ->where('invoice_status', '=', '1')
                ->paginate(20);

        }
        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $currenciesJson = json_encode(Currencies::all(['description', 'id']));
        return view('sales.index', compact('salesHeader', 'date_end', 'date_start', 'q', 'sale_type_id',
            'companiesJson', 'company_id', 'invoice_status', 'currenciesJson', 'currency_id'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (\Session::exists('sales_details')) {
            \Session::pull('sales_details');
        }

        $citiesJson = json_encode(Cities::all(['description', 'id']));
        $priceListJson = json_encode(PricingHeader::all(['title', 'id']));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all(['description', 'id']));
        } else {
            $branchesJson = '{}';
        }
        $products = \DB::table('products')
            ->join('stocks', 'stocks.product_id', '=', 'products.id')
            ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
            ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
            ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
            ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1,
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price, products.bar_code as bar_code')
            ->where("stocks.branch_id", "1")
            ->where('products.active', '=', 'true')
            ->get();
        foreach ($products as $product) {
            $product->quantity = '';
        }
        $productsJson = json_encode($products);
        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $clientsJson = json_encode(Clients::selectRaw("description || ' - ' || tax_code as description, id")->where('active', true)->get());
        $salesmanJson = json_encode(Salesman::all(['name', 'id']));
        $companyController = new CompaniesController();
        $voucher = $companyController->getNumber(1);
        $voucher = json_decode($voucher);
        $defaultValues = ['company_id' => 1, 'ruc' => $voucher->number, 'salesman_id' => 1];
        $cashiers = Cashiers::where('is_open', true)->get();
        if ($cashiers->isEmpty())
            return redirect()->back()->with('error', 'Debe tener al menos una caja abierta para continuar');

        $cashierJson = json_encode(Cashiers::where('is_open', true)->select('name', 'id')->get()->toArray());
        // Find first cashier
        $cashier = Cashiers::first();
        if (is_null($cashier))
            return redirect()->back()->with('error', 'Debe agregar un cajero para continuar');

        return view('sales.create', compact('productsJson', 'citiesJson', 'priceListJson',
            'branchesJson', 'companiesJson', 'clientsJson', 'defaultValues', 'salesmanJson', 'cashierJson', 'cashier'));
    }

    public function store(SalesRequest $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        $input['user_id'] = \Sentinel::getUser()->id;

        $client = Clients::find($input['client_id']);
        $company = Companies::find($input['company_id']);
        $user = \Sentinel::getUser();

        // Validamos que la venta no supere el limite disponible si es que es crédito
//        if ($input['sale_type_id'] == 2 && $client->credit_available < $input['sub_total_hidden']) {
//            \Log::warning("Client without enough credit");
//            return redirect()->back()->with('error', 'Cliente no cuenta con credito disponible');
//        }
        if (!is_null($client)) {
            $totalAmount = 0;
            $totalDiscount = 0;
            $input['client_id'] = $client->id;
            $input['sale_date'] = date("Y-m-d");
            $input['total_amount'] = $totalAmount;
            $input['total_discount'] = $totalDiscount;
            $input['currency_id'] = 1; // Guaranies
            $input['change_fee'] = 1; // Guaranies
            $input['branch_id'] = 1;
            $input['invoice_status'] = 1;
            $input['invoice_number'] = "00" . $company->invoice_first . "-" . "00" . $company->invoice_second . "-"
                . $company->print_invoice_number;

            if ($input['sale_type_id'] == 2) {
                $input['amount_left'] = $totalAmount;
            }
            if ($header = InvoiceHeaders::create($input)) {
                $company->print_invoice_number = $company->print_invoice_number + 1;
                $company->save();
                // Creamos los detalles
                for ($i = 0; $i < count($input['sale_product_id']); $i++) {

                    $product = Products::find($input['sale_product_id'][$i]);
                    $price = PricingDetails::where('product_id', $product->id)
                        ->where('pricing_header_id', $input['sale_price_list'][$i])->first();
                    \Log::info("SalesController | Product: {$input['sale_product_id'][$i]} -
                     Qty: {$input['sale_quantity'][$i]} - Price: {$price->selling_price}");

                    $subTotal = $price->selling_price * $input['sale_quantity'][$i];

                    $detailsArray = [
                        'invoice_header_id' => $header->id,
                        'product_id' => $product->id,
                        'price' => $price->selling_price,
                        'quantity' => $input['sale_quantity'][$i],
                        'pricing_list_id' => $input['sale_price_list'][$i],
                        'sub_total' => $subTotal,
                        'currency_id' => 1,
                        'change_fee' => 1,
                    ];

                    if ($input['sale_custom_price'][$i] != 0) {
                        // Product has discount or new price
                        $detailsArray['special_price'] = $input['sale_custom_price'][$i];
                        $detailsArray['special_subtotal'] = $input['sale_custom_price'][$i] * $input['sale_quantity'][$i];
                        $detailsArray['subtotal_discount'] = $subTotal - ($input['sale_custom_price'][$i] * $input['sale_quantity'][$i]);
                        $totalDiscount += $subTotal - ($input['sale_custom_price'][$i] * $input['sale_quantity'][$i]);
                    } else {
                        $detailsArray['special_price'] = 0;
                        $detailsArray['special_subtotal'] = 0;
                        $detailsArray['subtotal_discount'] = 0;
                    }

                    $totalAmount += $subTotal;
                    $invoiceDetail = InvoiceDetails::create($detailsArray);

                    if ($input['company_id'] != 7) { // Si no es presupuesto, sacamos del stock
                        $productStock = Stocks::where('product_id', $product->id)
                            ->where('branch_id', $header->branch_id)->first();

                        if (!is_null($productStock)) {
                            $currentStockAvailable = $productStock->available;
                            $productStock->available = $productStock->available - $input['sale_quantity'][$i];
                            $productStock->save();
                        } else {
                            $currentStockAvailable = 0;
                        }

                        $invoiceDetail->stock_before = $currentStockAvailable;
                        $invoiceDetail->stock_after = $productStock->available;
                        $invoiceDetail->save();

                        // Actualizamos la nueva tabla de movimientos para este producto
                        $stockMovement = [
                            'product_id' => $product->id,
                            'movement_type_id' => 3, // Venta,
                            'movement_id' => $header->id,
                            'quantity' => $input['sale_quantity'][$i],
                            'stock_before' => $currentStockAvailable,
                            'stock_after' => $currentStockAvailable - $input['sale_quantity'][$i]
                        ];

                        StockMovements::create($stockMovement);

                        // Verificamos si el producto tiene comision por venta para guardar en la tabla de comisiones
                        $commissionToSave = [
                            'salesman_id' => $input['salesman_id'],
                            'product_id' => $invoiceDetail->product_id,
                            'order_detail_id' => $invoiceDetail->id,
                        ];

                        $salesman = Salesman::find($input['salesman_id']);
                        $flag = false;
                        $amount = $invoiceDetail->special_subtotal == 0 ? $invoiceDetail->sub_total : $invoiceDetail->special_subtotal;
                        if ($salesman->percentage) {
                            $commissionToSave['commission_motive'] = 'salesman';
                            $commissionToSave['commission_percentage'] = $salesman->percentage;
                            $commissionToSave['amount'] = round((($amount * $salesman->percentage) / 100), 1, PHP_ROUND_HALF_UP);
                            $flag = true;
                        }
                        if ($invoiceDetail->product->commission_percentage != 0) {
                            $commissionToSave['commission_motive'] = 'product';
                            $commissionToSave['commission_percentage'] = $invoiceDetail->product->commission_percentage;
                            $commissionToSave['amount'] = round((($amount * $invoiceDetail->product->commission_percentage) / 100), 1, PHP_ROUND_HALF_UP);
                            $flag = true;
                        } elseif ($invoiceDetail->product->categories->commission_percentage != 0) {
                            $commissionToSave['commission_motive'] = 'category';
                            $commissionToSave['commission_percentage'] = $invoiceDetail->product->categories->commission_percentage;
                            $commissionToSave['amount'] = round((($amount * $invoiceDetail->product->categories->commission_percentage) / 100), 0, PHP_ROUND_HALF_UP);
                            $flag = true;
                        }

                        if ($flag) {
                            SalesmanCommissions::create($commissionToSave);
                        }
                    }
                }

                // Comisión ventas por usuario. - Solo si puede comisionar, no todos pueden porque este mundo no es justo
                if ($input['company_id'] != 7 && $user->can_get_commission) {
                    // TODO por ahora es comisión por venta total, después veremos para el detalle.
                    $commissionValue = ($totalAmount * $user->commission_percentage) / 100;
                    $userCommission = [
                        'user_id' => $user->id,
                        'invoice_header_id' => $header->id,
                        'commission_percentage' => $user->commission_percentage,
                        'amount' => (int) round($commissionValue, 0, PHP_ROUND_HALF_DOWN),
                    ];
                    UserCommission::create($userCommission);
                }

                $header->total_amount = $totalAmount;
                if ($input['sale_type_id'] == 2) {
                    $header->amount_left = $totalAmount;
                    // Restamos la factura de lo que el cliente tiene disponible
                    $client->credit_available = $client->credit_limit - $totalAmount;
                    $client->save();
                }

                if ($input['sale_type_id'] == 1) {
                    // Agregamos a la plata que entra a la caja
                    $cashier = Cashiers::where('id', $input['cashier_id'])->first();
                    $cashier_movement = $cashier->lastMovement();
                    $cashier_movement->closing_amount = $cashier_movement->closing_amount + $totalAmount;
                    $cashier_movement->save();
                }
                $header->total_discount = $totalDiscount;
                $header->save();

                return redirect()->route('sales.index')->with('success', 'Venta realizada exitosamente');
            }

        } else {
            \Log::warning("SalesController | Client {$input['client_num']} not found");
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }

    public function printInvoice($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.print')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        ini_set('magic_quotes_runtime', 0);

        if ($sale = InvoiceHeaders::find($id)) {
            // Imprimimos Ticket
            // TODO This changes depending of the browser - We need to fix that ! ASAP
            if ($sale->company_id == 1) { // Ticket
                $pdf = new \CustomFPDF('P', 'cm', [24, 24]);
                $pdf->Open();
                $pdf->AliasNbPages();
                $pdf->AddPage('P');
                $pdf->SetFont('Courier', 'B', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFontSpacing(0);

                /*
                |--------------------------------------------------------------------------
                | Cabecera
                |--------------------------------------------------------------------------
                 */
                $pdf->Text(14, 2, 'Comprobante Interno');
                $pdf->Text(14, 2.5, 'Nro: ');
                $pdf->Text(15, 2.5, $sale->invoice_number);

                $pdf->Text(1, 2.5, 'Fecha: ');
                $pdf->Text(1, 3, 'Cliente: ');
                //$pdf->Text(15, 3, 'RUC: ');
                $pdf->Text(1, 3.5, 'Direccion: ');
                $pdf->Text(14, 3, 'Tel: ');

                $pdf->Text(3, 2.5, date("d-m-Y", strtotime($sale->sale_date)));
                $pdf->Text(3.5, 3, utf8_decode($sale->client->tax_name));
                //$pdf->Text(17.5, 3, $sale->client->tax_code);
                $pdf->Text(3.5, 3.5, isset($sale->client->address) ? $sale->client->address : "-------------");
                $pdf->Text(15, 3, isset($sale->client->telephone) ? $sale->client->telephone : "-------------");

                /*
                |--------------------------------------------------------------------------
                | Detalle
                |--------------------------------------------------------------------------
                 */
                $height = 4.5;
                $pdf->Text(1, 4, 'CANT');
                $pdf->Text(3, 4, 'PRODUCTO');
                $pdf->Text(14.5, 4, 'PRECIO');
                $pdf->Text(18, 4, 'SUBTOTAL');

                foreach ($sale->invoiceDetails as $detail) {
                    $pdf->Text(1, $height, $detail->quantity);
                    $pdf->Text(3, $height, utf8_decode($detail->product->title));
                    if ($detail->special_price != 0) {
                        $pdf->Text(14.5, $height, number_format($detail->special_price, 0, ',', '.'));
                        $pdf->Text(18, $height, number_format($detail->special_subtotal, 0, ',', '.'));
                    } else {
                        $pdf->Text(14.5, $height, number_format($detail->price, 0, ',', '.'));
                        $pdf->Text(18, $height, number_format($detail->sub_total, 0, ',', '.'));
                    }

                    $height += 0.5;

                }

                /*
                |--------------------------------------------------------------------------
                | Footer
                |--------------------------------------------------------------------------
                 */

                $numberToWord = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);

                $pdf->Text(15, 12.5, "Total: ");
                $pdf->Text(1, 13.5, 'Total: ');

                if ($sale->total_discount != 0) {

                    $pdf->Text(17, 12.5, number_format($sale->total_amount - $sale->total_discount, 0, ',', '.'));
                    $pdf->Text(4, 13.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount - $sale->total_discount)))));

                } else {
                    $pdf->Text(17, 12.5, number_format($sale->total_amount, 0, ',', '.'));
                    $pdf->Text(4, 13.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount)))));

                }

                $pdf->Output();

                exit;
            } elseif ($sale->company_id == 7) { // Presupuesto
                $pdf = new \CustomFPDF('P', 'cm', [24, 24]);
                $pdf->Open();
                $pdf->AliasNbPages();
                $pdf->AddPage('0');
                $pdf->SetFont('Courier', 'B', 11);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFontSpacing(0);

                /*
                |--------------------------------------------------------------------------
                | Cabecera
                |--------------------------------------------------------------------------
                 */
                $pdf->Text(5, 1.5, 'NOTA DE PRESUPUESTO');
                $pdf->Text(12, 1.5, 'Presupuesto Nro: ');
                $pdf->Text(16.5, 1.5, $sale->invoice_number);

                $pdf->Text(1, 2.5, 'Fecha: ');
                $pdf->Text(1, 2, 'Cliente: ');
                $pdf->Text(14, 2, 'RUC: ');
                $pdf->Text(1, 3.5, 'Direccion: ');
                $pdf->Text(1, 3, 'Telefono: ');

                $pdf->Text(2.5, 2.5, date("d-m-Y", strtotime($sale->sale_date)));
                $pdf->Text(3, 2, utf8_decode($sale->client->tax_name));
                $pdf->Text(15, 2, $sale->client->tax_code);
                $pdf->Text(3.5, 3.5, isset($sale->client->address) ? $sale->client->address : "-------------");
                $pdf->Text(3.5, 3, isset($sale->client->telephone) ? $sale->client->telephone : "-------------");

                /*
                |--------------------------------------------------------------------------
                | Detalle
                |--------------------------------------------------------------------------
                 */

                $height = 4.5;
                $pdf->Text(1, 4, 'CANT');
                $pdf->Text(4, 4, 'PRODUCTO');
                $pdf->Text(14, 4, 'PRECIO');
                $pdf->Text(17, 4, 'SUBTOTAL');

                foreach ($sale->invoiceDetails as $detail) {
                    $pdf->Text(1, $height, $detail->quantity);
                    $pdf->Text(4, $height, utf8_decode($detail->product->title));
                    if ($detail->special_price != 0) {
                        $pdf->Text(14, $height, number_format($detail->special_price, 0, ',', '.'));
                        $pdf->Text(17, $height, number_format($detail->special_subtotal, 0, ',', '.'));
                    } else {
                        $pdf->Text(14, $height, number_format($detail->price, 0, ',', '.'));
                        $pdf->Text(17, $height, number_format($detail->sub_total, 0, ',', '.'));
                    }

                    $height += 0.5;

                }

                $numberToWord = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);

                $pdf->Text(15, 12, "Total: ");
                $pdf->Text(2, 12.5, 'Total: ');

                if ($sale->total_discount != 0) {

                    $pdf->Text(17, 12, number_format($sale->total_amount - $sale->total_discount, 0, ',', '.'));
                    $pdf->Text(5, 12.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount - $sale->total_discount)))));

                } else {
                    $pdf->Text(17, 12, number_format($sale->total_amount, 0, ',', '.'));
                    $pdf->Text(5, 12.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount)))));

                }

                $pdf->Output();

                exit;
            } else { // Facturas
                $pdf = new \CustomFPDF('P', 'cm', [20, 20]);
                $pdf->Open();
                $pdf->AliasNbPages();
                $pdf->AddPage('0');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetFontSpacing(0);

                /*
                |--------------------------------------------------------------------------
                | Cabecera
                |--------------------------------------------------------------------------
                 */
                $pdf->Text(4, 4, date('d-m-Y', strtotime($sale->sale_date)));
                if ($sale->sale_type_id == 1) {
                    $pdf->Text(12.3, 4, "XX");
                } else {
                    $pdf->Text(15.3, 4, "XX");
                }

                $clientTelephone = isset($sale->client->telephone) ? $sale->client->telephone : "-------------";

                $pdf->Text(3, 4.5, isset($sale->client->tax_code) ? $sale->client->tax_code : "-------------");
                $pdf->Text(3.6, 5, utf8_decode($sale->client->tax_name));
                $pdf->Text(14.3, 5, 'Tel:' . $clientTelephone);
                $pdf->Text(2.5, 5.5, isset($sale->client->address) ? utf8_decode($sale->client->address) : "-------------");

                /*
                |--------------------------------------------------------------------------
                | Detalle
                |--------------------------------------------------------------------------
                 */
                $height = 7;
                foreach ($sale->invoiceDetails as $detail) {
                    $pdf->Text(1, $height, $detail->quantity);
                    $pdf->Text(2, $height, utf8_decode($detail->product->title));
                    if ($detail->special_price != 0) {
                        $pdf->Text(10.5, $height, number_format($detail->special_price, 0, ',', '.'));
                        $pdf->Text(16.5, $height, number_format($detail->special_subtotal, 0, ',', '.'));
                    } else {
                        $pdf->Text(10.5, $height, number_format($detail->price, 0, ',', '.'));
                        $pdf->Text(16.5, $height, number_format($detail->sub_total, 0, ',', '.'));
                    }

                    $height += 0.5;
                }

                /*
                |--------------------------------------------------------------------------
                | Footer
                |--------------------------------------------------------------------------
                 */
                $pdf->Text(9, 16.5, '0');
                if ($sale->total_discount != 0) {
                    $pdf->Text(16.5, 15, number_format($sale->total_amount - $sale->total_discount, 0, ',', '.'));
                    $numberToWord = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
                    $pdf->Text(3.6, 15.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount - $sale->total_discount)))));
                    $pdf->Text(10, 16.5, number_format((($sale->total_amount - $sale->total_discount) / 11), 0, ',', '.'));
                    $pdf->Text(15, 16.5, number_format((($sale->total_amount - $sale->total_discount) / 11), 0, ',', '.'));

                } else {
                    $pdf->Text(16.5, 15, number_format($sale->total_amount, 0, ',', '.'));
                    $numberToWord = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
                    $pdf->Text(3.6, 15.5, utf8_decode('GUARANIES ' . preg_replace('~\x{00AD}~u', '', strtoupper($numberToWord->format($sale->total_amount)))));
                    $pdf->Text(10, 16.5, number_format(($sale->total_amount / 11), 0, ',', '.'));
                    $pdf->Text(15, 16.5, number_format(($sale->total_amount / 11), 0, ',', '.'));
                }
                $pdf->Output();

                exit;
            }

        } else {
            \Log::warning("SalesController | Missing invoice to print");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }

    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$header = InvoiceHeaders::find($id)) {
            \Log::warning("SalesController | Invoice id {$id} not found");
            return redirect()->back()->with("error", 'Factura no encontrada');
        }
        return view("sales.show", compact('header'));
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$header = InvoiceHeaders::find($id)) {
            \Log::warning("SalesController | Invoice id {$id} not found");
            return redirect()->back()->with("error", 'Factura no encontrada');
        }
        $citiesJson = json_encode(Cities::all(['description', 'id']));
        $priceListJson = json_encode(PricingHeader::all(['title', 'id']));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all(['description', 'id']));
        } else {
            $branchesJson = '{}';
        }
        $products = \DB::table('products')
            ->join('stocks', 'stocks.product_id', '=', 'products.id')
            ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
            ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
            ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
            ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1,
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price, products.bar_code as bar_code')
            ->where("stocks.branch_id", "1")
            ->get();
        foreach ($products as $product) {
            $product->quantity = '';
        }
        $productsJson = json_encode($products);
        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $clientsJson = json_encode(Clients::selectRaw("description || ' - ' || tax_code as description, id")->where('active', true)->get());

        $companyController = new CompaniesController();
        $voucher = $companyController->getNumber(1);
        $voucher = json_decode($voucher);
        $defaultValues = ['company_id' => 1, 'ruc' => $voucher->number, 'salesman_id' => 1];
        $salesmanJson = json_encode(Salesman::all(['name', 'id']));
        $cashierJson = json_encode(Cashiers::all(['name', 'id']));
        return view("sales.edit", compact('header', 'citiesJson', 'priceListJson', 'branchesJson',
            'productsJson', 'companiesJson', 'clientsJson', 'defaultValues', 'salesmanJson', 'cashierJson'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $user = \Sentinel::getUser();
        // Anulamos la factura anterior
        $oldInvoice = InvoiceHeaders::find($id);
        $oldInvoice->invoice_status = 2;
        $oldInvoice->save();

        // Devolvemos stock anterior si la factura anterior NO es un Presupuesto
        if ($oldInvoice->company_id != 7) {
            foreach ($oldInvoice->invoiceDetails as $oldDetail) {
                $stock = Stocks::where('product_id', $oldDetail->product_id)
                    ->where("branch_id", '=', 1)
                    ->first();
                $currentStockAvailable = $stock->available;
                $stock->available = $stock->available + $oldDetail->quantity;
                $stock->save();

                // Actualizamos la nueva tabla de movimientos para este producto
                $stockMovement = [
                    'product_id' => $oldDetail->product_id,
                    'movement_type_id' => 4, // Devolucion,
                    'movement_id' => $oldInvoice->id,
                    'quantity' => $oldDetail->quantity,
                    'stock_before' => $currentStockAvailable,
                    'stock_after' => $currentStockAvailable + $oldDetail->quantity
                ];

                StockMovements::create($stockMovement);

                $commission = SalesmanCommissions::where('order_detail_id', $oldDetail->id)
                    ->where('product_id', $oldDetail->product_id)->first();
                if (!is_null($commission)) {
                    $commission->canceled = true;
                    $commission->save();
                }
            }
        }
        $input = $request->all();

        $input['user_id'] = \Sentinel::getUser()->id;

        $client = Clients::find($input['client_id']);
        $company = Companies::find($input['company_id']);

        if (!is_null($client)) {
            $totalAmount = 0;
            $totalDiscount = 0;
            $input['client_id'] = $client->id;
            $input['sale_date'] = date("Y-m-d");
            $input['total_amount'] = $totalAmount;
            $input['total_discount'] = $totalDiscount;
            $input['currency_id'] = 1; // Guaranies
            $input['change_fee'] = 1; // Guaranies
            $input['branch_id'] = 1;
            $input['invoice_status'] = 1;
            $input['invoice_number'] = "00" . $company->invoice_first . "-" . "00" . $company->invoice_second . "-"
                . $company->print_invoice_number;

            if ($input['sale_type_id'] == 2) {
                $input['amount_left'] = $totalAmount;
            }
            if ($header = InvoiceHeaders::create($input)) {
                $company->print_invoice_number = $company->print_invoice_number + 1;
                $company->save();
                // Creamos los detalles
                for ($i = 0; $i < count($input['sale_product_id']); $i++) {
                    $product = Products::find($input['sale_product_id'][$i]);
                    $price = PricingDetails::where('product_id', $product->id)
                        ->where('pricing_header_id', $input['sale_price_list'][$i])->first();

                    \Log::info("SalesController | Product: {$input['sale_product_id'][$i]} -
                     Qty: {$input['sale_quantity'][$i]} - Price: {$price->selling_price}");

                    $subTotal = $price->selling_price * $input['sale_quantity'][$i];

                    $detailsArray = [
                        'invoice_header_id' => $header->id,
                        'product_id' => $product->id,
                        'price' => $price->selling_price,
                        'quantity' => $input['sale_quantity'][$i],
                        'pricing_list_id' => $input['sale_price_list'][$i],
                        'sub_total' => $subTotal,
                        'currency_id' => 1,
                        'change_fee' => 1,
                    ];

                    if ($input['sale_custom_price'][$i] != 0) {
                        // Product has discount or new price
                        $detailsArray['special_price'] = $input['sale_custom_price'][$i];
                        $detailsArray['special_subtotal'] = $input['sale_custom_price'][$i] * $input['sale_quantity'][$i];
                        $detailsArray['subtotal_discount'] = $subTotal - ($input['sale_custom_price'][$i] * $input['sale_quantity'][$i]);
                        $totalDiscount += $subTotal - ($input['sale_custom_price'][$i] * $input['sale_quantity'][$i]);
                    } else {
                        $detailsArray['special_price'] = 0;
                        $detailsArray['special_subtotal'] = 0;
                        $detailsArray['subtotal_discount'] = 0;
                    }

                    $totalAmount += $subTotal;

                    $invoiceDetail = InvoiceDetails::create($detailsArray);

                    if ($input['company_id'] != 7) { // Si no es presupuesto, sacamos del stock
                        $productStock = Stocks::where('product_id', $product->id)
                            ->where('branch_id', $header->branch_id)->first();

                        if (!is_null($productStock)) {
                            $currentStockAvailable = $productStock->available;
                            $productStock->available = $productStock->available - $input['sale_quantity'][$i];
                            $productStock->save();
                        } else {
                            $currentStockAvailable = 0;
                        }

                        $invoiceDetail->stock_before = $currentStockAvailable;
                        $invoiceDetail->stock_after = $productStock->available;
                        $invoiceDetail->save();

                        // Actualizamos la nueva tabla de movimientos para este producto
                        $stockMovement = [
                            'product_id' => $product->id,
                            'movement_type_id' => 3, // Venta,
                            'movement_id' => $header->id,
                            'quantity' => $input['sale_quantity'][$i],
                            'stock_before' => $currentStockAvailable,
                            'stock_after' => $currentStockAvailable - $input['sale_quantity'][$i]
                        ];

                        StockMovements::create($stockMovement);

                        // Verificamos si el producto tiene comision por venta para guardar en la tabla de comisiones
                        $commissionToSave = [
                            'salesman_id' => $input['salesman_id'],
                            'product_id' => $invoiceDetail->product_id,
                            'order_detail_id' => $invoiceDetail->id,
                        ];
                        $flag = false;
                        $amount = $invoiceDetail->special_subtotal == 0 ? $invoiceDetail->sub_total : $invoiceDetail->special_subtotal;
                        $salesman = Salesman::find($input['salesman_id']);
                        if ($salesman->percentage) {
                            $commissionToSave['commission_motive'] = 'salesman';
                            $commissionToSave['commission_percentage'] = $salesman->percentage;
                            $commissionToSave['amount'] = round((($amount * $salesman->percentage) / 100), 1, PHP_ROUND_HALF_UP);
                            $flag = true;
                        }
                        if ($invoiceDetail->product->commission_percentage != 0) {
                            $commissionToSave['commission_motive'] = 'product';
                            $commissionToSave['commission_percentage'] = $invoiceDetail->product->commission_percentage;
                            $commissionToSave['amount'] = ($amount * $invoiceDetail->product->commission_percentage) / 100;
                            $flag = true;
                        } elseif ($invoiceDetail->product->categories->commission_percentage != 0) {
                            $commissionToSave['commission_motive'] = 'category';
                            $commissionToSave['commission_percentage'] = $invoiceDetail->product->categories->commission_percentage;
                            $commissionToSave['amount'] = ($amount * $invoiceDetail->product->categories->commission_percentage) / 100;
                            $flag = true;
                        }

                        if ($flag) {
                            SalesmanCommissions::create($commissionToSave);
                        }
                    }
                }

                // Comisión ventas por usuario. - Solo si puede comisionar, no todos pueden porque este mundo no es justo
                if ($user->can_get_commission) {
                    // TODO por ahora es comisión por venta total, después veremos para el detalle.
                    $commissionValue = ($totalAmount * $user->commission_percentage) / 100;
                    $userCommission = [
                        'user_id' => $user->id,
                        'invoice_header_id' => $header->id,
                        'commission_percentage' => $user->commission_percentage,
                        'amount' => (int) round($commissionValue, 0, PHP_ROUND_HALF_DOWN),
                    ];
                    UserCommission::create($userCommission);
                }


                $header->total_amount = $totalAmount;

                if ($input['sale_type_id'] == 2) {
                    $header->amount_left = $totalAmount;
                }
                $header->total_discount = $totalDiscount;
                $header->save();


                return redirect()->route('sales.index')->with('success', 'Venta realizada exitosamente');
            }

        } else {
            \Log::warning("SalesController | Client {$input['client_num']} not found");
            return redirect()->back()->with('error', 'Cliente no encontrado');
        }
    }

    public function destroy($id)
    {
        // NOt really destroy the invoice, we really cancel the invoice and return the goods to our stock
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('sales.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($sale = InvoiceHeaders::find($id)) {
                if ($sale->invoice_status == 2) {
                    \Log::warning("SalesController | Invoice already canceled");
                    $error = true;
                    $message = 'Esta factura ya ha sido anulada';
                } else {
                    try {

                        $sale->invoice_status = 2;
                        $sale->save();

                        if ($sale->company_id != 7) {
                            // Return stock to deposit - Only one branch ... for now;
                            foreach ($sale->invoiceDetails as $detail) {
                                $stock = Stocks::where('product_id', $detail->product_id)
                                    ->where("branch_id", '=', 1)
                                    ->first();
                                $currentStockAvailable = $stock->available;
                                $stock->available = $stock->available + $detail->quantity;
                                $stock->save();

                                // Actualizamos la nueva tabla de movimientos para este producto
                                $stockMovement = [
                                    'product_id' => $detail->product_id,
                                    'movement_type_id' => 4, // Devolucion,
                                    'movement_id' => $sale->id,
                                    'quantity' =>  $detail->quantity,
                                    'stock_before' => $currentStockAvailable,
                                    'stock_after' => $currentStockAvailable + $detail->quantity
                            ];

                            StockMovements::create($stockMovement);

                            $commission = SalesmanCommissions::where('order_detail_id', $detail->id)
                                ->where('product_id', $detail->product_id)->first();
                            if (!is_null($commission)){
                                $commission->canceled = true;
                                $commission->save();
                            }

                            // Cancelar por usuario
                                if ($userCommission = UserCommission::where('invoice_header_id', $sale->id)->first()) {
                                    $userCommission->canceled = true;
                                    $userCommission->save();
                                }

                            }
                        }

                        // Confirmation Messages
                        $error = false;
                        $message = 'Factura anulada exitosamente';

                    } catch (\Exception $e) {
                        \Log::warning("SalesController | Error cancel Order : " . $e->getMessage());
                        $message = 'Error al intentar eliminar el registro';
                        $error = true;
                    }
                }

            } else {
                \Log::warning("SalesController | Sale {$id} not found");
                $message = 'Registro no encontrado';
                $error = true;
            }
        }

        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

    public function showRegisterVoucher($id)
    {
        if ($invoice = InvoiceHeaders::find($id)) {
            $banksJson = json_encode(Banks::all(['description', 'id']));
            $paymentMethodsJson = json_encode(PaymentMethods::all(['description', 'id']));
            return view('sales.voucher', compact('invoice', 'banksJson', 'paymentMethodsJson'));
        } else {
            \Log::warning("SalesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function registerVoucherPayment($id, Request $request)
    {
        if ($invoice = InvoiceHeaders::find($id)) {
            $input = $request->all();
            $newVoucherArray = [
                'invoice_header_id' => $id,
                'voucher_number' => $input['voucher_number'],
                'amount' => $input['amount'],
                'payment_id' => $input['payment_id'],
            ];

            if (isset($input['check_number'])) {
                $newVoucherArray['check_number'] = $input['check_number'];
                $newVoucherArray['check_date'] = $input['check_date'];
                $newVoucherArray['bank_id'] = $input['bank_id'];
            }
            Vouchers::create($newVoucherArray);

            // Editamos la fila de la factura para registrar los pagos
            $invoice->amount_left = $invoice->amount_left - $input['amount'];
            $invoice->save();

            // Restore client credit limit
            $client = Clients::find($invoice->client_id);
            $client->credit_available = $client->credit_available + $input['amount'];
            $client->save();

            return redirect()->route('sales.index')->with('success', 'Recibo cargado exitosamente');
        } else {
            \Log::warning("SalesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function showSalesRefund($id)
    {
        if ($header = InvoiceHeaders::find($id)) {
            return view('sales.refund', compact('header'));
        } else {
            \Log::warning("SalesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function doSalesRefund($id, Request $request)
    {
        if ($header = InvoiceHeaders::find($id)) {
            $input = $request->all();
            for ($i = 0; $i <= count($input['product_id']) - 1; $i++) {
                if (!is_null($input['devolution'][$i])) {
                    $invoiceDetail = InvoiceDetails::where('product_id', '=', $input['product_id'][$i])
                        ->where('invoice_header_id', '=', $header->id)->first();
                    $refundArray = [
                        'product_id' => $input['product_id'][$i],
                        'quantity' => $input['devolution'][$i],
                        'invoice_id' => $id,
                        'invoice_details_id' => $invoiceDetail->id,
                    ];
                    // Guardamos el historial de devoluciones
                    Refunds::create($refundArray);

                    // Sumamos este producto al stock general
                    $productStock = Stocks::where('product_id', $input['product_id'][$i])
                        ->where('branch_id', $header->branch_id)->first();

                    if (!is_null($productStock)) {
                        $productStock->available = $productStock->available + $input['devolution'][$i];
                        $productStock->save();
                    }
                }
            }

            return redirect()->route('sales.index')->with('success', 'Mercaderias devueltas correctamente');

        } else {
            \Log::warning("SalesController | Invoice not found");
            return redirect()->back()->with('error', 'Factura no encontrada');
        }
    }

    public function cancelVoucher($id)
    {
        if ($voucher = Vouchers::find($id)) {
            \Log::info("PurchasesController | Deleting voucher id: {$id}");
            $voucher->status_id = 2;
            $voucher->save();
            // Devolvemos el dinero a la cuenta
            $sale = InvoiceHeaders::find($voucher->invoice_header_id);
            $sale->amount_left = $sale->amount_left + $voucher->amount;
            $sale->save();

            return redirect()->back()->with('success', 'Recibo anulado exitosamente');
        }

        return redirect()->back()->with('error', 'Registro no encontrado');
    }

    public function paymentVouchers()
    {
        $creditSale = InvoiceHeaders::where('sale_type_id', '=', 2)
            ->where('invoice_status', '=', 1)
            ->orderBy('id', 'desc')->get();
        if (!$creditSale->isEmpty()) {
            $i = 0;
            $arrayResponse = [];
            foreach ($creditSale as $sale) {
                $left = $sale->amount_left - $sale->total_discount;
                $arrayResponse[$i] = [
                    'id' => $sale->id,
                    'client' => $sale->client->description,
                    'company' => $sale->company->description,
                    'sale_date' => $sale->sale_date,
                    'invoice_number' => $sale->invoice_number,
                    'amounts' => $sale->total_amount - $sale->total_discount . " / " . $left,
                    'created_at' => $sale->created_at->format('Y-m-d'),
                    'options' => ''
                ];
                $i++;
            }
            uasort($arrayResponse, function ($a, $b) {
                return ($a['id'] < $b['id']) ? 1 : -1;
            });
            $salesPurchasesJson = json_encode($arrayResponse);
            return view('sales.sales_vouchers', compact('salesPurchasesJson'));
        }

        return redirect()->back()->with('error', 'No existen facturas a credito disponibles');
    }

    public function paymentVouchersConfirmation(Request $request)
    {
        $input = $request->all();
        if (array_key_exists('payment', $input)) {
            $arrayResponse = [];
            $i = 0;
            $amountLeft = 0;
            foreach ($input['payment'] as $p) {
                $sale = InvoiceHeaders::find($p);

                $arrayResponse[$i] = [
                    'id' => $sale->id,
                    'provider' => $sale->client->description,
                    'company' => $sale->company->description,
                    'purchase_date' => $sale->purchase_date,
                    'invoice_number' => $sale->invoice_number,
                    'amounts' => $sale->amount_left,
                    'created_at' => $sale->created_at->format('Y-m-d'),
                    'options' => ''
                ];
                $amountLeft = $amountLeft + $sale->amount_left;
                $i++;
            }
            $banksJson = json_encode(Banks::all(['description', 'id']));
            $paymentMethodsJson = json_encode(PaymentMethods::all(['description', 'id']));
            return view('sales.sales_voucher_confirmation', compact('arrayResponse', 'banksJson', 'paymentMethodsJson', 'amountLeft'));
        }

        return redirect()->back()->with('error', 'Debe seleccionar facturas a pagar');
    }

    public function paymentVouchersConfirmationSubmit(Request $request)
    {
        $input = $request->all();
        if ($input['amount'] == $input['total_amount']) {
            foreach ($input['purchase_id'] as $purchase_id) {
                $purchase = InvoiceHeaders::find($purchase_id);
                $newVoucherArray = [
                    'invoice_header_id' => $purchase_id,
                    'voucher_number' => $input['voucher_number'],
                    'amount' => $purchase->amount_left,
                    'payment_id' => $input['payment_id']
                ];

                if (isset($input['check_number'])) {
                    $newVoucherArray['check_number'] = $input['check_number'];
                    $newVoucherArray['check_date'] = $input['check_date'];
                    $newVoucherArray['bank_id'] = $input['bank_id'];
                }

                Vouchers::create($newVoucherArray);

                // Editamos la fila de la factura para registrar los pagos
                $purchase->amount_left = 0;
                $purchase->save();

                // Restore client credit limit
                $client = Clients::find($purchase->client_id);
                $client->credit_available = $client->credit_available + $input['total_amount'];
                $client->save();
            }

            return redirect()->route('sales.index')->with('success', 'Pagos ingresados correctamente');
        } else {
            \Log::warning("PurchasesController | Amounts dont match");
            return redirect()->back()->with('error', 'Las cantidades no coinciden');
        }
    }

    public function newSalesMethod()
    {
        $citiesJson = json_encode(Cities::all(['description', 'id']));
        $priceListJson = json_encode(PricingHeader::all(['title', 'id']));
        if (\Sentinel::getUser()->hasAccess('branches.view.all')) {
            $branchesJson = json_encode(Branches::all(['description', 'id']));
        } else {
            $branchesJson = '{}';
        }
        $products = \DB::table('products')
            ->join('stocks', 'stocks.product_id', '=', 'products.id')
            ->join('pricing_details as a', 'products.id', '=', 'a.product_id')->where('a.pricing_header_id', 1)
            ->join('pricing_details as b', 'products.id', '=', 'b.product_id')->where('b.pricing_header_id', 2)
            ->join('pricing_details as c', 'products.id', '=', 'c.product_id')->where('c.pricing_header_id', 3)
            ->selectRaw('products.id as id, products.title as product, stocks.available as stock, a.selling_price as list_1,
            b.selling_price as list_2, c.selling_price as list_3, c.purchase_price as purchase_price, products.bar_code as bar_code')
            ->where("stocks.branch_id", "1")
            ->where('products.active', '=', 'true')
            ->get();
        foreach ($products as $product) {
            $product->quantity = '';
        }
        $productsJson = json_encode($products);
        $companiesJson = json_encode(Companies::all(['description', 'id']));
        $clientsJson = json_encode(Clients::selectRaw("description || ' - ' || tax_code as description, id")->get());
        $salesmanJson = json_encode(Salesman::all(['name', 'id']));
        $companyController = new CompaniesController();
        $voucher = $companyController->getNumber(1);
        $voucher = json_decode($voucher);
        $defaultValues = ['company_id' => 1, 'ruc' => $voucher->number, 'salesman_id' => 1];
        return view('sales.sales2', compact('citiesJson', 'priceListJson', 'branchesJson', 'product', 'productsJson', 'companiesJson',
            'clientsJson', 'salesmanJson', 'defaultValues'));
    }
}
