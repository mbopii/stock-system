<?php

namespace App\Http\Request;

use App\Http\Request\Request;

class PurchasesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchase_date' => 'required|date',
            'invoice_number' => 'required',
            'stamping' => 'required',
            'provider_id' => 'required',
            'payment_form_id' => 'required',
            'currency_id' => 'required',
            'branch_id' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'purchase_date.required' => 'Debe insertar una fecha de compra',
            'purchase_date.date' => 'Debe ingresar un formato de fecha valido (YYYY-MM-DD)',
            'invoice_number.required' => 'Debe ingresar el numero de factura',
            'stamping.required' => 'Debe ingresar el timbrado',
            'provider_id.required' => 'Debe elegir un proveedor',
            'payment_form_id.required' => 'Debe elegir una forma de pago',
            'currency_id.required' => 'Debe elegir una moneda',
            'branch_id.required' => 'Debe tener una sucursal asignada'
        ];
    }

}
