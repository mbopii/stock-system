<?php namespace App\Http\Request;

use App\Http\Request\Request;

class ProvidersRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'tax_code' => 'required',
            'tax_name' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe insertar un nombre al proveedor',
            'tax_code.required' => 'El campo Razon Social es obligatorio',
            'tax_name.required' => 'El campo Ruc es obligatorio'
        ];
    }

}
