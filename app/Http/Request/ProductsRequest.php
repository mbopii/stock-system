<?php

namespace App\Http\Request;

use App\Http\Request\Request;

class ProductsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'bar_code' => 'required',
            'metric_unit_id' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'max_quantity' => 'required|numeric',
            'min_quantity' => 'required|numeric'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Debe insertar un nombre al producto',
            'bar_code.required' => 'Debe insertar un codigo de barras',
            'metric_unit_id.required' => 'Debe insertar una unidad de medida',
            'brand_id.required' => 'Debe seleccionar una marca',
            'category_id.required' => 'Debe seleccionar una categoria',
            'max_quantity.required' => 'Debe seleccionar una cantidad maxima',
            'min_quantity.required' => 'Debe seleccionar una cantidad minima',
            'max_quantity.numeric' => 'La cantidad maxima debe ser un valor numerico',
            'min_quantity.numeric' => 'La cantidad minima debe ser un valor numerico',
        ];
    }

}
