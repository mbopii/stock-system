<?php namespace App\Http\Request;

use App\Http\Request\Request;

class CompaniesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'tax_code' => 'required',
            'stamping' => 'required',
            'address' => 'required',
            'telephone' => 'required',
            'city_id' => 'required',
            'invoice_first' => 'required|numeric',
            'invoice_second' => 'required|numeric',
            'print_invoice_number' => 'required|numeric'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe insertar un nombre a la empresa',
            'stamping.required' => 'El campo timbrado es obligatorio',
            'tax_code.required' => 'El campo Razon Social es obligatorio',
            'address.required' => 'El campo Dirección es obligatorio',
            'city_id.required' => 'Debe elegir una ciudad',
            'invoice_first.required' => 'Debe agregar un numero de factura para la impresion',
            'invoice_first.numeric' => 'En los campos de factura deben ingresarse valores numericos',
            'invoice_second.required' => 'Debe agregar un numero de factura para la impresion',
            'invoice_second.numeric' => 'En los campos de factura deben ingresarse valores numericos',
            'print_invoice_number.required' => 'Debe agregar un numero de factura para la impresion',
            'print_invoice_number.numeric' => 'Los campos de la factura deben ser numericos'
        ];
    }

}
