<?php namespace App\Http\Request;

use App\Http\Request\Request;

class CurrenciesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'symbol' => 'required',
            'purchase_price' => 'required',
            'selling_price' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe insertar una description',
            'symbol.required' => 'Debe asignar un simbolo',
            'purchase_price.required' => 'Debe asignar un precio de compra',
            'selling_price.required' => 'Debe asignar un precio de vventa'
        ];
    }

}
