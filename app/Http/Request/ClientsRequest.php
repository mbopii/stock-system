<?php namespace App\Http\Request;

use App\Http\Request\Request;

class ClientsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_code' => 'required',
            'description' => 'required',
            'idnum' => 'required',
            'tax_code' => 'required',
            'tax_name' => 'required',
            'city_id' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'client_code.required' => 'Debe asignar un codigo al Cliente',
            'description.required' => 'Debe insertar un nombre al Cliente',
            'idnum.required' => 'El campo cedula es obligatorio',
            'tax_code.required' => 'El campo Razon Social es obligatorio',
            'tax_name.required' => 'El campo Ruc es obligatorio',
            'city_id.required' => 'Debe elegir una ciudad'
        ];
    }

}
