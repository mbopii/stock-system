<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Descripcion del Producto</label>
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Codigo de Barras</label>
    {!! Form::text('bar_code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Unidad de Medida</label>
    {!! Form::text('metric_unit_id', null, [ 'id' => 'select_metric_unit']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Marca</label>
    {!! Form::text('brand_id', null, ['id' => 'select_brand']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Categoría</label>
    {!! Form::text('category_id', null, [ 'id' => 'select_categories']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Cantidad Mínima</label>
    {!! Form::text('min_quantity', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Cantidad Máxima</label>
    {!! Form::text('max_quantity', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Porcentaje de Comisión</label>
    {!! Form::number('commission_percentage', null, ['class' => 'form-control', 'step' => '.01']) !!}
</div>

<div class="form-group">
    <label for="client_name" class="control-label mb-1">Estado del Producto:</label>
    {!! Form::select('active',[0 => 'Inactivo', 1 => 'Activo'], null, ['class' => 'form-control']) !!}
</div>


{{--@if (\Sentinel::getUser()->hasAccess('branches.view.all'))--}}
{{--<div class="form-group">--}}
{{--<label for="cc-payment" class="control-label mb-1">Sucursal</label>--}}
{{--{!! Form::text('branch_id', null, ['id' => 'select_branches']) !!}--}}
{{--</div>--}}
{{--@else--}}
{{--{!! Form::hidden('branch_id', $branch_id) !!}--}}
{{--@endif--}}
