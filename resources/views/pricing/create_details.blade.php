@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('pricing.index') }}">Precios</a></li>
            <li class="active">Nuevo - Detalles</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('pricing.create.details', $pricingHeader->id) }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Nombre</label>
                                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Categoría</label>
                                        {!! Form::text('category_id', null, ['id' => 'select_categories']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Marca</label>
                                        {!! Form::text('brand_id', null, ['id' => 'select_brands']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <a href="{{ route('pricing.create.details', $pricingHeader->id) }}"
                                       class="btn btn-warning btn-sm"><i class="fa fa-reply-all"></i> Ver Todos</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles para la lista {{ $pricingHeader->title }}</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                @include('pricing.partials.field_details')
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>
    {!! Form::open(['route' => ['pricing.details.store', $pricingHeader->id ], 'method' => 'post']) !!}
    {!! Form::close() !!}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
@endsection

@section('js')
    @include('partials.row_destroy')
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_categories').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $categoriesJson !!}
        });

        $('#select_brands').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $brandsJson !!}
        });
    </script>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $('#products_price').on('click', 'tbody tr', function (event) {
            $(this).addClass('highlight').siblings().removeClass('highlight');
            var detail_id = $(this).attr('id');
            var product_id = $(this).find('.product_id').val();
            console.log(product_id);
            swal({
                title: 'Inserte el nuevo precio',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Look up',
                showLoaderOnConfirm: true,
                preConfirm: (price) => {
                    return fetch(`/pricing/edit/${detail_id}/${product_id}/${price}`)
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)
                            }
                            return response.json()
                        })
                        .catch(error => {
                            swal.showValidationError(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                console.log(result.value.data.price);
                $(this).find('.selling_price').text(result.value.data.price);
                $(this).find('.profit').text(result.value.data.profit);
                $(this).find('.profit_percentage').text(result.value.data.profit_percentage);
                if (result.value) {
                    swal(
                        'Exito!',
                        'Precio modificado exitosamente.',
                        'success'
                    )
                }
            })
        });

    </script>
@endsection