@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('pricing.index') }}">Precios</a></li>
            <li class="active">Todos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-10">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('pricing.index') }}" class="print">
                            <input type="hidden" name="q" value="q">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Lista</label>
                                    {!! Form::select('price_id', $pricingHeaders ,0, ['id' => 'select_payments_form', 'required' => 'required', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
                                </div>
                                <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                    <i class="fa fa-file-pdf-o"></i> PDF
                                </button>
                                <button class="btn btn-success btn-sm" name="download" value="xls">
                                    <i class="fa fa-file-excel-o"></i> Excel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="preloader"></div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Lista de Precios</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="nowrap order-column table-striped compact table-bordered" style="width: 100%"
                               id="products_table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Artículo</th>
                                <th>Existencia</th>
                                <th>Lista 1</th>
                                <th>Lista 2</th>
                                <th>Lista 3</th>
                                <th>Costo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-edit" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="new_price" class="modal-content form-horizontal" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <input type="hidden" name="sales" value="sales">
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" name="product_id" value="" id="edit_product_id">
                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Costo</label>
                            {!! Form::text('product_cost', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'product_cost', 'disabled']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Lista 1</label>
                            {!! Form::text('list_1', null, ['class' => 'form-control', 'required', 'id' => 'product_list_1']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Lista 2</label>
                            {!! Form::text('list2', null, ['class' => 'form-control', 'required', 'id' => 'product_list_2']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Lista 3</label>
                            {!! Form::text('list_3', null, ['class' => 'form-control', 'required', 'id' => 'product_list_3']) !!}
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary update_price">Agregar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        var xhr;
        var response;
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        // Add new Row on table
        $(document).ready(function () {
            var table = $('#products_table').DataTable({
                createdRow: function (row, data, dataIndex) {
                    $(row).addClass(data.id + "_item");
                },
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin reultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                    searchPlaceholder: "Buscar Productos...",
                    search: "",
                },
                pageLength: 25,
                keys: {
                    keys: [13, 38, 40]
                },
                dom: ' <"search"f><"top"l>rt<"bottom"ip><"clear">',

                columns: [
                    {data: "id"},
                    {data: "product"},
                    {data: "stock"},
                    {data: "list_1"},
                    {data: "list_2"},
                    {data: "list_3"},
                    {data: "purchase_price"},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<button class="btn btn-success btn-sm btn-info edit_modal" type="button" ' +
                                'data-id="' + row.id + '" data-name="' + row.product + '" data-cost="' + row.purchase_price + '"' +
                                ' data-price1="' + row.list_1 + '" data-price2="' + row.list_2 + '" data-price3="' + row.list_3 + '">' +
                                '<i class="fa fa-pencil"></i> Editar</button>';
                        }
                    }
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('all.products') }}',
                    type: 'POST',
                    data: {_token: '{!! csrf_token() !!}'}
                }

            });
        });

        $(document).on('click', '.edit_modal', function () {
            $("#edit_product_id").val($(this).data('id'));
            $("#editor-title").text($(this).data('name'));
            $("#product_cost").val($(this).data('cost'));
            $("#product_list_1").val($(this).data('price1'));
            $("#product_list_2").val($(this).data('price2'));
            $("#product_list_3").val($(this).data('price3'));
            $('#modal-edit').modal('toggle');
        });

        $(document).on("submit", "#new_price", function (e) {
            e.preventDefault();
            $("#preloader").show();
            var product_id = $("#edit_product_id");
            var list_1 = $("#product_list_1");
            var list_2 = $("#product_list_2");
            var list_3 = $("#product_list_3");
            var cost = $("#product_cost");
            var name = $("#editor-title").text();

            xhr && xhr.abort();

            xhr = $.ajax({
                url: rootURL + 'pricing/update',
                method: "PATCH",
                data: {
                    product_id: product_id.val(),
                    list_1: list_1.val(),
                    list_2: list_2.val(),
                    list_3: list_3.val(),
                    _token: '{!! csrf_token() !!}'
                },
                success: function (result) {
                    if (result.error == false) {
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        // Reemplazamos la fila en la tabla dinámica
                        $("." + product_id.val() + '_item').replaceWith(
                            '<tr class="' + product_id.val() + '_item odd" role="row">' +
                            '<td class="sorting_1">' + product_id.val() + '</td>' +
                            '<td>' + name + '</td>' +
                            '<td>' + result.data.stock + '</td>' +
                            '<td>' + list_1.val() + '</td>' +
                            '<td>' + list_2.val() + '</td>' +
                            '<td>' + list_3.val() + '</td><' +
                            'td>' + cost.val() + '</td><' +
                            'td><button class="btn btn-success btn-sm btn-info edit_modal" type="button" ' +
                            'data-id="' + product_id.val() + '" data-name="' + name + '" data-cost="' + cost.val() + '"' +
                            ' data-price1="' + list_1.val() + '" data-price2="' + list_2.val() + '" data-price3="' + list_3.val() + '">' +
                            '<i class="fa fa-pencil"></i> Editar</button></td></tr>'
                        );
                        $("#modal-edit").modal('toggle');
                        swal('Exito!', result.message, 'success');
                    } else {
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        $("#modal-edit").modal('toggle');
                        swal('Error', result.message, 'error');
                    }
                },
                error: function () {
                    alert("this is not working");
                    $('#preloader').fadeOut('slow', function () {
                        $(this).css('display', 'none');
                    });
                    $("#modal-edit").modal('toggle');

                }
            });
        });
    </script>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>
    <style>
        .search {
            width: 850px;
        }

        .search .dataTables_filter input[type=search] {
            width: 400px;
            height: 25px;
        }
    </style>
    <style>
        div#preloader {
            background: #fefefea3 url("../images/loading.gif") no-repeat scroll center center;
            height: 100%;
            width: 100%;
            left: 0;
            overflow: visible;
            position: fixed;
            top: 0;
            z-index: 999;
            display: none;
        }
    </style>
@endsection