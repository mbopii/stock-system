<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Descripcion</label>
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
