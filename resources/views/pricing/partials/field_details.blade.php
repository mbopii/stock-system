<table class="table" id="products_price">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Producto</th>
        <th scope="col">Precio de Compra</th>
        <th scope="col">Precio de Venta</th>
        <th scope="col">Ganancia</th>
        <th scope="col">% Ganancia</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($pricingDetails as $detail)
        <tr data-id="{{ $detail->id }}" id="{{ $detail->id }}">
            <td>{{ $detail->id }}</td>
            <td>{{ $detail->title }}{{ Form::hidden('product_id', $detail->product_id, ['class' => 'product_id']) }}</td>
            <td>{{ number_format($detail->purchase_price, 0, ',', '.') }}</td>
            <td><label class="selling_price">{{ $detail->selling_price }}</label></td>
            <td><label class="profit">{{ $detail->profit }}</label></td>
            <td><label class="profit_percentage">{{ $detail->profit_percentage }}</label></td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $pricingDetails->appends(['category_id' => $category_id,
                        'brand_id' => $brand_id,
                        'title' => $title, 'q' => $q]) }}

<style>
    .row tbody tr.highlight td {
        background-color: #ccc;
    }
</style>