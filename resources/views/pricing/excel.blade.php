@extends('reports.excel.template')

@section('content')
    <tr>
        <th scope="col">#</th>
        <th scope="col">Artículo</th>
        <th scope="col">Existencia</th>
        <th scope="col">Costo</th>
        <th scope="col">Precio</th>
    </tr>
    <tbody>
    @foreach ($data as $pricing)
        <tr class="even pointer" data-id="{{ $pricing->id }}">
            <th scope="row">{{ $pricing->product->id }}</th>
            <td>{{ $pricing->product->title }}</td>
            <td>0</td>
            <td>{{ $pricing->purchase_price }}</td>
            <td>{{ $pricing->selling_price }}</td>
        </tr>
    @endforeach
    </tbody>
@endsection