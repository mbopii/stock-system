@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="#">Stock</a></li>
            <li class="active">Resumen</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('stocks.index') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Nombre</label>
                                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Categoría</label>
                                        {!! Form::text('category_id', null, ['id' => 'select_categories']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Marca</label>
                                        {!! Form::text('brand_id', null, ['id' => 'select_brands']) !!}
                                    </div>
                                </div>
{{--                                @if(\Sentinel::getUser()->hasAccess('branches.view.all'))--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="cc-payment" class="control-label mb-1">Sucursal</label>--}}
{{--                                            {!! Form::text('branch_id', null, ['id' => 'select_branches']) !!}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @else--}}
                                    <input type="hidden" name="branch_id" value="{{\Sentinel::getUser()->branch_id}}">
{{--                                @endif--}}
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <a href="{{ route('stocks.index') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Stock General del Sistema</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                @if(\Sentinel::getUser()->hasAccess('branches.view.all'))
                                    <th scope="col">Sucursal</th>@endif
                                <th scope="col">Producto</th>
                                <th scope="col">Min</th>
                                <th scope="col">Max</th>
                                <th scope="col">Actual</th>
                                <th scope="col">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($stocks as $stock)
                                <tr data-id="{{ $stock->id }}">
                                    <th scope="row">{{ $stock->id }}</th>
                                    @if(\Sentinel::getUser()->hasAccess('branches.view.all'))
                                        <td>{{ $stock->description }}</td>
                                    @endif
                                    <td>{{ $stock->title }}</td>
                                    <td>{{ $stock->min_quantity }}</td>
                                    <td>{{ $stock->max_quantity }}</td>
                                    <th class="stock_available">{{ $stock->available }}</th>
                                    <td>

                                        <button class="btn btn-info btn-sm btn-update" type="button">
                                            <i class="fa fa-retweet"></i> Actualizar
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $stocks->appends(['title' => $title, 'branch_id' => $branch_id,
                         'brand_id' => $brand_id, 'category_id' => $category_id , 'q' => $q])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_categories').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $categoriesJson !!}
        });

        $('#select_brands').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $brandsJson !!}
        });

        $('#select_branches').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $branchesJson !!}
        });
    </script>

    <script>
        $('.btn-update').click(function () {
            let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
            var row = $(this).parents('tr');
            var id = row.data('id');
            var xhr;
            swal({
                title: 'Actualizar Stock',
                html:
                '<input id="swal-input1" class="swal2-input" placeholder="Nueva Cantidad">' +
                '<textarea id="swal-input2" class="swal2-textarea" placeholder="Motivo de Actualización">',
                focusConfirm: false,
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    xhr && xhr.abort();

                    return xhr = $.ajax({
                        url: rootURL + 'stocks/update',
                        method: "POST",
                        data: {
                            stock_id: id,
                            new_stock: $('#swal-input1').val(),
                            observation: $('#swal-input2').val(),
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (result) {
                            return result;
                        },
                        error: function () {
                            return false;
                        }
                    });
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                if (result.value){
                    console.log(result);
                    let response = JSON.parse(result.value);
                    if (response.error == false){
                       row.find('.stock_available').text($('#swal-input1').val());
                        swal("Exito", 'Stock Moodificado exitosamente', 'success');
                    }else{
                        swal("Error", 'Ocurrio un error al intentar actualizar el registro', 'error');
                    }
                }
            }).catch(swal.noop);
        });
    </script>
@endsection