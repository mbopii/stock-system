<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Porcentaje de Comisión</label>
    {!! Form::number('commission_percentage', null, ['class' => 'form-control', 'step' => '.01']) !!}
</div>
