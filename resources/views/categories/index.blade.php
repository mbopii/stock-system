@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('categories.index') }}">Categorías</a></li>
            <li class="active">Todas</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('categories.create') }}">
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-plus"></i> Nuevo
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('categories.index') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Nombre</label>
                                        {!! Form::text('description', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <a href="{{ route('categories.index') }}" class="btn btn-warning btn-sm"><i class="fa fa-reply-all"></i> Ver Todos</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Categorías</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Ultima Modificación</th>
                                <th scope="col">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $category)
                                <tr data-id="{{ $category->id }}">
                                    <th scope="row">{{ $category->id }}</th>
                                    <td>{{ $category->description }}</td>
                                    <td>{{ $category->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $category->updated_at->format('Y-m-d') }}</td>
                                    <td>
                                        <a href="{{ route('categories.edit', $category->id) }}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil"></i> Editar
                                            </button>
                                        </a>

                                        <button class="btn btn-danger btn-sm btn-delete" type="button">
                                            <i class="fa fa-times"></i> Eliminar
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $categories->appends(['description' => $description, 'q' => $q])->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['categories.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('js')
    @include('partials.row_destroy')
@endsection