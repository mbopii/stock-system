@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">Dash</li>
        </ol>
    </div>
@endsection

@section('content')

    {{--Opciones Principales para el sistema--}}
    <div class="col-lg-3 col-md-6">
        <a href="{{ route('sales.create') }}">
            <div class="social-box sales twitter">
                <i class="fa fa-shopping-cart"></i>
                <ul>
                    <li>
                        <sctrong>
                            <span>Nueva </span>
                        </sctrong>
                    </li>
                    <li>
                        <sctrong>
                            <span>Venta</span>
                        </sctrong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </a>
    </div>

    <div class="col-lg-3 col-md-6">
        <a href="{{ route('purchases.create') }}">
            <div class="social-box sales google-plus">
                <i class="fa fa-shopping-bag"></i>
                <ul>
                    <li>
                        <sctrong>
                            <span>Nueva </span>
                        </sctrong>
                    </li>
                    <li>
                        <sctrong>
                            <span>Compra</span>
                        </sctrong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="{{ route('pricing.index') }}">
            <div class="social-box sales facebook">
                <i class="fa fa-address-book"></i>
                <ul>
                    <li>
                        <sctrong>
                            <span>Listas </span>
                        </sctrong>
                    </li>
                    <li>
                        <sctrong>
                            <span>Precios</span>
                        </sctrong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </a>
    </div>

    <div class="col-lg-3 col-md-6">
        <a href="{{ route('products.index') }}">
            <div class="social-box sales linkedin">
                <i class="fa fa-address-card"></i>
                <ul>
                    <li>
                        <sctrong>
                            <span>Lista </span>
                        </sctrong>
                    </li>
                    <li>
                        <sctrong>
                            <span>Productos</span>
                        </sctrong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </a>
    </div>
    {{--Fin Opciones Principales para el Sistema--}}


    <div class="col-md-4">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <div class="media-body">
                            <h2 class="text-light display-6">Productos</h2>
                            <p>Mas Vendidos</p>
                        </div>
                    </div>
                </div>
                @if (is_object($productsMostSale))
                    <ul class="list-group list-group-flush">
                        @foreach ($productsMostSale as $sales)
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-check"></i> {{ $sales->product->title }} <span
                                            class="badge badge-primary pull-right">{{ number_format($sales->p_quantity, 0, ',', '.') }}</span></a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </section>
        </aside>
    </div>

    <div class="col-md-4">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">

                        <div class="media-body">
                            <h2 class="text-light display-6">Compras</h2>
                            <p>Por Proveedor</p>
                        </div>
                    </div>
                </div>
                @if (is_object($purchaseByProvider))
                    <ul class="list-group list-group-flush">
                        @foreach ($purchaseByProvider as $purchase)
                            <li class="list-group-item">
                                <a href="#"> <i class="fa fa-check"></i> {{ $purchase->provider->description }} <span
                                            class="badge badge-primary pull-right">Gs. {{ number_format($purchase->p_sub_total, 0, ',', '.') }}</span></a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </section>
        </aside>
    </div>

    <div class="col-xl-2 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-widget text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Productos</div>
                        <div class="stat-digit">{{ number_format($productsCount, 0, ',', '.') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-2 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user text-warning border-warning"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Clientes</div>
                        <div class="stat-digit">{{ number_format($clientsCount, 0, ',', '.') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-2 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-truck text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Proveedores</div>
                        <div class="stat-digit">{{ number_format($providersCount, 0, ',', '.') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-2 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-shopping-cart text-dark border-dark"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Compras</div>
                        <div class="stat-digit">{{ number_format($purchasesCount, 0, ',', '.') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-2 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-save text-info border-info"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Ventas</div>
                        <div class="stat-digit">{{ number_format($salesCount, 0, ',', '.') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <!-- Right Panel -->

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="assets/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
    <script>
        (function ($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>
@endsection