<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Stock System</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('partials.css')
    @yield('css')
</head>

<body>
<style>
    body{
        font-size: 13px;
        line-height: 1;
    }
    .navbar .navbar-nav li > a {
        font-size: 13px;
    }
</style>
<!-- Left Panel -->

@include('partials.sidebar')

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">

    @include('partials.navbar')

    <div class="content mt-3">

        @include('partials.alerts')

        @yield('content')

    </div> <!-- .content -->
</div><!-- /#right-panel -->
</body>
@include('partials.js')
@yield('js')
</html>
