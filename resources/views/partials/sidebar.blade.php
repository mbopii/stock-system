<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="{{ '/images/logo_ferreteria.png' }}" width="15%" height=15%"
                                                   alt="Ferreteria"><span> Ferreteria Capiata</span></a>
            <a class="navbar-brand hidden" href="./"><img src="{{ '/images/logo_ferreteria.png' }}" alt="F"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <h3 class="menu-title">Inicio</h3><!-- /.menu-title -->
                <li class="active">
                    <a href="{{ url("/") }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
                </li>
                <li class="active">
                    <a href="{{ route("reports.index") }}"> <i class="menu-icon fa fa-laptop"></i>Reportes</a>
                </li>

                <h3 class="menu-title">Stock</h3><!-- /.menu-title -->
                <li>
                    <a href="{{ route('stocks.index') }}"> <i class="menu-icon fa fa-percent"></i>Resumen </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-drivers-license"></i>Productos</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-bookmark"></i><a
                                    href="{{ route('products.index') }}">Productos</a></li>
                        <li><i class="menu-icon fa fa-calendar"></i><a
                                    href="{{ route('brands.index') }}">Marcas</a>
                        </li>
                        <li><i class="menu-icon fa fa-align-right"></i><a href="{{ route('categories.index') }}">Categorias</a>
                        </li>
                        <li><i class="menu-icon fa fa-briefcase"></i><a href="{{ route('providers.index') }}">Proveedores</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('purchases.index')}}"> <i class="menu-icon fa fa-dollar"></i>Compras</a>
                </li>


                <h3 class="menu-title">Venta</h3><!-- /.menu-title -->
                <li>
                    <a href="{{route('pricing.index')}}"> <i class="menu-icon fa fa-address-book"></i>Lista de
                        Precios</a>
                </li>


                <li>
                    <a href="{{route('clients.index')}}"> <i class="menu-icon fa fa-child"></i>Clientes</a>
                </li>

                <li>
                    <a href="{{route('sales.index')}}"> <i class="menu-icon fa fa-money"></i>Ventas</a>
                </li>

                <li>
                    <a href="{{route('salesman.index')}}"> <i class="menu-icon fa fa-share-alt"></i>Vendedores</a>
                </li>


                <h3 class="menu-title">Configuraciones</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-shield"></i>Seguridad</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sticky-note"></i><a
                                    href="{{ route('permissions.index') }}">Permisos</a></li>
                        <li><i class="menu-icon fa fa-users"></i><a href="{{ route('roles.index') }}">Roles</a>
                        </li>
                        <li><i class="menu-icon fa fa-user"></i><a href="{{ route('users.index') }}">Usuarios</a>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"> <i class="menu-icon fa fa-credit-card"></i>Varios</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sticky-note"></i><a
                                    href="{{ route('payments_forms.index') }}">Forma de Pago</a></li>
                        <li><i class="menu-icon fa fa-database"></i><a
                                    href="{{ route('currencies.index') }}">Monedas</a>
                        </li>
                        <li><i class="menu-icon fa fa-filter"></i><a
                                    href="{{ route('metrics.index') }}">Unidades</a></li>
                        <li><i class="menu-icon fa fa-building"></i><a
                                    href="{{ route('branches.index') }}">Sucursales</a></li>
                        <li><i class="menu-icon fa fa-sitemap"></i><a
                                    href="{{ route('companies.index') }}">Empresas</a></li>
                        <li><i class="menu-icon fa fa-bank"></i><a
                                    href="{{ route('banks.index') }}">Bancos</a></li>
                        <li><i class="menu-icon fa fa-bell-o"></i><a
                                    href="{{ route('cashiers.index') }}">Cajas</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('departments.index')}}"> <i class="menu-icon fa fa-map-marker"></i>Ciudades</a>
                </li>
                <h3 class="menu-title">Miscelaneas</h3><!-- /.menu-title -->
                <li>
                    <a href="{{route('logout')}}"> <i class="menu-icon fa fa-user-times"></i>Cerrar Sesión</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->
