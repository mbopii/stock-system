<!-- Header-->
<header id="header" class="header" style="padding-bottom: 8px !important; padding-top: 10px !important">
    <div class="header-menu">
        <div class="col-sm-1">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa-hand-o-left"></i></a>
        </div>
        <div class="col-sm-11">
            @yield('breadcrumbs')
        </div>
    </div>
</header><!-- /header -->
<!-- Header-->