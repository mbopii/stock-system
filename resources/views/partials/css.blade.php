<link rel="stylesheet" href="{{ '/assets/css/normalize.css' }}">
<link rel="stylesheet" href="{{ '/assets/css/bootstrap.min.css' }}">
<link rel="stylesheet" href="{{ '/assets/css/font-awesome.min.css' }}">
<link rel="stylesheet" href="{{ '/assets/css/themify-icons.css' }}">
<link rel="stylesheet" href="{{ '/assets/css/flag-icon.min.css' }}">
<link rel="stylesheet" href="{{ '/assets/css/cs-skin-elastic.css' }}">
<!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
<link rel="stylesheet" href="{{ '/assets/scss/style.css' }}">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


<link rel="apple-touch-icon" href="{{ '/apple-icon.png' }}">
<link rel="shortcut icon" href="{{ '/favicon.ico' }}">

<link href="{{ '/assets/css/lib/vector-map/jqvmap.min.css' }}" rel="stylesheet">
<link href="{{ '/assets/css/sweet-alert/sweetalert2.min.css' }}" rel="stylesheet">