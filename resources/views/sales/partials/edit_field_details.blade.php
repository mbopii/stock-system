<table class="table" id="editing-example">
    <thead>
    <tr>
        <td data-name="id">Id</td>
        <td data-name="productName">Producto</td>
        <td data-name="lastPrice">Costo Ultima Compra</td>
        <td data-name="currentPrice">Precio Actual</td>
        <td data-name="quantity">Cantidad</td>
        <td data-name="subtotal">SubTotales</td>
    </tr>
    </thead>
    <tbody>
    @foreach ($purchase->purchaseDetails as $detail)
        <tr data-expanded="true">
            <td data-name="id">{{ $detail->id }}</td>
            <td data-name="productName">{{ $detail->product->title }}</td>
            <td data-name="lastPrice">0</td>
            <td data-name="currentPrice">{{ $detail->single_amount}}</td>
            <td data-name="quantity">{{ $detail->quantity }}</td>
            <td data-name="subtotal">{{ $detail->total_amount }}</td>
        </tr>
    @endforeach
    </tbody>
</table>