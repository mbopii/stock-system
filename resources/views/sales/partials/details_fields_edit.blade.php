<br>
<table class="table table-bordered" id="sales_table">
    <thead>
    <tr>
        <td>Id</td>
        <td>Producto</td>
        <td>Cantidad</td>
        <td>Lista Precio</td>
        <td>Precio Venta</td>
        <td>Nuevo Precio</td>
        <td>SubTotales</td>
        <td>Acciones</td>
    </tr>
    </thead>
    <tbody>
    @if ($header)
        @foreach ($header->invoiceDetails as $detail)
            <tr>
                <td>{{ $detail->product->id }}</td>
                <td>{{ $detail->product->title }}
                    <input type="hidden" name="sale_product_id[]" value="{{ $detail->product->id }}">
                    <input type="hidden" class="sales_quantity" name="sale_quantity[]" value="{{ $detail->quantity }}">
                    <input type="hidden" class="sales_custom_price" name="sale_custom_price[]"
                           value="@if ($detail->special_price != 0) {{ $detail->special_price }}
                           @else 0 @endif">
                </td>
                <td>{{ $detail->quantity }}</td>
                <td>{{ Form::select('sale_price_list[]', ['1' => 'Lista 1', '2' => 'Lista 2', '3' => 'Lista 3'] , $detail->pricing_list_id, ['class' => 'sales_price_list']) }}</td>
                <td><label class="sell_price">{{ $detail->price }}</label></td>
                <td>@if ($detail->special_price){{ $detail->special_price }}@endif</td>
                <td>{{ $detail->sub_total }}</td>
                <td></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>