<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Cliente</label>
            {!! Form::text('client_id', null, [ 'id' => 'select_client', 'required']) !!}
        </div>
    </div>
    <div class="col-md-1">
        <br>
        <button class="btn btn-info" id="add_client">Nuevo</button>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Empresa / Comprobante</label>
            {!! Form::text('company_id', $defaultValues['company_id'], ['id' => 'select_companies', 'required']) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Vendedor</label>
            {!! Form::text('salesman_id', $defaultValues['company_id'], ['id' => 'select_salesman', 'required']) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Nro Factura - Comprobante: </label><br><br>
            <label for="cc-payment" class="control-label mb-1" id="invoice_print_number">{{ $defaultValues['ruc'] }}</label>
        </div>
    </div>
</div>

<div class="row" id="client_details">
    <div class="col-md-3">
        <label for="client_name">Razon Social:</label>
        <label for="client_name" id="client_tax_name"></label>
        <br>
        <label for="client_name">Ruc:</label>
        <label for="client_name" id="client_tax_code"></label>
        <br>
        <label for="client_name">Cedula:</label>
        <label for="client_name" id="client_client_idnum"></label>
    </div>
    <div class="col-md-3">
        <label for="client_name">Cod.Cliente:</label>
        <label for="client_name" id="client_client_code"></label>
        <br>
        <label for="client_name">Direccion:</label>
        <label for="client_name" id="client_client_address"></label>
        <br>
        <label for="client_name">Teléfono:</label>
        <label for="client_name" id="client_client_telephone"></label>
    </div>
    <div class="col-md-2">
        <label for="client_name">Tipo de Venta:</label>
        {!! Form::select('sale_type_id',[1 => 'Contado', 2 => 'Credito'], null, ['class' => 'form-control', 'id' => 'select_type_sell']) !!}
    </div>
    <div class="col-md-1">
        <label for="client_name">Días del crédito:</label>
        {!! Form::text('credit_days',null, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'input_credit_days']) !!}
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Caja</label>
            {!! Form::text('cashier_id', $cashier->id, ['id' => 'select_cashier', 'required']) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Lista</label>
            {!! Form::text('pricing_id', 1, ['id' => 'select_pricing', 'required']) !!}
        </div>
    </div>
</div>




