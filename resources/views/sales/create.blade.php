@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </div>
@endsection

@section('content')
    <style>
        div#preloader {
            background: #fefefea3 url("../images/loading.gif") no-repeat scroll center center;
            height: 100%;
            width: 100%;
            left: 0;
            overflow: visible;
            position: fixed;
            top: 0;
            z-index: 999;
            display: none;
        }
    </style>
    {!! Form::open(['route' => ['sales.store', 'method' => 'post', 'id' => 'sellForm']]) !!}
    <div class="animated fadeIn">
        <div id="preloader"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Nueva Venta</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Carlayoutd -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                @include('sales.partials.fields')
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div>
                                    <div class="col-md-3">
                                        <button type="button" data-toggle="modal" data-target="#editor-modal"
                                                class="btn btn-sm btn-dark btn-block">
                                            <i class="fa fa-paper-plane-o"></i>&nbsp;
                                            <span id="payment-button-amount">Agregar Producto</span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <button id="payment-button" onclick="placeOrder()" class="btn btn-sm btn-info btn-block">
                                            <i class="fa fa-shopping-cart"></i>&nbsp;
                                            <span id="payment-button-amount">Vender</span>
                                        </button>
                                    </div>
                                    <div class="offset-3 col-md-3">
                                        Gs. <label class="sub_total">0</label>
                                        <input type="hidden" name="sub_total_hidden" class="sub_total_hidden" value="0">
                                    </div>
                                    <br>
                                    @include('sales.partials.details_fields')

                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
    {!! Form::close() !!}
    <div class="modal fade" tabindex="-1" role="dialog" id="editor-modal" aria-labelledby="editor-title">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" role="document" style="width: 1100px !important;">
                <div class="modal-header">
                    <h4 class="modal-title" id="editor-title">Agregar Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <table class="nowrap order-column table-striped compact table-bordered" style="width: 100%"
                           id="products_table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>COD</th>
                            <th>Artículo</th>
                            <th>Existencia</th>
                            <th>Lista 1</th>
                            <th>Lista 2</th>
                            <th>Lista 3</th>
                            <th>Costo</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    {{--<button class="btn btn-primary">Agregar</button>--}}
                    <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal para Nuevo Cliente--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="new-client-modal" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="new_client" class="modal-content form-horizontal" action="{{ route('clients.store') }}"
                      method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Nuevo Cliente</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <input type="hidden" name="sales" value="sales">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Codigo Cliente</label>
                            {!! Form::text('client_code', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'new_client_code']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Razon Social</label>
                            {!! Form::text('tax_name', null, ['class' => 'form-control', 'required', 'id' => 'new_client_tax_name']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Ruc</label>
                            {!! Form::text('tax_code', null, ['class' => 'form-control', 'required', 'id' => 'new_client_tax_code']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Dirección</label>
                            {!! Form::text('address', null, ['class' => 'form-control', 'required', 'id' => 'new_client_address']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Teléfono</label>
                            {!! Form::text('telephone', null, ['class' => 'form-control', 'required', 'id' => 'new_client_telephone']) !!}
                        </div>

                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Ciudad</label>
                            {!! Form::text('city_id', null, ['id' => 'new_select-city', 'required']) !!}
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary add_client">Agregar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>
    <style>
        .search {
            width: 700px;
        }

        .search .dataTables_filter input[type=search] {
            width: 369px;
            height: 25px;
        }
    </style>
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $("#sellForm").submit(function (e) {

                //disable the submit button
                $("#payment-button").attr("disabled", true);

                return true;

            });
        });
    </script>
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>

        $("#select_type_sell").change(function () {
            var selectedSellType = $(this).children("option:selected").val();
            console.log(selectedSellType);
            if (selectedSellType == '2'){
                $('#input_credit_days').removeAttr('disabled');
            }else{
                $('#input_credit_days').prop('disabled', true).val('');
            }
        });

        $('#input_credit_days').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
            if ((event.which < 48 || event.which > 60)) {
                event.preventDefault();
            }
        });

    </script>
    <script>
        var count = 0;
        var xhr;
        var response;
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';

        let clients =
                {!! $clientsJson !!}


        var client_list = $('#select_client').selectize({
                delimiter: ',',
                persist: false,
                openOnFocus: true,
                valueField: 'id',
                labelField: 'description',
                searchField: 'description',
                maxItems: 1,
                render: {
                    item: function (item, escape) {
                        return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                    }
                },
                onChange(value) {
                    console.log(value);
                    $("#preloader").show();
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: rootURL + 'clients/search/' + value,
                        method: "GET",
                        success: function (result) {
                            console.log(result);
                            var response = JSON.parse(result);
                            if (response.error == false) {
                                $('#client_tax_code').text(response.client.tax_code);
                                $('#client_tax_name').text(response.client.tax_name);
                                $('#client_client_code').text(response.client.client_code);
                                $("#client_client_address").text(response.client.address);
                                $("#client_client_telephone").text(response.client.telephone);
                                $('#client_client_idnum').val(response.client.idnum);
                                $('#preloader').fadeOut('slow', function () {
                                    $(this).css('display', 'none');
                                });
                            } else {
                                $('#preloader').fadeOut('slow', function () {
                                    $(this).css('display', 'none');
                                });
                                if (response.code == 10) {
                                    $("#new-client-modal").modal()
                                }
                            }
                        },
                        error: function () {
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                            console.log("THis is an error message");
                        }
                    });
                },
                options: clients
            });

        client_list[0].selectize.setValue(3398); // Cliente Ocasional

        $('.add_client').on('click', function (e) {
            e.preventDefault();
            console.log('Add new Client');
            if ($('#new_client_tax_code').val() == '' || $('#new_client_tax_name').val() == '' ||
                $('#new_client_id_num').val() == '' || $('#new_client_address').val() == '' || $('#new_client_telephone').val() == '' || $('#new_select').val() == '') {
                swal('Error', 'Debe completar todos los campos', 'error');
            }
            $("#preloader").show();

            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + 'clients/add_sales',
                method: "POST",
                data: {
                    description: $('#new_client_tax_name').val(),
                    client_code: $('#new_client_code').val(),
                    tax_code: $('#new_client_tax_code').val(),
                    tax_name: $('#new_client_tax_name').val(),
                    idnum: $('#new_client_tax_code').val(),
                    address: $('#new_client_address').val(),
                    telephone: $('#new_client_telephone').val(),
                    city_id: $('#new_select-city').val(),
                    _token: "{{ csrf_token() }}",
                },
                success: function (result) {
                    console.log(result);
                    var response = JSON.parse(result);
                    if (response.error == false) {
                        console.log("Client ADDED");
                        $('#client_name').val($('#new_client_name').val());
                        $('#client_tax_code').val($('#new_client_code').val());
                        $('#client_tax_name').val($('#new_client_tax_name').val());
                        $('#client_id_num').val($('#new_client_id_num').val());
                        client_list[0].selectize.addOption({
                            id: response.data.id,
                            description: $('#new_client_tax_name').val() + " - " + $('#new_client_code').val()
                        });
                        client_list[0].selectize.refreshOptions();
                        $('#select_client').val(response.data.id);
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        $("#new-client-modal").modal();
                    } else {
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        if (response.code == 10) {
                            $("#new-client-modal").modal()
                        }
                        swal('Error: ' + response.message);
                    }
                },
                error: function (result) {
                    $('#preloader').fadeOut('slow', function () {
                        $(this).css('display', 'none');
                    });
                    var response = JSON.parse(result);
                    console.log(response.message);
                    alert(response.message);
                }
            });
        });

        $('#add_client').click(function (e) {
            e.preventDefault();

            $('#client_tax_code').val('');
            $('#new_client_tax_code').val('');
            $('#new_client_address').val('');
            $('#new_client_telephone').val('');
            $('#new_select-city').val('');

            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + "clients/get_client_code",
                method: "GET",
                success: function (result) {
                    // let response = JSON.parse(result);
                    if (result.error == false) {
                        $('#new_client_code').val(result.client_code);

                        $("#new-client-modal").modal()
                    }
                },
                error: function () {
                    alert("this is not working");

                }
            });
        });

        $('#select_branches').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $branchesJson !!}
        });

        $('#select_salesman').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.name) + '</span></div>';
                }
            },
            options: {!! $salesmanJson !!}
        });


        $('#new_select-city').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $citiesJson !!}
        });

        $('#select_pricing_list').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            options: {!! $priceListJson !!},
            onChange: function (value) {
                console.log(value);
                for (var i = 0; i < response.pricing.length; i++) {
                    if (response.pricing[i].id == value) {
                        console.log("Change pricing list to " + response.pricing[i].price);
                        $('#product_price').val(response.pricing[i].price);
                    }
                    $('#pricing_list').val(value);
                }

                // Get the selected price

            }
        });

        $('#select_cashier').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.name) + '</span></div>';
                }
            },
            options: {!! $cashierJson !!},
        });


        $('#select_companies').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            onChange(value) {
                $("#preloader").show();
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: rootURL + 'companies/get_number/' + value,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        var response = JSON.parse(result);
                        if (response.error == false) {
                            $('#invoice_print_number').text(response.number);
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                        } else {
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                        }
                    },
                    error: function () {
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        console.log("THis is an error message");
                    }
                });
            },
            options: {!! $companiesJson !!}
        });

    </script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        var price_id;
        $('#select_pricing').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            onChange: function (value) {
               price_id = value;

            },
            options: {!! $priceListJson !!}
        });
        // Add new Row on table
        $(document).ready(function () {
            var data =
                    {!! $productsJson !!}
            var table2 = $('#sales_table').DataTable({
                    ordering: false,
                    language: {
                        lengthMenu: "Mostrar _MENU_ resultados por página",
                        sZeroRecords: "Sin resultados",
                        info: "Mostrando pagina _PAGE_ de _PAGES_",
                        infoEmpty: "Sin reultados",
                        sInfoFiltered: "Filtrando de _MAX_ resultados",
                    },
                    pageLength: 25,
                    // keys: true,
                    columns: [
                        {data: "id"},
                        {data: 'product'},
                        {data: 'quantity', className: 'products_quantity'},
                        {data: 'price_list'},
                        {data: 'sell_price'},
                        {data: "new_price", className: "alter_price"},
                        {data: 'subtotal'},
                        {
                            'data': 'Action', 'render': function (data, type, row, meta) {
                                return '<button class="btn btn-danger btn-sm btn-danger remove_product" type="button"><i class="fa fa-trash"></i></button>';
                            }
                        }

                    ]
                });
            var table = $('#products_table').DataTable({
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin reultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                    searchPlaceholder: "Buscar Productos...",
                    search: "",
                },
                keys: {
                    keys: [13, 38, 40]
                },
                dom: ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
                aaData: data,
                columns: [
                    {data: "id"},
                    {data: "bar_code"},
                    {data: "product"},
                    {data: "stock"},
                    {data: "list_1"},
                    {data: "list_2"},
                    {data: "list_3"},
                    {data: "purchase_price"},
                    {data: "quantity", sDefaultContent: ''},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<button class="btn btn-success btn-sm btn-success add_product" type="button"><i class="fa fa-cart-plus"></i> Agregar</button>';
                        }
                    }
                ]

            });

            // Edit cell with new plugin
            function myCallBackFunction(updatedCell, updatedRow, oldValue) {
                var newData = updatedCell.data();
                newData.toString();
                if (/[,\-]/.test(newData)) {
                    alert("No puede ingresar un numero con coma (,) utilice un punto para valores decimales (.)");
                    updatedCell.data(oldValue);
                    return false;
                }

                return true;

            }

            table.MakeCellsEditable({
                "onUpdate": myCallBackFunction,
                "inputCss": 'form-control',
                "columns": [8],
                "allowNulls": {
                    "columns": [8],
                    "errorClass": 'error'
                },
                "inputTypes": [
                    {
                        "column": 7,
                        "type": "text",
                        "options": null
                    }
                ]
            });

            $(document).on('change', '.products_quantity', function () {
                let list_id = $(this).parent('tr').find('.sales_price_list').val();
                let data = table2.row($(this).parents('tr')).data();
                let row = table2.row($(this).parents('tr'));
                let hiddenQuantity = $(this).parents('tr').find('.sales_quantity');
                let customPrice = $(this).parents('tr').find('.sales_custom_price');
                updatePrice(rootURL + 'products/find_price/' + data.id + "/" + list_id, row, hiddenQuantity, customPrice)
            });


            table2.MakeCellsEditable({
                "onUpdate": myCallBackFunction,
                "inputCss": 'form-control',
                "columns": [2, 5],
                "allowNulls": {
                    "columns": [2, 5],
                    "errorClass": 'error'
                },
                "inputTypes": [
                    {
                        "column": 2,
                        "type": "text",
                        "options": null
                    },
                    {
                        "column": 5,
                        "type": "text",
                        "options": null,
                    }
                ]
            });


            $('#sales_table tbody').on('click', '.remove_product', function () {
                let data = table2.row($(this).parents('tr')).data();

                table2.row($(this).parents("tr")).remove().draw();

                $('.sub_total').text(parseInt($('.sub_total').text()) - parseInt(data.subtotal));
                $('.sub_total_hidden').val(parseInt($('.sub_total_hidden').val()) - parseInt(data.subtotal));
                count = count - 1;
            });
            $("#products_table tbody").on("click", ".add_product", function () {
                let data = table.row($(this).parents('tr')).data();
                console.log('adding new row');
                console.log(data);

                // Get Product price !
                let list_id = $('#select_pricing').val();
                if (!list_id) {
                    swal(
                        'Error!',
                        'Debe elegir una lista de precio',
                        'error'
                    );
                    return;
                }
                if (data.quantity <= 0) {
                    swal(
                        'Error!',
                        'Debe asignar una cantidad',
                        'error'
                    );
                    return;
                }
                if (count == 14){
                    console.log("Product Limit");
                    swal('Error!',
                        'No puede agregar mas de 14 productos a una factura',
                        'error');
                        return;
                }
                $("#preloader").show();
                $('#editor-modal').modal('toggle');
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: rootURL + 'products/find_price/' + data.id + "/" + list_id,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        let response = JSON.parse(result);
                        if (response.error == false) {
                            let subTotalRow = parseFloat(response.price) * parseFloat(data.quantity);
                            let currentSubTotal = $('.sub_total');
                            let newSubtotal = (parseFloat(currentSubTotal.text()) + parseFloat(subTotalRow)).toFixed(0);
                            currentSubTotal.text(newSubtotal);
                            $('.sub_total_hidden').val(newSubtotal);
                            var pricingList;
                            console.log(price_id);
                            if (price_id == 1){
                                pricingList = '<select class="sales_price_list" name="sale_price_list[]">' +
                                '<option value="1" selected >Lista 1</option>'
                                + '<option value="2">Lista 2</option>'
                                + '<option value="2">Lista 3</option>'
                                + '</select>'
                            }else if (price_id == 2){
                                pricingList = '<select class="sales_price_list" name="sale_price_list[]">' +
                                    '<option value="1"  >Lista 1</option>'
                                    + '<option value="2" selected>Lista 2</option>'
                                    + '<option value="2">Lista 3</option>'
                                    + '</select>'
                            } else if (price_id == 3){
                                pricingList = '<select class="sales_price_list" name="sale_price_list[]">' +
                                    '<option value="1"  >Lista 1</option>'
                                    + '<option value="2" >Lista 2</option>'
                                    + '<option value="2" selected>Lista 3</option>'
                                    + '</select>'
                            } else {
                                pricingList = '<select class="sales_price_list" name="sale_price_list[]">' +
                                    '<option value="1" selected >Lista 1</option>'
                                    + '<option value="2">Lista 2</option>'
                                    + '<option value="2">Lista 3</option>'
                                    + '</select>'

                            }
                            table2.row.add({
                                    id: data.id,
                                    product: data.product + '<input type="hidden" name="sale_product_id[]" value="' + data.id + '">' +
                                        '<input type="hidden" class="sales_quantity" name="sale_quantity[]" value="' + data.quantity.toString().replace(/\,/g, '.') + '">' +
                                        '<input type="hidden" class="sales_custom_price" name="sale_custom_price[]" value="0">',
                                    quantity: data.quantity.toString().replace(/\,/g, '.'),
                                    price_list: pricingList,
                                    sell_price: '<label class="sell_price">' + response.price + '</label>',
                                    new_price: "",
                                    subtotal: subTotalRow.toFixed(0),
                                }
                            ).draw();
                            count = count + 1;
                            console.log("Product Count: " + count)
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                        } else {
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                            swal('Error', response.message, 'error');
                        }
                    },
                    error: function () {
                        alert("this is not working");
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                    }
                });
            });
            table.on('key-focus.dt', function (e, datatable, cell) {
                // Select highlighted row
                $(table.row(cell.index().row).node()).addClass('selected');
            });
            // Handle event when cell looses focus
            table.on('key-blur.dt', function (e, datatable, cell) {
                // Deselect highlighted row
                $(table.row(cell.index().row).node()).removeClass('selected');
            });
            table.on('key', function (e, datatable, key, cell, originalEvent) {
                if (key === 13) {

                }
            });

            $(document).on('change', '.sales_price_list', function () {
                let list_id = $('option:selected', this).val();
                let data = table2.row($(this).parents('tr')).data();
                let row = table2.row($(this).parents('tr'));
                let customPrice = $(this).parents('tr').find('.sales_custom_price');
                updatePrice(rootURL + 'products/find_price/' + data.id + "/" + list_id, row, null, customPrice)
            });


            $(document).on('change', '.alter_price', function () {
                let data = table2.row($(this).parents('tr')).data();
                let row = table2.row($(this).parents('tr'));
                let customPrice = $(this).parents('tr').find('.sales_custom_price');
                console.log(customPrice);
                updatePrice(rootURL + 'products/find_price/' + data.id + "/" + 1, row, null, customPrice)
            });

            function updatePrice(url, row, hiddenQuantity = null, customPrice) {
                console.log(customPrice);
                xhr && xhr.abort();
                $("#preloader").show();
                xhr = $.ajax({
                    url: url,
                    method: "GET",
                    success: function (result) {
                        let response = JSON.parse(result);
                        if (response.error == false) {
                            let price = 0;
                            if (table2.cell(row, 5).data()) {
                                price = table2.cell(row, 5).data();
                                customPrice.val(price);
                            } else {
                                price = response.price;
                                customPrice.val(0);
                            }
                            console.log("SelectedPrice: " + price);
                            let oldSubTotalRow = table2.cell(row, 6).data();
                            let quantity = table2.cell(row, 2).data();
                            let newSubTotal = (parseFloat(quantity) * parseFloat(price)).toFixed(0);
                            table2.cell(row, 4).data(response.price).draw();
                            table2.cell(row, 6).data(parseFloat(newSubTotal)).draw();


                            // Update the hidden inputs to send
                            var subTotal = $('.sub_total').text();
                            var total = parseFloat(subTotal) - parseFloat(oldSubTotalRow) + parseFloat(newSubTotal);
                            console.log(total);

                            $('.sub_total').text(total);
                            $('.sub_total_hidden').val(total);

                            if (hiddenQuantity) {
                                hiddenQuantity.val(quantity);
                            }
                            $('#preloader').fadeOut('slow', function () {
                                $(this).css('display', 'none');
                            });
                        }
                    },
                    error: function () {
                        alert("this is not working");
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                    }
                });
            }
        });

        // Add gif to submit button.
        function placeOrder() {
            $("#preloader").show();
            let form = document.getElementById('sellForm');
            form.submit();
        }


    </script>

@endsection
