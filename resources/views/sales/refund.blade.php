@extends('partials.layout') 
@section('breadcrumbs')
<div class="page-header float-right">
    <ol class="breadcrumb text-right">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li><a href="{{ route('sales.index') }}">Ventas</a></li>
        <li class="active">Devoluciones</li>
    </ol>
</div>
@endsection
 
@section('content')
<div class="animated fadeIn">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title"><b>Orden Nro:</b> {{$header->id}}</strong>
                </div>
                <div class="card-body">
                    <!-- Credit Card -->
                    <div id="pay-invoice">
                        <div class="card-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Fecha de
                                                Compra:</b> {{ $header->sale_date }}</label>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Cliente:</b> {{ $header->client->description }}
                                        </label>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Moneda:</b> {{ $header->currency->description }}
                                        </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($header->sale_type_id == 2)
                                    <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total/Monto Pagado</b>
                                                Gs. {{ number_format($header->total_amount, 0, ',', '.') }}
                                                / Gs. {{ number_format($header->total_amount - $header->amount_left, 0, ',', '.') }}
                                            </label> @else
                                    <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total:</b>
                                                Gs. {{ number_format($header->total_amount, 0, ',', '.') }}</label>                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Descuento: </b>
                                            Gs. {{ $header->total_discount }}</label>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Forma de Pago: </b>
                                            {{ $header->paymentForm->description }}</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Numero de Factura: </b>
                                            Gs. {{ $header->invoice_number }}</label>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Comprobante/Empresa: </b>
                                            {{ $header->company->description }}</label>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1"><b>Estado de la factura: </b>
                                            {{ $header->invoiceStatus->description }}</label>
                                </div>

                            </div>
                        </div>
                    </div </div>
                </div>
                <!-- .card -->
            </div>
            <!--/.col-->
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            {{ Form::open(['route' => ['sales.refund.do', $header->id], 'method' => 'post']) }}
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Producto</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Cantidad devuelta</th>
                                            <th scope="col">Cantidad a devolver</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($header->invoiceDetails as $detail)
                                        <tr>
                                            <th scope="row">{{ $detail->id }}</th>
                                            <td>{{ $detail->product->title }}</td>
                                            <td><label class="refund">{{ $detail->quantity }}</label></td>
                                            <td><label class="refund_quantity">{{ !is_null($detail->refunds->where('product_id', $detail->product_id)->first()) ? $detail->refunds->where('product_id', $detail->product_id)->first()->quantity : 0 }}</label></td>
                                            <td><input type="text" name="devolution[]" class="refund_test" autocomplete="off">
                                                <input
                                                    type="hidden" name="product_id[]" value="{{$detail->product_id}}"></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button id="payment-button" type="submit" class="btn btn-sm btn-info btn-block">
                                    <i class="fa fa-retweet"></i>&nbsp;
                                    <span id="payment-button-amount">Enviar</span>
                                </button>
                        </div>
                        {{ Form::close()}}
                    </div>
                </div>
                <!-- .card -->
            </div>
        </div>
    </div>
@endsection
 
@section('js')
    <script>
        $('.refund_test').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
            if ((event.which < 48 || event.which > 60)) {
                event.preventDefault();
            }
        });
        $('.refund_test').on('change', function(){
            var toRefund = parseInt($(this).val());
            var refunded = parseInt($(this).parents('tr').find('.refund_quantity').text());
            var quantity = parseInt($(this).parents('tr').find('.refund').text());


            if ((toRefund + refunded) > quantity){
                alert ("No puede devolver más mercaderias que las disponibles");
                $(this).val("");
            }
            if (quantity < toRefund){
                alert ("No puede devolver más mercaderias que las disponibles");
                $(this).val("");
            }
        });
    </script>
@endsection