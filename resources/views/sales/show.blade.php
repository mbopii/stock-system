@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Ver</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"><b>Orden Nro:</b> {{$header->id}}</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Fecha de
                                                Compra:</b> {{ $header->sale_date }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment"
                                               class="control-label mb-1"><b>Cliente:</b> {{ $header->client->description }}
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment"
                                               class="control-label mb-1"><b>Moneda:</b> {{ $header->currency->description }}
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        @if ($header->sale_type_id == 2)
                                            <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total/Monto Pagado</b>
                                                Gs. {{ number_format($header->total_amount - $header->total_discount, 0, ',', '.') }}
                                                / Gs. {{ number_format(($header->total_amount - $header->total_discount)- $header->amount_left, 0, ',', '.') }}
                                            </label>
                                        @else
                                            <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total:</b>
                                                Gs. {{ number_format($header->total_amount - $header->total_discount, 0, ',', '.') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Descuento/Interes: </b>
                                            Gs. {{ $header->total_discount }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Forma de Pago: </b>
                                            {{ $header->paymentForm->description }}</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Numero de Factura: </b>
                                            Gs. {{ $header->invoice_number }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Comprobante/Empresa: </b>
                                            {{ $header->company->description }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Estado de la factura: </b>
                                            {{ $header->invoiceStatus->description }}</label>
                                    </div>

                                </div>
                            </div>
                        </div
                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Lista de Precio</th>
                                        <th scope="col">Moneda</th>
                                        <th scope="col">Cambio</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Precio</th>
                                        <th scope="col">Precio con descuento</th>
                                        <th scope="col">SubTotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($header->invoiceDetails as $detail)
                                        <tr>
                                            <th scope="row">{{ $detail->id }}</th>
                                            <td>{{ $detail->product->title }}</td>
                                            <td>{{ $detail->priceList->description }}</td>
                                            <td>{{ $detail->currencies->description }}</td>
                                            <td>{{ $detail->change_fee }}</td>
                                            <td>{{ $detail->quantity }}</td>
                                            <td>{{ number_format($detail->price, 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->special_price, 0, ',', '.') }}</td>
                                            <td>{{ ($detail->special_subtotal == 0) ? number_format($detail->sub_total, 0, ',', '.') : number_format($detail->special_subtotal, 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div>
        </div>
    </div>

    @if ($header->sale_type_id == 2)
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pagos</strong>
                        </div>
                        <div class="card-body">
                            <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nro de Recibo</th>
                                            <th scope="col">Forma de Pago</th>
                                            <th scope="col">Banco</th>
                                            <th scope="col">Nro Cheque</th>
                                            <th scope="col">Fecha de Cobro</th>
                                            <th scope="col">Monto</th>
                                            <th scope="col">Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($header->vouchers as $voucher)
                                            <tr>
                                                <th scope="row">{{ $voucher->id }}</th>
                                                <td>{{ $voucher->voucher_number }}</td>
                                                <td>{{ $voucher->paymentMethods->description }}</td>
                                                <td>@if(isset($voucher->banks)){{ $voucher->banks->description }}@endif</td>
                                                <td>{{ $voucher->check_number }}</td>
                                                <td>{{ $voucher->check_date }}</td>
                                                <td>{{ number_format($voucher->amount, 0, ',', '.') }}</td>
                                                <td>{{ $voucher->created_at }}</td>
                                                <td>
                                                    @if($voucher->status_id == 1)
                                                        <a href="{{ route('sales.voucher.cancel', $voucher->id) }}">
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="fa fa-times"
                                                                   @if($voucher->status_id != 2 ) style="color:#ffffff"
                                                                   @else style="color: #696969;" @endif></i>
                                                            </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
            </div>
        </div>
    @endif
@endsection
