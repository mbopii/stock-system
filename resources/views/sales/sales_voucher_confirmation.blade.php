@extends('partials.layout')
@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('purchases.index') }}">Compras</a></li>
            <li class="active">Recibos de Crédito</li>
        </ol>
    </div>
@endsection

@section('content')
    {!! Form::open( ['route' => ['sales.voucher.submit'], 'method' => 'post']) !!}
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Añadir Recibo</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Numero de Recibo</label> {!! Form::text('voucher_number',
                                null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Monto a Pagar</label> {!! Form::text('amount',
                                $amountLeft, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Forma de Pago</label> {!! Form::text('payment_id',
                                null, ['id' => 'select_payment_method']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Nro de Cheque</label> {!! Form::text('check_number',
                                null, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'insert_check_number'])
                                !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Fecha de Cobro</label> {!! Form::date('check_date',
                                    null, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'insert_check_date'])
                                    !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Banco</label> {!! Form::text('bank_id',
                                null, [ 'id' => 'select_banks']) !!}
                                </div>

                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-paper-plane-o"></i>&nbsp;
                                    <span id="payment-button-amount">Guardar</span>
                                </button>
                            </div>

                        </div>
                    </div>
                    <!-- .card -->
                </div>
                <!--/.col-->

            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Facturas a pagar</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="row">
                                    @foreach ($arrayResponse as $purchase)
                                        <input type="hidden" name="purchase_id[]" value="{{ $purchase['id'] }}">
                                        <input type="hidden" name="total_amount" value="{{ $amountLeft }}">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">ID: </label> {{ $purchase['id'] }}
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">Comprobante: </label> {{ $purchase['company'] }}
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">Proveedor: </label> {{ $purchase['provider'] }}
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">Fecha de
                                                    Compra: </label> {{ $purchase['purchase_date'] }}
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">Nro de
                                                    Factura: </label> {{ $purchase['invoice_number'] }}
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-payment" class="control-label mb-1"
                                                       style="font-weight:bold">Monto: </label> {{ number_format($purchase['amounts'], 0, ',', '.') }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1" style="font-weight:bold">Total: </label> {{ number_format($amountLeft, 0, ',', '.') }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#input_credit_days').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
            if ((event.which < 48 || event.which > 60)) {
                event.preventDefault();
            }
        });
    </script>
    <script>
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var xhr;
        $('#select_payment_method').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            onChange(value) {
                console.log(value);
                if (value == 2) {
                    $('#insert_check_number').removeAttr('disabled');
                    $('#insert_check_date').removeAttr('disabled');
                    $('#select_banks').removeAttr('disabled').removeAttr('locked');
                } else {
                    $('#insert_check_number').prop('disabled', true).val('');
                    $('#insert_check_date').prop('disabled', true).val('');
                    $('#select_banks').prop('disabled', true).val('');
                }
            },
            options: {!! $paymentMethodsJson !!}
        });

        $('#select_banks').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },

            options: {!! $banksJson !!}
        });
    </script>
@endsection
