<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Codigo Cliente</label>
    {!! Form::text('client_code', isset($clientCode) ? $clientCode : null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Cedula</label>
    {!! Form::text('idnum', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Razon Social</label>
    {!! Form::text('tax_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Ruc</label>
    {!! Form::text('tax_code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Dirección</label>
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Ciudad</label>
    {!! Form::text('city_id', null, ['id' => 'select-city']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Teléfono</label>
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Límite de crédito</label>
    {!! Form::number('credit_limit', null, ['class' => 'form-control']) !!}
</div>
