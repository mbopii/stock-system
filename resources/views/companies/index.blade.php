@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('companies.index') }}">Empresas</a></li>
            <li class="active">Todas</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Empresas</strong>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('companies.create') }}">
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-plus"></i> Nuevo
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Razon Social</th>
                                <th scope="col">Ruc</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($companies as $company)
                                <tr data-id="{{ $company->id }}">
                                    <th scope="row">{{ $company->id }}</th>
                                    <td>{{ $company->description }}</td>
                                    <td>{{ $company->tax_code }}</td>
                                    <td>{{ $company->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        <a href="{{ route('companies.edit', $company->id) }}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil"></i> Editar
                                            </button>
                                        </a>

                                        <button class="btn btn-danger btn-sm btn-delete" type="button">
                                            <i class="fa fa-times"></i> Eliminar
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['companies.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('js')
    @include('partials.row_destroy')
@endsection