<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Ruc</label>
    {!! Form::text('tax_code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Timbrado</label>
    {!! Form::text('stamping', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Dirección</label>
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Telefono</label>
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Ciudad</label>
    {!! Form::text('city_id', null, ['id' => 'select-city']) !!}
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <label for="cc-payment" class="control-label mb-1">Nro de Factura</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            {!! Form::text('invoice_first', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::text('invoice_second', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-8">
            {!! Form::text('print_invoice_number', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>




