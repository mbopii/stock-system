@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('roles.index') }}">Roles</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => ['roles.store', 'method' => 'post']]) !!}
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Nuevo Permiso</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                @include('roles.partials.fields')
                                <div>
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-paper-plane-o"></i>&nbsp;
                                        <span id="payment-button-amount">Guardar</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Permisos para el nuevo rol</strong>
                    </div>
                    <div class="card-body">
                        @include('roles.partials.permissions')
                    </div>
                </div> <!-- .card -->
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection