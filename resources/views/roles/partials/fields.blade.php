<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Identificador</label>
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Descripcion</label>
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
