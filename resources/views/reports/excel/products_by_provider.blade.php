@extends('reports.excel.template')

@section('content')
    <tr>
        <th scope="col">#</th>
        <th scope="col">Fecha de Compra</th>
        <th scope="col">Producto</th>
        <th scope="col">Proveedor</th>
        <th scope="col">Cantidad</th>
        <th scope="col">Precio</th>
        <th scope="col">Moneda</th>
    </tr>
    <tbody>
    @foreach ($data as $response)
        <tr >
            <th scope="row">-</th>
            <td>{{ $response->purchase_date }}</td>
            <td>{{ $response->title}}</td>
            <td>{{ $response->description }}</td>
            <td>{{ $response->quantity }}</td>
            <td>{{ $response->single_amount }}</td>
            <td>{{ $response->currency }}</td>
        </tr>
    @endforeach

@endsection