@extends('reports.excel.template')

@section('content')
    <tr>
        <th>Id</th>
        <th>Cajero</th>
        <th>Fecha</th>
        <th>Moneda</th>
        <th>Monto Inicial</th>
        <th>Monto Final</th>
        <th>Diferencia</th>
    </tr>
    <tbody>
    @foreach ($data as $response)
        <tr>
            <th scope="row">{{ $response->id }}</th>
            <td>{{ $response->cashier->name  }}</td>
            <td>{{ $response->opened_at }}</td>
            <td>{{ $response->currency->symbol }}</td>
            <td>{{ number_format($response->opening_amount, 0, ',', '.') }}</td>
            <td>{{ number_format($response->closing_amount, 0, ',', '.') }}</td>
            <td>{{ number_format($response->opening_amount - $response->closing_amount, 0, ',', '.') }}</td>
        </tr>
    @endforeach

@endsection
