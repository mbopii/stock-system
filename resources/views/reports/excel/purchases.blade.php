@extends('reports.excel.template')

@section('content')
    <tr>
        <th scope="col">#</th>
        <th scope="col">Fecha de Compra</th>
        <th scope="col">Empresa</th>
        <th scope="col">Proveedor</th>
        <th scope="col">Nro de Factura</th>
        <th scope="col">Monto</th>
        <th scope="col">Creado</th>
    </tr>
    <tbody>
    @foreach ($data as $purchaseHeader)
        <tr data-id="{{ $purchaseHeader->id }}">
            <th scope="row">{{ $purchaseHeader->id }}</th>
            <td>{{ $purchaseHeader->purchase_date }}</td>
            <td>@if($purchaseHeader->company_id == 1)
                    --- @else{{ $purchaseHeader->companies->description }}@endif</td>
            <td>{{ $purchaseHeader->provider->description }}</td>
            <td>{{ $purchaseHeader->invoice_number }}</td>
            <td>{{ number_format($purchaseHeader->total_amount, 0, ',', '.') }}</td>
            <td>{{ $purchaseHeader->created_at->format('Y-m-d') }}</td>
        </tr>
    @endforeach

@endsection