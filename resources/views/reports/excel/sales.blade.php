@extends('reports.excel.template')

@section('content')
    <tr>
        <th scope="col">#</th>
        <th scope="col">Fecha de Compra</th>
        <th scope="col">Empresa/Comprobante</th>
        <th scope="col">Cliente</th>
        <th scope="col">Nro de Factura</th>
        <th scope="col">Monto</th>
        <th scope="col">Moneda</th>
        <th scope="col">Creado</th>
    </tr>
    <tbody>
    @foreach ($data as $saleHeader)
        <tr class="even pointer" data-id="{{ $saleHeader->id }}">
            <th scope="row">{{ $saleHeader->id }}</th>
            <td>{{ $saleHeader->sale_date }}</td>
            <td>@if ($saleHeader->company->id != 1)
                    Factura @endif{{ $saleHeader->company->description }}</td>
            <td>{{ $saleHeader->client->description }}</td>
            <td>{{ $saleHeader->invoice_number }}</td>
            <td>{{ $saleHeader->total_amount}}</td>
            <td>{{ $saleHeader->currency->symbol }}</td>
            <td>{{ $saleHeader->created_at->format('Y-m-d') }}</td>
        </tr>
    @endforeach

@endsection