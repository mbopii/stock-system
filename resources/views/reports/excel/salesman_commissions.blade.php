@extends('reports.excel.template')

@section('content')
    <tr>
        <th>Id</th>
        <th>Vendedor</th>
        <th>Factura</th>
        <th>Fecha</th>
        <th>Tipo de comisión</th>
        <th>Porcentaje a comisionar</th>
        <th>Monto a comisionar</th>
    </tr>
    <tbody>
    @foreach ($data as $commission)
        <tr>
            <th scope="row">{{ $commission->id }}</th>
            <td>{{ $commission->salesman->name  }}</td>
            <td>{{ $commission->detail->invoiceHeader->invoice_number }}</td>
            <td>{{ $commission->detail->invoiceHeader->sale_date }}</td>
            <td>{{ $commission->commission_motive == 'product' ? 'Producto' : 'Categoria' }}</td>
            <td>{{ $commission->commission_percentage }}</td>
            <td>{{ number_format($commission->amount, 0, ',', '.') }}</td>

        </tr>
    @endforeach

@endsection