@extends('reports.excel.template') 
@section('content')
<tr>
    <th scope="col">#</th>
    <th scope="col">Cliente</th>
    <th scope="col">Factura</th>
    <th scope="col">Fecha</th>
    <th scope="col">Monto</th>
    <th scope="col">Plazo</th>
    <th scope="col">Pendiente</th>
</tr>
<tbody>
    @foreach ($data as $response)
    <tr>
        <th scope="row">{{ $response->id }}</th>
        <td>{{ $response->client }}</td>
        <td>{{ $response->invoice_number }}</td>
        <td>{{ $response->purchase_date }}</td>
        <td>{{ $response->total_amount }}</td>
        <td>{{ $response->credit_days }}</td>
        <td>{{ $response->amount_left }}</td>
    </tr>
    @endforeach
@endsection