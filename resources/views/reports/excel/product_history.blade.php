@extends('reports.excel.template')

@section('content')
    <tr>
        <th scope="col">Fecha de Transacción</th>
        <th scope="col">Producto</th>
        <th scope="col">Factura</th>
        <th scope="col">Cliente/Proveedor</th>
        <th scope="col">Movimiento</th>
        <th scope="col">Precio/Costo</th>
        <th scope="col">Moneda</th>
        <th scope="col">Cambio del Día</th>
        <th scope="col">a PYG</th>
        <th scope="col">Cant.</th>
        <th scope="col">Antes</th>
        <th scope="col">Despues</th>
    </tr>
    <tbody>
    @foreach ($data as $response)
        <tr>
            <td>{{ $response['transaction_date'] }}</td>
            <td>{{ $response['product'] }}</td>
            <td>{{ $response['invoice_number'] }}</td>
            <td>{{ $response['client'] }}</td>
            <td>{{ $response['movement_type'] }}</td>
            <td>{{ number_format($response['price'], 0, ',', '.') }}</td>
            <td>{{ $response['currency'] }}</td>
            <td>{{ $response['fee'] }}</td>
            <td>{{ number_format($response['price'] * $response['fee'], 0, ',', '.') }}</td>
            <td>{{ $response['quantity'] }}</td>
            <td>{{ $response['before'] }}</td>
            <td>{{ $response['after'] }}</td>
            {{--                                        <td>{{ $response->available }}</td>--}}
        </tr>
    @endforeach

@endsection