@extends('reports.excel.template') 
@section('content')
<tr>
    <th scope="col">#</th>
    <th scope="col">Categoria</th>
    <th scope="col">Marca</th>
    <th scope="col">Producto</th>
    <th scope="col">Codigo de Barra</th>
    <th scope="col">Cantidad</th>
</tr>
<tbody>
    @foreach ($data as $response)
    <tr>
        <th scope="row">{{ $response->id }}</th>
        <td>{{ $response->categories->description }}</td>
        <td>{{ $response->brands->description }}</td>
        <td>{{ $response->title }}</td>
        <td>{{ $response->bar_code }}</td>
        <td>{{ $response->stock[0]->available }}</td>
    </tr>
    @endforeach
@endsection