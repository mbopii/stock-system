@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Todos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.products.history') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Desde</label>
                                        {!! Form::text('date_start', null, [ 'id' => 'datepicker', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Hasta</label>
                                        {!! Form::text('date_end', null, [ 'id' => 'datepicker2', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Producto</label>
                                        {!! Form::text('product_id', null, ['id' => 'select_products', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.products.history') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if ($final)
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-11">
                                <strong class="card-title">Movimientos</strong>
                            </div>

                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Fecha de Transacción</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Factura</th>
                                    <th scope="col">Cliente/Proveedor</th>
                                    <th scope="col">Movimiento</th>
                                    <th scope="col">Precio/Costo</th>
                                    <th scope="col">Moneda</th>
                                    <th scope="col">Cambio del Día</th>
                                    <th scope="col">a PYG</th>
                                    <th scope="col">Cant.</th>
                                    <th scope="col">Antes</th>
                                    <th scope="col">Despues</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($final['data'] as $response)
                                    <tr>
                                        <td>{{ $response['transaction_date'] }}</td>
                                        <td>{{ $response['product'] }}</td>
                                        <td>{{ $response['invoice_number'] }}</td>
                                        <td>{{ $response['client'] }}</td>
                                        <td>{{ $response['movement_type'] }}</td>
                                        <td>{{ number_format($response['price'], 0, ',', '.') }}</td>
                                        <td>{{ $response['currency'] }}</td>
                                        <td>{{ $response['fee'] }}</td>
                                        <td>{{ number_format($response['price'] * $response['fee'], 0, ',', '.') }}</td>
                                        <td>{{ $response['quantity'] }}</td>
                                        <td>{{ $response['before'] }}</td>
                                        <td>{{ $response['after'] }}</td>
{{--                                        <td>{{ $response->available }}</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $final['query']->appends(['date_start' => $date_start, 'date_end' => $date_end, 'product_id' => $product_id,
                                             'q' => $q, ])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_products').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            options: {!! $productsJson !!}
        });

    </script>
    <script>
        $(document).ready(function () {
            $('.print_button').click(function (e) {
                e.preventDefault();
                let printUrl = $(this).attr('href');
                let win = window.open(printUrl);
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });

        });
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });
        $(function () {
            $("#datepicker2").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection