@extends('reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Vendedor</th>
            <th>Factura</th>
            <th>Fecha</th>
            <th>Porcentaje a comisionar</th>
            <th>Monto a comisionar</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $commission)
            <tr>
                <th scope="row">{{ $commission->id }}</th>
                <td>{{ $commission->user->description  }}</td>
                <td>{{ $commission->order->invoice_number }}</td>
                <td>{{ $commission->order->sale_date }}</td>
                <td>{{ $commission->commission_percentage }}</td>
                <td>{{ number_format($commission->amount, 0, ' ,', ' .') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
