@extends('reports.pdf.template') 
@section('content')
<table class="table table-striped table-condensed table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Artículo</th>
            <th>Existencia</th>
            <th>Lista 1</th>
            <th>Lista 2</th>
            <th>Lista 3</th>
            <th>Costo</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $response)
        <tr>
            <th scope="row">{{ $response['id'] }}</th>
            <td>{{ $response['product'] }}</td>
            <td>{{ $response['stock'] }}</td>
            <td>{{ $response['list_1'] }}</td>
            <td>{{ $response['list_2'] }}</td>
            <td>{{ $response['list_3'] }}</td>
            <td>{{$response['purchase_price']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection