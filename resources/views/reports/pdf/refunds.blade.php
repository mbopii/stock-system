@extends('reports.pdf.template') 
@section('content')
<table class="table table-striped table-condensed table-bordered">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Factura</th>
            <th scope="col">Producto</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Fecha</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $response)
        <tr>
            <td>{{ $response->id }}</td>
            <td>{{ $response->invoiceHeader->invoice_number }}</td>
            <td>{{ $response->product->title }}</td>
            <td>{{ $response->quantity }}</td>
            <td>{{ $response->created_at->format('Y-m-d') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection