@extends('reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Fecha de Compra</th>
            <th scope="col">Empresa</th>
            <th scope="col">Proveedor</th>
            <th scope="col">Nro de Factura</th>
            <th scope="col">Monto / Pendiente</th>
            <th scope="col">Creado</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $purchaseHeader)
            <tr data-id="{{ $purchaseHeader->id }}">
                <th scope="row">{{ $purchaseHeader->id }}</th>
                <td>{{ $purchaseHeader->purchase_date }}</td>
                <td>@if($purchaseHeader->company_id == 1)
                        --- @else{{ $purchaseHeader->companies->description }}@endif</td>
                <td>{{ $purchaseHeader->provider->description }}</td>
                <td>{{ $purchaseHeader->invoice_number }}</td>
                <td>{{ number_format($purchaseHeader->total_amount, 0, ',', '.') }}</td>
                <td>{{ $purchaseHeader->created_at->format('Y-m-d') }}</td>

            </tr>
        @endforeach
        </tbody>
    </table>
@endsection