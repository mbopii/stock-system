@extends('reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th colspan="2">Ventas por Empresa</th>
        </tr>
        </thead>
        @for($i = 1; $i <= count($data['companiesArray']) -1; $i++)
            <tr>
                <th scope="col">{{ $data['companiesArray'][$i]['name'] }}</th>
                <td>@if(is_null($data['companiesArray'][$i]['amount'])) --- @else
                        Gs. {{ number_format($data['companiesArray'][$i]['amount'], 0, ',', '.') }}@endif</td>
            </tr>
        @endfor
    </table>

    <br>

    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th colspan="4">Datos del Día - {{ $data['reportDate'] }}</th>
        </tr>
        </thead>
        <tr>
            <th scope="col">Ventas Totales</th>
            <td>{{ $data['totalSale']  }}</td>

            <th scope="col">Total Facturado</th>
            <td>Gs. {{ number_format($data['totalCollected']->sum, 0, ',', '.') }}</td>
        </tr>

        <tr>
            <th scope="col">Productos Vendidos</th>
            <td>{{ $data['productsOut'] }}</td>

            <th scope="col">Clientes Agregados</th>
            <td>{{{ $data['newClients'] }}}</td>
        </tr>

        <tr>
            <th scope="col">Total Compras</th>
            <td>{{ $data['purchasesCount'] }}</td>

            <th scope="col">Compras Facturadas</th>
            <td>Gs. {{ number_format($data['purchasesCollected'], 0, ',', '.') }}</td>
        </tr>

    </table>
@endsection