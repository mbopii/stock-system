@extends('reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Cliente</th>
            <th scope="col">Factura</th>
            <th scope="col">Fecha</th>
            <th scope="col">Monto</th>
            <th scope="col">Plazo</th>
            <th scope="col">Pendiente</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $response)
            <tr>
                <th scope="row">{{ $response->id }}</th>
                <td>{{ $response->client }}</td>
                <td>{{ $response->invoice_number }}</td>
                <td>{{ $response->sale_date }}</td>
                <td>{{ number_format($response->total_amount, 0, ',', '.') }}</td>
                <td>{{ $response->credit_days }}</td>
                <td>{{ number_format($response->amount_left, 0, ',', '.') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection