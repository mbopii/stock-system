@extends('reports.pdf.template')
@section('content')
    <table class="table table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Fecha de Compra</th>
            <th scope="col">Empresa/Comprobante</th>
            <th scope="col">Cliente</th>
            <th scope="col">Nro de Factura</th>
            <th scope="col">Monto</th>
            <th scope="col">Moneda</th>
            <th scope="col">Creado</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $saleHeader)
            <tr data-id="{{ $saleHeader->id }}">
                <th scope="row">{{ $saleHeader->id }}</th>
                <td>{{ $saleHeader->sale_date }}</td>
                <td>@if ($saleHeader->company->id != 1)
                        Factura @endif{{ $saleHeader->company->description }}</td>
                <td>{{ $saleHeader->client->description }}</td>
                <td>{{ $saleHeader->invoice_number }}</td>
                <td>{{ number_format($saleHeader->total_amount, 0, ',', '.') }}</td>
                <td>{{ $saleHeader->currency->symbol }}</td>
                <td>{{ $saleHeader->created_at->format('Y-m-d') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection