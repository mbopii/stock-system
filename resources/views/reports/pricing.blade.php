@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Todos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.pricing') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Descripcion</label>
                                        {!! Form::text('description', null, [ 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Marca</label>
                                        {!! Form::text('brand_id', null, ['id' => 'select_brand', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Categorías</label>
                                        {!! Form::text('category_id', null, ['id' => 'select_category', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.pricing') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-11">
                                <strong class="card-title">Lista de Precios</strong>
                            </div>

                        </div>
                        <div class="card-body">
                            @if (array_key_exists('data', $data))
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Artículo</th>
                                        <th>Existencia</th>
                                        <th>Lista 1</th>
                                        <th>Lista 2</th>
                                        <th>Lista 3</th>
                                        <th>Costo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($data['data'] as $response)
                                    <tr>
                                        <th scope="row">{{ $response['id'] }}</th>
                                        <td>{{ $response['product'] }}</td>
                                        <td>{{ $response['stock'] }}</td>
                                        <td>{{ $response['list_1'] }}</td>
                                        <td>{{ $response['list_2'] }}</td>
                                        <td>{{ $response['list_3'] }}</td>
                                        <td>{{ number_format($response['purchase_price'], 0, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $data['paginate']->appends(['description' => $description, 'category_id' => $category_id,
                                             'q' => $q, 'brand_id' => $brand_id])->links() }}
                                             @endif
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_brand').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $brandsJson !!}
        });

        $('#select_category').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $categoriesJson !!}
        });

    </script>
    <script>
        $(document).ready(function () {
            $('.print_button').click(function (e) {
                e.preventDefault();
                let printUrl = $(this).attr('href');
                let win = window.open(printUrl);
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });

        });
    </script>
@endsection