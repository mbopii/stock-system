@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('reports.index') }}">Reportes</a></li>
            <li class="active">Productos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.products.all') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Producto</label>
                                        {!! Form::text('product_id', null, ['id' => 'select_products', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Marca</label>
                                        {!! Form::text('brand_id', null, ['id' => 'select_brands', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Categorias</label>
                                        {!! Form::text('category_id', null, ['id' => 'select_categories', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.products.history') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if ($products)
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-11">
                                <strong class="card-title">Productos</strong>
                            </div>

                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Marca</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Codigo de Barra</th>
                                    <th scope="col">Cantidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $response)
                                    <tr>
                                        <th scope="row">{{ $response->id }}</th>
                                        <td>{{ $response->categories->description }}</td>
                                        <td>{{ $response->brands->description }}</td>
                                        <td>{{ $response->title }}</td>
                                        <td>{{ $response->bar_code }}</td>
                                        <td>{{ $response->stock[0]->available }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $products->appends(['brand_id' => $brand_id, 'product_id' => $product_id,
                                             'q' => $q, ])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_products').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            options: {!! $productsJson !!}
        });

        $('#select_brands').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $brandsJson !!}
        });

        $('#select_categories').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $categoriesJson !!}
        });

    </script>
    <script>
        $(document).ready(function () {
            $('.print_button').click(function (e) {
                e.preventDefault();
                let printUrl = $(this).attr('href');
                let win = window.open(printUrl);
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });

        });
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });
        $(function () {
            $("#datepicker2").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection