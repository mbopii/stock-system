@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Todos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.users.commissions') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Desde</label>
                                        {!! Form::text('date_start', null, [ 'id' => 'datepicker', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Hasta</label>
                                        {!! Form::text('date_end', null, [ 'id' => 'datepicker2', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Usuario</label>
                                        {!! Form::text('user_id', null, ['id' => 'select_users']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.users.commissions') }}"
                                       class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-9">
                            <strong class="card-title">Comisiones por Usuario</strong>
                        </div>

                        <div class="col-md-3">
                            Total: Gs.<strong class="card-title">{{ number_format($sum, 0, ',', '.') }}</strong>
                        </div>

                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Vendedor</th>
                                <th>Factura</th>
                                <th>Total Facturado</th>
                                <th>Fecha</th>
                                <th>Porcentaje a comisionar</th>
                                <th>Monto a comisionar</th>
                                <th>Factura</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($commissions as $commission)
                                <tr>
                                    <th scope="row">{{ $commission->id }}</th>
                                    <td>{{ $commission->user->description  }}</td>
                                    <td>{{ $commission->order->invoice_number }}</td>
                                    <td>{{ number_format($commission->order->total_amount, 0, ',', '.') }}</td>
                                    <td>{{ $commission->order->sale_date }}</td>
                                    <td>{{ $commission->commission_percentage }}</td>
                                    <td>{{ number_format($commission->amount, 0, ' ,', ' .') }}</td>
                                    <td>
                                        <a href="{{ route('sales.show', $commission->order->id) }}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa fa-search"></i> Ver Factura</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $commissions->appends(['user_id' => $user_id, 'date_start' => $date_start,
                                         'q' => $q, 'date_end' => $date_end])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_users').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'name',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $userJson !!}
        });

    </script>
    <script>
        $(document).ready(function () {
            $('.print_button').click(function (e) {
                e.preventDefault();
                let printUrl = $(this).attr('href');
                let win = window.open(printUrl);
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });

        });

        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });
        $(function () {
            $("#datepicker2").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection
