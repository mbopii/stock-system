@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Todos</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.sales.day') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Fecha</label>
                                        {!! Form::text('date_start', null, [ 'id' => 'datepicker', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.sales.day') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Hoy</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Ventas por empresa - {{ $reportDate }}</strong>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table">
                            @for($i = 1; $i <= count($companiesArray) -1; $i++)
                                <tr>
                                    <th scope="col">{{ $companiesArray[$i]['name'] }}</th>
                                    <td>@if(is_null($companiesArray[$i]['amount'])) --- @else
                                            Gs. {{ number_format($companiesArray[$i]['amount'], 0, ',', '.') }}@endif</td>
                                </tr>
                            @endfor
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Datos del día - {{ $reportDate }}</strong>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th scope="col">Ventas Totales</th>
                                <td>{{ $totalSale  }}</td>

                                <th scope="col">Total Facturado</th>
                                <td>Gs. {{ number_format($totalCollected->sum, 0, ',', '.') }}</td>
                            </tr>

                            <tr>
                                <th scope="col">Productos Vendidos</th>
                                <td>{{ $productsOut }}</td>

                                <th scope="col">Clientes Agregados</th>
                                <td>{{{ $newClients }}}</td>
                            </tr>

                            <tr>
                                <th scope="col">Total Compras</th>
                                <td>{{ $purchasesCount }}</td>

                                <th scope="col">Compras Facturadas</th>
                                <td>Gs. {{ number_format($purchasesCollected, 0, ',', '.') }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $(document).ready(function () {
            $('.print_button').click(function (e) {
                e.preventDefault();
                let printUrl = $(this).attr('href');
                let win = window.open(printUrl);
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });

        });
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });
        $(function () {
            $("#datepicker2").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection