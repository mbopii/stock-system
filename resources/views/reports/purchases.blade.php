@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('reports.index') }}">Reportes</a></li>
            <li class="active">Compras</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-10">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('reports.purchases') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Desde</label>
                                        {!! Form::text('date_start', null, [ 'id' => 'datepicker', 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Hasta</label>
                                        {!! Form::text('date_end', null, [ 'id' => 'datepicker2', 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Comprobante</label>
                                        {!! Form::text('company_id', null, ['id' => 'select_companies', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Factura</label>
                                        {!! Form::text('invoice_number', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Proveedor</label>
                                        {!! Form::text('provider_id', null, ['id' => 'select_provider']) !!}
                                    </div>
                                </div>

                                @if(\Sentinel::getUser()->hasAccess('branches.view.all'))
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="cc-payment" class="control-label mb-1">Sucursal</label>
                                            {!! Form::text('branch_id', null, ['id' => 'select_branches']) !!}
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="branch_id" value="{{\Sentinel::getUser()->branch_id}}">
                                @endif
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="client_name" class="control-label mb-1">Estado de Factura:</label>
                                        {!! Form::select('invoice_status',[0 => 'Seleccione', 1 => 'Facturado', 2 => 'Cancelado'], null, ['class' => 'form-control', 'id' => 'select_type_sell']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="client_name" class="control-label mb-1">Forma de Pago:</label>
                                        {!! Form::select('payment_form_id',[0 => 'Seleccione', 1 => 'Contado', 2 => 'Credito'], null, ['class' => 'form-control', 'id' => 'select_type_sell']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <button class="btn btn-danger btn-sm" name="download" value="pdf">
                                        <i class="fa fa-file-pdf-o"></i> PDF
                                    </button>
                                    <button class="btn btn-success btn-sm" name="download" value="xls">
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </button>
                                    <a href="{{ route('reports.purchases') }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-reply-all"></i> Ver Todos</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-9">
                            <strong class="card-title">Compras</strong>
                        </div>

                        <div class="col-md-3">
                            Total: Gs.<strong class="card-title">{{ number_format($sum, 0, ',', '.') }}</strong>
                        </div>

                    </div>
                    @if (!is_null($purchasesHeader))
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Fecha de Compra</th>
                                    <th scope="col">Empresa</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">Nro de Factura</th>
                                    <th scope="col">Monto / Pendiente</th>
                                    <th scope="col">Creado</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($purchasesHeader as $purchaseHeader)
                                    <tr data-id="{{ $purchaseHeader->id }}">
                                        <th scope="row">{{ $purchaseHeader->id }}</th>
                                        <td>{{ $purchaseHeader->purchase_date }}</td>
                                        <td>@if($purchaseHeader->company_id == 1)
                                                --- @else{{ $purchaseHeader->companies->description }}@endif</td>
                                        <td>{{ $purchaseHeader->provider->description }}</td>
                                        <td>{{ $purchaseHeader->invoice_number }}</td>
                                        <td>{{ number_format($purchaseHeader->total_amount, 0, ',', '.') }}
                                            / {{ number_format($purchaseHeader->total_amount - $purchaseHeader->amount_left, 0, ',', '.') }}</td>
                                        <td>{{ $purchaseHeader->created_at->format('Y-m-d') }}</td>
                                        <td>
                                            <a href="{{ route('purchases.show', $purchaseHeader->id) }}">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-search"></i></button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $purchasesHeader->appends(['date_start' => $date_start, 'date_end' => $date_end,
                         'provider_id' => $provider_id, 'q' => $q, 'branch_id' => $branch_id,
                         'invoice_number' => $invoice_number, 'invoice_status' => $invoice_status, 'payment_form_id' => $payment_form_id, 
                         'company_id' => $company_id])->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['purchases.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection
@section('js')
    @include('partials.row_destroy')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $('#select_companies').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $companiesJson !!}
        });

        $('#select_branches').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $branchesJson !!}
        });

        $('#select_provider').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $providersJson !!}
        });

        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });
        $(function () {
            $("#datepicker2").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
@endsection