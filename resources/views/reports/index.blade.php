@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">Reportes</li>
        </ol>
    </div>
@endsection

@section('content')

    <div class="col-md-6">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <div class="media-body">
                            <h2 class="text-light display-6">Reportes</h2>
                            <p>Del Sistema</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="{{ route('reports.sales') }}"> <i class="fa fa-check"></i> Ventas
                            {{--<span class="badge badge-primary pull-right"></span>--}}
                        </a>

                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.sales.day') }}"> <i class="fa fa-check"></i> Ventas del día
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.purchases') }}"> <i class="fa fa-check"></i> Compras
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.products.history') }}"> <i class="fa fa-check"></i> Historial de Producto
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.products.provider') }}"> <i class="fa fa-check"></i> Productos por Proveedor
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.clients.credit') }}"> <i class="fa fa-check"></i> Clientes a Crédito
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.providers.credit') }}"> <i class="fa fa-check"></i> Proveedores a Crédito
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.products.all') }}"> <i class="fa fa-check"></i> Productos
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.pricing') }}"> <i class="fa fa-check"></i> Lista de Precios
                        </a>
                    </li>
                    <li class="list-group-item">
                            <a href="{{ route('reports.refunds') }}"> <i class="fa fa-check"></i> Devoluciones
                            </a>
                        </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.salesman.commissions') }}"> <i class="fa fa-check"></i> Comisiones por Vendedor
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.users.commissions') }}"> <i class="fa fa-check"></i> Comisiones por Usuario
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('reports.cashiers.movements') }}"> <i class="fa fa-check"></i> Movimientos de Caja
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
    </div>

@endsection
