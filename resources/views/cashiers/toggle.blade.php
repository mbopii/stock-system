@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('cashiers.index') }}">Cajas</a></li>
            <li class="active">@if ($cashier->is_open) Cerrar @else Abrir @endif Caja</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">@if ($cashier->is_open) Cerrar @else Abrir @endif Caja</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                {!! Form::open(['route' => ['cashiers.toggle.do', $cashier->id], 'method' => 'post']) !!}
                                @foreach (\App\Models\Currencies::all() as $currency)
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Cantidad
                                            en {{ $currency->symbol }}</label>
                                        @if (is_null($cashier->lastMovement()))
                                            {!! Form::text('amount_' . $currency->symbol, null, ['class' => 'form-control']) !!}
                                        @else
                                            {!! Form::text('amount_' . $currency->symbol, $cashier->lastMovement()->where('currency_id', $currency->id)->orderBy('id', 'desc')->first()->closing_amount, ['class' => 'form-control', 'readonly']) !!}
                                        @endif
                                    </div>

                                @endforeach
                                <div>
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-paper-plane-o"></i>&nbsp;
                                        <span id="payment-button-amount">Guardar</span>
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>

@endsection
