@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('cashiers.index') }}">Cajas</a></li>
            <li class="active">Extracción</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Extracción de Caja</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                {!! Form::open(['route' => ['cashiers.extraction.do', $cashier->id], 'method' => 'post']) !!}
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Cantidad </label>
                                    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Motivo </label>
                                    {!! Form::text('reason', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Moneda </label>
                                    {!! Form::select('currency_id', [0 => 'Seleccione'] + \App\Models\Currencies::all()->pluck('description', 'id')->toArray(), 1, ['class' => 'form-control']) !!}
                                </div>

                                <div>
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-paper-plane-o"></i>&nbsp;
                                        <span id="payment-button-amount">Guardar</span>
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
        </div>
    </div>

@endsection
