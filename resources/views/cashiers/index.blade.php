@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('cashiers.index') }}">Cajas</a></li>
            <li class="active">Todas</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Filtros de Busqueda</strong>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('cashiers.create') }}">
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-plus"></i> Nuevo
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('cashiers.index') }}">
                            <input type="hidden" name="q" value="q">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Nombre</label>
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                    <a href="{{ route('cashiers.index') }}" class="btn btn-warning btn-sm"><i class="fa fa-reply-all"></i> Ver Todos</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-11">
                            <strong class="card-title">Marcas</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Ultima Modificación</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($cashiers as $cashier)
                                <tr data-id="{{ $cashier->id }}">
                                    <th scope="row">{{ $cashier->id }}</th>
                                    <td>{{ $cashier->name }}</td>
                                    <td>{{ $cashier->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $cashier->updated_at->format('Y-m-d') }}</td>
                                    <td><button class="btn btn-sm @if($cashier->is_open) btn-success @else btn-danger @endif">{{ $cashier->is_open ? 'Abierto' : 'Cerrado' }}</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('cashiers.edit', $cashier->id) }}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil"></i> Editar
                                            </button>
                                        </a>

                                        <a href="{{ route('cashiers.toggle', $cashier->id) }}">
                                            <button class="btn btn-secondary btn-sm">
                                                <i class="fa fa-hand-o-right"></i> @if($cashier->is_open) Cerrar @else Abrir @endif Caja
                                            </button>
                                        </a>

                                        <a href="{{ route('cashiers.extraction.show', $cashier->id) }}">
                                            <button class="btn btn-info btn-sm">
                                                <i class="fa fa-money"></i> Extracción
                                            </button>
                                        </a>

                                        <button class="btn btn-danger btn-sm btn-delete" type="button">
                                            <i class="fa fa-times"></i> Eliminar
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $cashiers->appends(['name' => $name, 'q' => $q])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['cashiers.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('js')
    @include('partials.row_destroy')
@endsection
