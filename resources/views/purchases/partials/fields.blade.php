<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Proveedor</label>
            {!! Form::text('provider_id', null, ['id' => 'select_provider', 'required' , 'autocomplete' => 'off']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Razon Social</label>
            {!! Form::text('provider_tax_name', null, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'provider_tax_name']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">RUC</label>
            {!! Form::text('provider_tax_code', null, ['class' => 'form-control', 'id' => 'provider_tax_code', 'disabled' => 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Dirección</label>
            {!! Form::text('provider_address', null, ['class' => 'form-control', 'id' => 'provider_address', 'disabled' => 'disabled']) !!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Fecha de Compra</label>
            {!! Form::text('purchase_date', null, ['class' => 'form-control', 'required', 'id' => 'datepicker', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Timbrado</label>
            {!! Form::text('stamping', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Moneda</label>
            {!! Form::text('currency_id', 1, ['id' => 'select_currency', 'autocomplete' => 'off']) !!}
        </div>
    </div>


    @if(\Sentinel::getUser()->hasAccess('branches.view.all'))
        <div class="col-md-3">
            <div class="form-group">
                <label for="cc-payment" class="control-label mb-1">Sucursal</label>
                {!! Form::text('branch_id', 1, ['id' => 'select_branches', 'required']) !!}
            </div>
        </div>
    @else
        <input type="hidden" name="branch_id" value="{{\Sentinel::getUser()->branch_id}}">
    @endif

    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Numero de Factura</label>
            {!! Form::text('invoice_number', null, ['class' => 'form-control', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Empresa</label>
            {!! Form::text('company_id', 1, ['id' => 'select_company', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Forma de Pago</label>
            {!! Form::select('payment_form_id', [0 => 'Seleccione...', 1 => 'Contado', 2 => 'Credito'], 1, ['id' => 'select_payments_form', 'required', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-1">
        <label for="client_name">Días del crédito:</label>
        {!! Form::text('credit_days',null, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'input_credit_days']) !!}
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="cc-payment" class="control-label mb-1">Caja</label>
            {!! Form::text('cashier_id', null, ['id' => 'select_cashier', 'autocomplete' => 'off', 'required']) !!}
        </div>
    </div>
</div>





