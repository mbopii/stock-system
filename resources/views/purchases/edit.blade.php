@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('purchases.index') }}">Compras</a></li>
            <li class="active">Editar</li>
        </ol>
    </div>
@endsection

@section('content')
    {!! Form::model($purchase, ['route' => ['purchases.update', $purchase->id], 'method' => 'patch', 'id' => 'purchaseForm']) !!}
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Editar Orden</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                @include('purchases.partials.fields')
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div>
                                    @include('purchases.partials.edit_field_details')
                                    <div class="offset-9 col-md-3">
                                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                            <i class="fa fa-paper-plane-o"></i>&nbsp;
                                            <span id="payment-button-amount">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
    {!! Form::close() !!}
    <div class="modal fade" tabindex="-1" role="dialog" id="editor-modal" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="editor" class="modal-content form-horizontal">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Producto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="product_name" class="control-label mb-1">Producto</label>
                            {!! Form::text('product_name', null, ['id' => 'select_products']) !!}
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="control-label mb-1">Cantidad</label>
                            {!! Form::text('quantity', null, ['class' => 'form-control', 'id' => 'quantity']) !!}
                        </div>
                        <div class="form-group">
                            <label for="price" class="control-label mb-1">Precio de Compra</label>
                            {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'price']) !!}
                        </div>
                        <input type="hidden" name="product_name" id="product_name">
                        <input type="hidden" name="last_price" id="last_price">
                        <input type="hidden" name="subtotal" id="subtotal">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $("#purchaseForm").submit(function (e) {

                //disable the submit button
                $("#payment-button").attr("disabled", true);

                return true;

            });
        });
    </script>
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        var xhr;
        $('#select_branches').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $branchesJson !!}
        });

        $('#select_provider').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $providersJson !!}
        });

        $('#select_payments_form').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $paymentsFormJson !!}
        });

        $('#select_currency').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $currencyJson !!}
        });

        $('#select_products').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            options: {!! $productsJson !!},
            onChange: function (value) {
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: 'http://ferreteria.test/purchases/find_product/' + value,
                    method: 'GET',
                    success: function (result) {
                        console.log(result);
                        var response = JSON.parse(result);
                        $('#last_price').val(response.price);
                        $('#product_name').val(response.name);
                    },
                    error: function () {
                        alert("this is not working");
                    }
                });
            }
        });
    </script>

    <script type="text/javascript" src="{{ '/assets/js/footable.js' }}"></script>
    <script type="text/javascript" src="{{ '/assets/js/moment.js' }}"></script>
    <script>
        jQuery(function ($) {
            // $('.table').footable();
            var $modal = $('#editor-modal'),
                $editor = $('#editor'),
                $editorTitle = $('#editor-title'),
                ft = FooTable.init('#editing-example', {
                    editing: {
                        enabled: true,
                        addRow: function () {
                            $modal.removeData('row');
                            $editor[0].reset();
                            $editorTitle.text('Agregar nueva fila');
                            $modal.modal('show');
                        },
                        editRow: function (row) {
                            var values = row.val();
                            $editor.find('#select_products').val(values.id);
                            $editor.find('#product_name').val(values.productName);
                            $editor.find('#last_price').val(values.lastPrice);
                            $editor.find('#price').val(values.currentPrice);
                            $editor.find('#quantity').val(values.quantity);
                            $editor.find('#subtotal').val(values.subtotal);

                            $modal.data('row', row);
                            $editorTitle.text('Editar #' + values.id);
                            $modal.modal('show');
                        },
                        deleteRow: function (row) {
                            console.log(row);
                            if (confirm('Esta por eliminar esta fila, esta seguro?')) {
                                xhr && xhr.abort();
                                xhr = $.ajax({
                                    url: 'http://ferreteria.test/purchases/save/pre',
                                    method: 'POST',
                                    data : {
                                        product_id: row.value.id,
                                        quantity: row.value.quantity,
                                        price: row.value.currentPrice,
                                        action: 'delete',
                                        _token:  "{!! csrf_token() !!}"
                                    },
                                    success: function (result) {
                                        // values.id = uid++;
                                        row.delete();
                                    },
                                    error: function () {
                                        alert("this is not working");
                                    }
                                });
                            }
                        }
                    }
                }),

                uid = 0;

            $editor.on('submit', function (e) {
                if (this.checkValidity && !this.checkValidity()) return;
                e.preventDefault();
                var row = $modal.data('row'),
                    values = {
                        id: $editor.find('#select_products').val(),
                        productName: $editor.find('#product_name').val(),
                        lastPrice: $editor.find('#last_price').val(),
                        currentPrice: $editor.find('#price').val(),
                        quantity: $editor.find('#quantity').val(),
                        subtotal: (parseInt($editor.find('#subtotal').val()) * parseInt($editor.find('#quantity').val()))
                    };

                if (row instanceof FooTable.Row) {
                    console.log('edit');
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: 'http://ferreteria.test/purchases/save/pre',
                        method: 'POST',
                        data : {
                            product_id: values.id,
                            quantity: values.quantity,
                            price: values.currentPrice,
                            action: 'edit',
                            _token:  "{!! csrf_token() !!}"
                        },
                        success: function (result) {
                            // values.id = uid++;
                            row.val(values);
                        },
                        error: function () {
                            alert("this is not working");
                        }
                    });
                } else {
                    console.log('add');
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: 'http://ferreteria.test/purchases/save/pre',
                        method: 'POST',
                        data : {
                            product_id: values.id,
                            quantity: values.quantity,
                            price: values.currentPrice,
                            action: 'add',
                            _token:  "{!! csrf_token() !!}"
                        },
                        success: function (result) {
                            // values.id = uid++;
                            ft.rows.add(values);
                        },
                        error: function () {
                            alert("this is not working");
                        }
                    });

                }
                $modal.modal('hide');
            });
        });
    </script>
@endsection