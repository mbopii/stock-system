@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('sales.index') }}">Ventas</a></li>
            <li class="active">Ver</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"><b>Factura Nro:</b> {{$header->id}}</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Fecha de
                                                Compra:</b> {{ $header->purchase_date }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment"
                                               class="control-label mb-1"><b>Proveedor:</b> {{ $header->provider->description }}
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment"
                                               class="control-label mb-1"><b>Timbrado:</b> {{ $header->stamping }}
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Nro de Factura: </b>
                                            {{ $header->invoice_number }}</label>
                                    </div>
                                    <div class="form-group">

                                        @if ($header->payment_form_id == 2)
                                            <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total/Monto Pagado</b>
                                                Gs. {{ number_format($header->total_amount, 0, ',', '.') }}
                                                /
                                                Gs. {{ number_format($header->total_amount - $header->amount_left, 0, ',', '.') }}
                                            </label>
                                        @else
                                            <label for="cc-payment" class="control-label mb-1"><b>Monto
                                                    Total:</b>
                                                Gs. {{ number_format($header->total_amount, 0, ',', '.') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Forma de Pago: </b>
                                            {{ $header->paymentForm->description }}</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Cuotas: </b>
                                            {{$header->total_fee}}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Total/Faltantes: </b>
                                            {{ $header->total_fee }} / {{ $header->remaining_fee }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1"><b>Estado de la factura: </b>
                                            {{ $header->invoiceStatus->description }}</label>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Precio</th>
                                        <th scope="col">Cambio del Díao</th>
                                        <th scope="col">Precio (PYG)</th>
                                        <th scope="col">Precio con Descuento</th>
                                        <th scope="col">Precio con Descuento (PGY)</th>
                                        <th scope="col">SubTotal</th>
                                        <th scope="col">SubTotal Con Descuento</th>
                                        <th>SubTotal (PGY)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($header->purchaseDetails as $detail)
                                        <tr>
                                            <th scope="row">{{ $detail->id }}</th>
                                            <td>{{ $detail->product->title }}</td>
                                            <td>{{ $detail->quantity }}</td>
                                            <td>{{ number_format($detail->single_amount , 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->currency_change , 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->single_amount * $detail->currency_change, 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->special_price * $detail->currency_change, 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->special_price , 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->total_amount , 0, ',', '.') }}</td>
                                            <td>{{ number_format($detail->special_subtotal, 0, ',', '.') }}</td>
                                            <td>{{ number_format(($detail->special_subtotal != 0) ? $detail->special_subtotal  *  $detail->currency_change : $detail->total_amount * $detail->currency_change , 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div>
        </div>
    </div>

    @if ($header->payment_form_id == 2)
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pagos</strong>
                        </div>
                        <div class="card-body">
                            <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nro de Recibo</th>
                                            <th scope="col">Forma de Pago</th>
                                            <th scope="col">Banco</th>
                                            <th scope="col">Nro Cheque</th>
                                            <th scope="col">Monto</th>
                                            <th scope="col">Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($header->vouchers as $voucher)
                                            <tr>
                                                <th scope="row">{{ $voucher->id }}</th>
                                                <td>{{ $voucher->voucher_number }}</td>
                                                <td>{{ $voucher->paymentMethods->description }}</td>
                                                <td>@if(isset($voucher->banks)){{ $voucher->banks->description }}@endif</td>
                                                <td>{{ $voucher->check_number }}</td>
                                                <td>{{ number_format($voucher->amount, 0, ',', '.') }}</td>
                                                <td>{{ $voucher->created_at }}</td>
                                                <td>
                                                    @if($voucher->status_id == 1)
                                                        <a href="{{ route('purchases.voucher.cancel', $voucher->id) }}">
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="fa fa-times"
                                                                   @if($voucher->status_id != 2 ) style="color:#ffffff"
                                                                   @else style="color: #696969;" @endif></i>
                                                            </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
            </div>
        </div>
    @endif
@endsection