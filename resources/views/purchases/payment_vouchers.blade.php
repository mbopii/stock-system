@extends('partials.layout')
@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('purchases.index') }}">Compras</a></li>
            <li class="active">Recibos</li>
        </ol>
    </div>
@endsection

@section('content')
    {{ Form::open(['route' => 'purchases.payments.confirmation', 'method' => 'POST']) }}
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-10">
                            <strong class="card-title">Facturas a Pagar</strong>
                            <small>Seleccione las facturas que se pagaran</small>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success btn-sm" type="submit">
                                <i class="fa fa-money"></i> Confirmar Pago
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table" id="purchases_table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Proveedor</th>
                                <th scope="col">Empresa</th>
                                <th scope="col">Fecha de Compra</th>
                                <th scope="col">Nro de Factura</th>
                                <th scope="col">Monto / Pendiente</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection

@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>
@endsection
@section('js')
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        var data =
                {!! $creditPurchasesJson !!}
        var table = $('#purchases_table').DataTable({
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin reultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                    searchPlaceholder: "Buscar Facturas...",
                    search: "",
                },
                keys: {
                    keys: [13, 38, 40]
                },
                pageLength: 25,
                dom: ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
                aaData: data,
                order: [[0, "desc"]],
                columns: [
                    {data: "id"},
                    {data: "provider"},
                    {data: "company"},
                    {data: "purchase_date"},
                    {data: "invoice_number"},
                    {data: "amounts"},
                    {data: "created_at"},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<input class="btn btn-success btn-sm btn-success" type="checkbox" name="payment[]" value="' + row.id +'">';
                        }
                    }
                ]

            });
    </script>
@endsection

