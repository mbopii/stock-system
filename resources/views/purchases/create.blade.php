@extends('partials.layout')

@section('breadcrumbs')
    <div class="page-header float-right">
        <ol class="breadcrumb text-right">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ route('purchases.index') }}">Compras</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </div>
@endsection

@section('content')
    <style>
        div#preloader {
            background: #fefefea3 url("../images/loading.gif") no-repeat scroll center center;
            height: 100%;
            width: 100%;
            left: 0;
            overflow: visible;
            position: fixed;
            top: 0;
            z-index: 9999999999;
            display: none;
        }
    </style>
    {!! Form::open(['route' => ['purchases.store', 'method' => 'post', 'id' => 'purchaseForm']]) !!}
    <div class="animated fadeIn">
        <div id="preloader"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Nueva Orden</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                @include('purchases.partials.fields')
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->

            </div><!--/.col-->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Detalles</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div>
                                    <div class="col-md-3">
                                        <button type="button" data-toggle="modal" data-target="#editor-modal"
                                                class="btn btn-sm btn-dark btn-block">
                                            <i class="fa fa-paper-plane-o"></i>&nbsp;
                                            <span id="payment-button-amount">Agregar Producto</span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <button id="payment-button" type="submit" class="btn btn-sm btn-info btn-block">
                                            <i class="fa fa-shopping-cart"></i>&nbsp;
                                            <span id="payment-button-amount">Comprar</span>
                                        </button>
                                    </div>
                                    <div class="offset-3 col-md-3">
                                        Gs. <label class="sub_total">0</label>
                                    </div>
                                    <br>
                                    @include('purchases.partials.details_fields')
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- .card -->
            </div><!--/.col-->
        </div>
    </div>
    {!! Form::close() !!}
    <div class="modal fade" tabindex="-1" role="dialog" id="editor-modal" aria-labelledby="editor-title">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" role="document" style="width: 760px;">
                <form id="editor" class="modal-content form-horizontal">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Producto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product_name" class="control-label mb-1">Producto</label>
                            {!! Form::text('product_name', null, ['id' => 'select_products', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="control-label mb-1">Cantidad</label>
                            {!! Form::text('quantity', null, ['class' => 'form-control', 'id' => 'quantity', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="form-group">
                            <label for="price" class="control-label mb-1">Precio de Compra</label>
                            {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'price', 'autocomplete' => 'off']) !!}
                        </div>
                        <input type="hidden" name="product_name" id="product_name">
                        <input type="hidden" name="last_price" id="last_price">
                        <input type="hidden" name="subtotal" id="subtotal">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary products" id="add_product" disabled="disabled">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@section('css')
    <link rel="stylesheet" href="{{ '/assets/css/selectize.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/footable.bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/datatables.min.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/lib/datatable/jquery.dataTables.min.css' }}">
    <link rel="stylesheet" type="text/css"
          href="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/css/keyTable.dataTables.css' }}"/>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $("#purchaseForm").submit(function (e) {

                //disable the submit button
                $("#payment-button").attr("disabled", true);

                return true;

            });
        });
    </script>
    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ '/assets/js/selectize.js' }}"></script>
    <script>
        $("#select_payments_form").change(function () {
            var selectedSellType = $(this).children("option:selected").val();
            console.log(selectedSellType);
            if (selectedSellType == '2'){
                $('#input_credit_days').removeAttr('disabled');
            }else{
                $('#input_credit_days').prop('disabled', true).val('');
            }
        });

        $('#input_credit_days').on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^0-9\\:]+/g, ""));
            if ((event.which < 48 || event.which > 60)) {
                event.preventDefault();
            }
        });
    </script>
    <script>
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var xhr;
        $('#select_branches').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $branchesJson !!}
        });

        $('#select_provider').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            onChange(value) {
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: rootURL + 'providers/search/' + value,
                    method: "GET",
                    success: function (result) {
                        console.log(result);
                        var response = JSON.parse(result);
                        if (response.error == false) {
                            $('#provider_tax_code').val(response.provider.tax_code);
                            $('#provider_tax_name').val(response.provider.tax_name);
                            $('#provider_address').val(response.provider.idnum);
                        }
                    },
                    error: function () {
                        console.log("THis is an error message");

                    }
                });
            },
            options: {!! $providersJson !!}
        });

        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
            })
        });

        $('#select_currency').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $currencyJson !!}
        });

        $('#select_cashier').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.name) + '</span></div>';
                }
            },
            options: {!! $cashierJson !!}
        });

        $('#select_company').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'description',
            searchField: 'description',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.description) + '</span></div>';
                }
            },
            options: {!! $companiesJson !!}
        });

        $('#select_products').selectize({
            delimiter: ',',
            persist: false,
            openOnFocus: true,
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            maxItems: 1,
            render: {
                item: function (item, escape) {
                    return '<div><span class="label label-primary">' + escape(item.title) + '</span></div>';
                }
            },
            options: {!! $productsJson !!},
            onChange: function (value) {
                if (!value) {
                    $('#last_price').val('');
                    $('#product_name').val("");
                    $('.products').prop("disabled", true);
                    return;
                }

                $("#preloader").show();
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: rootURL + 'products/find_product/' + value,
                    method: 'GET',
                    success: function (result) {
                        console.log(result);
                        var response = JSON.parse(result);
                        $('#last_price').val(response.purchase_price);
                        $('#product_name').val(response.name);
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        $('.products').prop("disabled", false);
                    },
                    error: function () {
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                        $('.products').prop("disabled", false);
                        alert("Error al intentar obtener los datos del producto");

                    }
                });
            }
        });
    </script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/jquery.dataTables.min.js' }}"></script>
    <script type="text/javascript"
            src="{{ '/assets/js/lib/data-table/KeyTable-2.4.0/js/dataTables.keyTable.js' }}"></script>
    <script type="text/javascript" src="{{ '/assets/js/lib/data-table/dataTables.cellEdit.js' }}"></script>
    <script>
        $(document).ready(function () {
            var table = $('#purchases_table').DataTable({
                language: {
                    lengthMenu: "Mostrar _MENU_ resultados por página",
                    sZeroRecords: "Sin resultados",
                    info: "Mostrando pagina _PAGE_ de _PAGES_",
                    infoEmpty: "Sin reultados",
                    sInfoFiltered: "Filtrando de _MAX_ resultados",
                },
                pageLength: 50,
                // keys: true,
                columns: [
                    {data: "id"},
                    {data: 'product'},
                    {data: 'last_purchase'},
                    {data: 'amount'},
                    {data: "discount", className: "alter_price"},
                    {data: 'quantity', className: 'products_quantity'},
                    {data: 'subtotal'},
                    {
                        'data': 'Action', 'render': function (data, type, row, meta) {
                            return '<button class="btn btn-danger btn-sm btn-danger remove_product" type="button"><i class="fa fa-trash"></i></button>';
                        }
                    }
                ]
            });

            $('#add_product').on("click", function (e) {
                e.preventDefault();
                $('#editor-modal').modal('toggle');
                $("#preloader").show();
                let quantity = $("#quantity").val();
                let currentPurchasePrice = $("#price").val();
                let lastPurchasePrice = $("#last_price").val();
                let productName = $('#product_name').val();
                let productId = $('#select_products').val();

                let currentSubTotal = $('.sub_total');
                currentSubTotal.text(parseInt(currentSubTotal.text()) + parseInt(quantity * currentPurchasePrice));
                table.row.add({
                        id: productId,
                        product: productName + '<input type="hidden" name="purchase_product_id[]" value="' + productId + '">' +
                        '<input type="hidden" class="purchase_quantity" name="purchase_quantity[]" value="' + quantity + '">' +
                        '<input type="hidden" class="purchase_price" name="purchase_price[]" value="' + currentPurchasePrice + '">' +
                        '<input type="hidden" class="purchase_discount" name="purchase_discount[]" value="0">',
                        discount: null,
                        last_purchase: lastPurchasePrice,
                        amount: currentPurchasePrice,
                        quantity: quantity,
                        subtotal: quantity * currentPurchasePrice,
                    }
                ).draw(false);

                $("#price").val('');
                $('#select_products').val('');
                $("#quantity").val('');

                $('#preloader').fadeOut('slow', function () {
                    $(this).css('display', 'none');
                });
                $('.products').prop("disabled", true);

            });

            function myCallBackFunction(updatedCell, updatedRow, oldValue) {
                console.log("The new value for the cell is: " + updatedCell.data());
                console.log("The values for each cell in that row are: " + updatedRow.data());
                console.log(oldValue);
            }

            table.MakeCellsEditable({
                "onUpdate": myCallBackFunction,
                "inputCss": 'form-control',
                "columns": [4, 5],
                "allowNulls": {
                    "columns": [4, 5],
                    "errorClass": 'error'
                },
                "inputTypes": [
                    {
                        "column": 5,
                        "type": "text",
                        "options": null
                    },
                    {
                        "column": 4,
                        "type": "text",
                        "options": null
                    }
                ]
            });

            $('#purchases_table tbody').on('click', '.remove_product', function () {
                let data = table.row($(this).parents('tr')).data();
                table.row($(this).parents("tr")).remove().draw();
                $('.sub_total').text(parseInt($('.sub_total').text()) - parseInt(data.subtotal));
            });

            $(document).on('change', '.products_quantity', function () {

                let row = table.row($(this).parents('tr'));
                let hiddenQuantity = $(this).parents('tr').find('.purchase_quantity');
                let quantity = table.cell(row, 5).data(); // 6ta columna cantidad

                updateQuantity(row, table, $(this))

            });


            $(document).on('change', '.alter_price', function () {
                let row = table.row($(this).parents('tr'));
                let hiddenQuantity = $(this).parents('tr').find('.purchase_quantity');
                let quantity = table.cell(row, 5).data(); // 6ta columna cantidad
                updateQuantity(row, table, $(this))

            });

            function updateQuantity(row, table, cell) {
                xhr && xhr.abort();
                $("#preloader").show();
                xhr = $.ajax({
                    url: rootURL + 'providers/search/' + 1,
                    method: "GET",
                    success: function (result) {
                        let newQuantity = table.cell(row, 5).data();
                        let productAmount = table.cell(row, 3).data();
                        let oldSubTotal = table.cell(row, 6).data();

                        let discountInput = cell.parents('tr').find('.purchase_discount');

                        // Actualizamos subTotal de la fila
                        let discountPercentage = parseInt(table.cell(row, 4).data());
                        if (table.cell(row, 4).data()){
                            console.log("this purchase has discounts " + table.cell(row, 4).data());
                            productAmount = productAmount - (productAmount * discountPercentage / 100);
                            discountInput.val(discountPercentage);
                            table.cell(row, 6).data(parseInt(productAmount * newQuantity)).draw();
                        } else {
                            discountInput.val(discountPercentage);
                            console.log("this purchases has NO discounts");
                            table.cell(row, 6).data(parseInt(productAmount * newQuantity)).draw();
                        }

                        // Actualizamos subTotal de la tabla o total del producto
                        let subTotal = $('.sub_total');
                        let s = parseInt(subTotal.text()) - parseInt(oldSubTotal);
                        subTotal.text(parseInt(s + (productAmount * newQuantity)));

                        // Actualizamos los valores ocultos
                        cell.parents('tr').find('.purchase_quantity').val(newQuantity);
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        })
                    },
                    error: function () {
                        alert("this is not working");
                        $('#preloader').fadeOut('slow', function () {
                            $(this).css('display', 'none');
                        });
                    }
                });
            }
        });
    </script>
@endsection
