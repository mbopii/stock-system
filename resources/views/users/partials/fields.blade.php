<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Email</label>
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre de Usuario</label>
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Contraseña</label>
    {!! Form::password('password',  ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Confirmar Contraseña</label>
    {!! Form::password('password_confirmation',  ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Dirección</label>
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Teléfono</label>
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Puede comisionar</label>
    @if(isset($user))
        {!! Form::select('can_get_commission', [1 => 'Si', 2 => 'No'], $user->can_get_commission ? 1 : 2, ['class' => 'form-control', 'placeholder' => 'Seleccione']) !!}
    @else
        {!! Form::select('can_get_commission', [1 => 'Si', 2 => 'No'], null, ['class' => 'form-control', 'placeholder' => 'Seleccione']) !!}
    @endif
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Porcentaje Comisión</label>
    {!! Form::text('commission_percentage', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Roles</label>
    {!! Form::select('role_id', $rolesList , null, ['class' => 'form-control']) !!}
</div>
