<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Nombre</label>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Dirección</label>
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Teléfono</label>
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Observaciones</label>
    {!! Form::textarea('observations', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="cc-payment" class="control-label mb-1">Comisión</label>
    {!! Form::number('percentage', null, ['class' => 'form-control']) !!}
</div>
 {{ Form::hidden("branch_id", 1) }}
